package acqua.query.result;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Iterator;
import java.util.TreeMap;

import acqua.config.Config;

public class NMResultAnalyserByAggregate {
	public  Connection c = null;
	public InputStream    fis;
	public BufferedReader br;
	
	public NMResultAnalyserByAggregate(){
		try{Class.forName("org.sqlite.JDBC");
		c = DriverManager.getConnection(Config.INSTANCE.getDatasetDb());

		c.setAutoCommit(true); // only required if autocommit state not known
		Statement stat = c.createStatement(); 
		stat.executeUpdate("PRAGMA synchronous = OFF;");
		stat.close();

		}catch(Exception e){e.printStackTrace();}
	}
	public  void insertMNResultToDB(String jointype){
		Statement stmt = null;
		try{
			stmt = c.createStatement();
		stmt.executeUpdate(" DROP TABLE IF EXISTS "+jointype+"J ;");
		String sql = "CREATE TABLE  `"+jointype+"J` ( " +
				" `USERID`           INT    NOT NULL, " + 
				" `sid`     INT    NOT NULL, " + 
				" `sRevenue`    INT    NOT NULL, " + 
				" `TIMESTAMP`        BIGINT NOT NULL); "; 
		System.out.println(sql);
		stmt.executeUpdate(sql);
		fis = new FileInputStream(Config.INSTANCE.getProjectPath()+Config.INSTANCE.getDatasetFolder()+"joinOutput/NM"+jointype+"Output.txt");
		br = new BufferedReader(new InputStreamReader(fis));
		String line=null;
		while((line=br.readLine())!=null)
		{
			String[] userInfo = line.split(" ");	
			sql = "INSERT INTO "+jointype+"J (USERID,sid,sRevenue,TIMESTAMP) " +
					"VALUES ("+userInfo[0]+","+userInfo[1]+","+userInfo[2]+","+userInfo[3]+")"; 
			//System.out.println(sql);
			stmt.executeUpdate(sql);
		}	
		br.close();
		stmt.close();
		}catch(Exception e){e.printStackTrace();}
		
	}
	
	public void EXP1(){
		//insertMNResultToDB("JoinOperatorMaintainBasedOnIndegreeV");
		insertMNResultToDB("JoinOperatorMaintainBasedOnOutdegreeU");
		//insertMNResultToDB("JoinOperatorMaintainBasedOnWeightV");
		insertMNResultToDB("JoinOperatorMaintainLRU");
		insertMNResultToDB("JoinOperatorMaintainRandom");
		insertMNResultToDB("JoinOperatorMaintainRandomWithrepetition");
		insertMNResultToDB("JoinOracle");
		
		TreeMap<Long,Integer> oracleNMCount=computeNMOJoin();
		
		//HashMap<Long, Integer> JoinOperatorMaintainBasedOnIndegreeVE=computeJoinPrecision("JoinOperatorMaintainBasedOnIndegreeV");
		HashMap<Long, Integer> JoinOperatorMaintainBasedOnOutdegreeUE=computeJoinPrecision("JoinOperatorMaintainBasedOnOutdegreeU");
		HashMap<Long, Integer> JoinOperatorMaintainLRUE= computeJoinPrecision("JoinOperatorMaintainLRU");
		HashMap<Long, Integer> JoinOperatorMaintainRandomE= computeJoinPrecision("JoinOperatorMaintainRandom");
		HashMap<Long, Integer> JoinOperatorMaintainRandomWithrepetitionE= computeJoinPrecision("JoinOperatorMaintainRandomWithrepetition");
		try{
		BufferedWriter bw=new BufferedWriter(new FileWriter(new File(Config.INSTANCE.getProjectPath()+Config.INSTANCE.getDatasetFolder()+"joinOutput/NMcompareAggEXP1skewed.csv")));
		
		Iterator<Long> itO = oracleNMCount.keySet().iterator();

		bw.write("timestampe,WST-AGG,SBM-AGG,LRU-AGG,RND-AGG,RND-BGP-AGG \n");
		while(itO.hasNext()){
			long nextTime = itO.next();
			Integer OC=oracleNMCount.get(nextTime);
			Integer JoinOperatorMaintainBasedOnOutdegreeUJE=JoinOperatorMaintainBasedOnOutdegreeUE.get(nextTime);
			//Integer JoinOperatorMaintainBasedOnOutdegreeUJE=JoinOperatorMaintainBasedOnOutdegreeUE.get(nextTime);
			Integer JoinOperatorMaintainLRUJE=JoinOperatorMaintainLRUE.get(nextTime);
			Integer JoinOperatorMaintainRandomJE=JoinOperatorMaintainRandomE.get(nextTime);
			Integer JoinOperatorMaintainRandomWithrepetitionJE=JoinOperatorMaintainRandomWithrepetitionE.get(nextTime);
			bw.write(nextTime+","+OC+","+(JoinOperatorMaintainBasedOnOutdegreeUJE==null?0:JoinOperatorMaintainBasedOnOutdegreeUJE)+","
					+(JoinOperatorMaintainLRUJE==null?0:JoinOperatorMaintainLRUJE)+","
							+(JoinOperatorMaintainRandomJE==null?0:JoinOperatorMaintainRandomJE)+","
					+(JoinOperatorMaintainRandomWithrepetitionJE==null?0:JoinOperatorMaintainRandomWithrepetitionJE)+" \n");	
		}		
		bw.flush();
		bw.close();
		}catch(Exception e){e.printStackTrace();}
		
	}
	public void EXP2(){
		insertMNResultToDB("JoinOperatorMaintainBasedOnIndegreeV");
		insertMNResultToDB("ScoreIndegreeV");
		insertMNResultToDB("JoinOracle");
		
		TreeMap<Long,Integer> oracleNMCount=computeNMOJoin();
		
		HashMap<Long, Integer> JoinOperatorMaintainBasedOnIndegreeVE=computeJoinPrecision("JoinOperatorMaintainBasedOnIndegreeV");
		HashMap<Long, Integer> ScoreIndegreeVE= computeJoinPrecision("ScoreIndegreeV");
		try{
		BufferedWriter bw=new BufferedWriter(new FileWriter(new File(Config.INSTANCE.getProjectPath()+Config.INSTANCE.getDatasetFolder()+"joinOutput/NMcompareAggEXP2skewed.csv")));
		
		Iterator<Long> itO = oracleNMCount.keySet().iterator();

		bw.write("timestampe,WST-AGG,SBM-AGG,IBM-AGG \n");
		while(itO.hasNext()){
			long nextTime = itO.next();
			Integer OC=oracleNMCount.get(nextTime);
			Integer JoinOperatorMaintainBasedOnIndegreeVJE=JoinOperatorMaintainBasedOnIndegreeVE.get(nextTime);
			//Integer JoinOperatorMaintainBasedOnOutdegreeUJE=JoinOperatorMaintainBasedOnOutdegreeUE.get(nextTime);
			Integer ScoreIndegreeVJE=ScoreIndegreeVE.get(nextTime);
			bw.write(nextTime+","+OC+","+(JoinOperatorMaintainBasedOnIndegreeVJE==null?0:JoinOperatorMaintainBasedOnIndegreeVJE)+","
					+(ScoreIndegreeVJE==null?0:ScoreIndegreeVJE)+" \n");	
		}		
		bw.flush();
		bw.close();
		}catch(Exception e){e.printStackTrace();}
	}
	public void EXP3(){
		insertMNResultToDB("ScoreIndegreeVFlexibleBudget");
		insertMNResultToDB("ScoreIndegreeV");
		insertMNResultToDB("JoinOracle");
		
		TreeMap<Long,Integer> oracleNMCount=computeNMOJoin();
		
		HashMap<Long, Integer> ScoreIndegreeVFlexibleBudgetE=computeJoinPrecision("ScoreIndegreeVFlexibleBudget");
		HashMap<Long, Integer> ScoreIndegreeVE= computeJoinPrecision("ScoreIndegreeV");
		try{
		BufferedWriter bw=new BufferedWriter(new FileWriter(new File(Config.INSTANCE.getProjectPath()+Config.INSTANCE.getDatasetFolder()+"joinOutput/NMcompareAggEXP3uniformCRu6GUw150,120s58.csv")));
		
		Iterator<Long> itO = oracleNMCount.keySet().iterator();

		bw.write("timestampe,WST-AGG,IBM-AGG, IBM-AGG-FBA \n");
		while(itO.hasNext()){
			long nextTime = itO.next();
			Integer OC=oracleNMCount.get(nextTime);
			Integer ScoreIndegreeVFlexibleBudgetJE=ScoreIndegreeVFlexibleBudgetE.get(nextTime);
			//Integer JoinOperatorMaintainBasedOnOutdegreeUJE=JoinOperatorMaintainBasedOnOutdegreeUE.get(nextTime);
			Integer ScoreIndegreeVJE=ScoreIndegreeVE.get(nextTime);
			bw.write(nextTime+","+OC+","+(ScoreIndegreeVJE==null?0:ScoreIndegreeVJE)+","+
			(ScoreIndegreeVFlexibleBudgetJE==null?0:ScoreIndegreeVFlexibleBudgetJE)
					+" \n");	
		}
		bw.flush();
		bw.close();		
		}catch(Exception e){e.printStackTrace();}
	}
	
	public static void insertMNResultToDB(){
		Connection c = null;
		Statement stmt = null;
		try {		
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(Config.INSTANCE.getDatasetDb());

			c.setAutoCommit(true); // only required if autocommit state not known
			Statement stat = c.createStatement(); 
			stat.executeUpdate("PRAGMA synchronous = OFF;");
			stat.close();

			stmt = c.createStatement();
			//stmt.executeQuery("DROP INDEX IF EXISTS timeIndex ON BKG;");
			stmt.executeUpdate(" DROP TABLE IF EXISTS lruJ ;");
			String sql = "CREATE TABLE  `lruJ` ( " +
					" `USERID`           INT    NOT NULL, " + 
					" `sid`     INT    NOT NULL, " + 
					" `sRevenue`    INT    NOT NULL, " + 
					" `TIMESTAMP`        BIGINT NOT NULL); "; 
			System.out.println(sql);
			stmt.executeUpdate(sql);
			
			stmt.executeUpdate(" DROP TABLE IF EXISTS drandJ ;");
			sql = "CREATE TABLE  `drandJ` ( " +
					" `USERID`           INT    NOT NULL, " + 
					" `sid`     INT    NOT NULL, " + 
					" `sRevenue`    INT    NOT NULL, " + 
					" `TIMESTAMP`        BIGINT NOT NULL); "; 
			System.out.println(sql);
			stmt.executeUpdate(sql);
			
			stmt.executeUpdate(" DROP TABLE IF EXISTS RandJ ;");
			sql = "CREATE TABLE  `RandJ` ( " +
					" `USERID`           INT    NOT NULL, " + 
					" `sid`     INT    NOT NULL, " + 
					" `sRevenue`    INT    NOT NULL, " + 
					" `TIMESTAMP`        BIGINT NOT NULL); "; 
			System.out.println(sql);
			stmt.executeUpdate(sql);
			stmt.executeUpdate(" DROP TABLE IF EXISTS inJ ;");
			sql = "CREATE TABLE  `inJ` ( " +
					" `USERID`           INT    NOT NULL, " + 
					" `sid`     INT    NOT NULL, " + 
					" `sRevenue`    INT    NOT NULL, " + 
					" `TIMESTAMP`        BIGINT NOT NULL); "; 
			System.out.println(sql);
			stmt.executeUpdate(sql);
			
			stmt.executeUpdate(" DROP TABLE IF EXISTS UJ ;");
			sql = "CREATE TABLE  `UJ` ( " +
					" `USERID`           INT    NOT NULL, " + 
					" `sid`     INT    NOT NULL, " + 
					" `sRevenue`    INT    NOT NULL, " + 
					" `TIMESTAMP`        BIGINT NOT NULL); "; 
			System.out.println(sql);
			stmt.executeUpdate(sql);
			
			stmt.executeUpdate(" DROP TABLE IF EXISTS VJ ;");
			sql = "CREATE TABLE  `VJ` ( " +
					" `USERID`           INT    NOT NULL, " + 
					" `sid`     INT    NOT NULL, " + 
					" `sRevenue`    INT    NOT NULL, " + 
					" `TIMESTAMP`        BIGINT NOT NULL); "; 
			System.out.println(sql);
			stmt.executeUpdate(sql);
			
			stmt.executeUpdate(" DROP TABLE IF EXISTS OJ ;");
			sql = "CREATE TABLE  `OJ` ( " +
					" `USERID`           INT    NOT NULL, " + 
					" `sid`     INT    NOT NULL, " + 
					" `sRevenue`    INT    NOT NULL, " + 
					" `TIMESTAMP`        INT NOT NULL); "; 
			System.out.println(sql);
			stmt.executeUpdate(sql);
			
			stmt.executeUpdate(" DROP TABLE IF EXISTS scoreV ;");
			sql = "CREATE TABLE  `scoreV` ( " +
					" `USERID`           INT    NOT NULL, " + 
					" `sid`     INT    NOT NULL, " + 
					" `sRevenue`    INT    NOT NULL, " + 
					" `TIMESTAMP`        INT NOT NULL); "; 
			System.out.println(sql);
			stmt.executeUpdate(sql);
			
			stmt.executeUpdate(" DROP TABLE IF EXISTS scoreU ;");
			sql = "CREATE TABLE  `scoreU` ( " +
					" `USERID`           INT    NOT NULL, " + 
					" `sid`     INT    NOT NULL, " + 
					" `sRevenue`    INT    NOT NULL, " + 
					" `TIMESTAMP`        INT NOT NULL); "; 
			System.out.println(sql);
			stmt.executeUpdate(sql);
			
			stmt.executeUpdate(" DROP TABLE IF EXISTS scoreURemove ;");
			sql = "CREATE TABLE  `scoreURemove` ( " +
					" `USERID`           INT    NOT NULL, " + 
					" `sid`     INT    NOT NULL, " + 
					" `sRevenue`    INT    NOT NULL, " + 
					" `TIMESTAMP`        INT NOT NULL); "; 
			System.out.println(sql);
			stmt.executeUpdate(sql);
			
			
			
			InputStream    fis;
			BufferedReader br;

			fis = new FileInputStream(Config.INSTANCE.getProjectPath()+Config.INSTANCE.getDatasetFolder()+"joinOutput/NMJoinOracleOutput.txt");
			br = new BufferedReader(new InputStreamReader(fis));
			String line=null;
			while((line=br.readLine())!=null)
			{
				String[] userInfo = line.split(" ");	
				sql = "INSERT INTO OJ (USERID,sid,sRevenue,TIMESTAMP) " +
						"VALUES ("+userInfo[0]+","+userInfo[1]+","+userInfo[2]+","+userInfo[3]+")"; 
				// System.out.println(line);
				stmt.executeUpdate(sql);
			}

			fis = new FileInputStream(Config.INSTANCE.getProjectPath()+Config.INSTANCE.getDatasetFolder()+"joinOutput/NMJoinOperatorMaintainRandomWithrepetitionOutput.txt");
			br = new BufferedReader(new InputStreamReader(fis));
			 line=null;
			while((line=br.readLine())!=null)
			{
				String[] userInfo = line.split(" ");	
				sql = "INSERT INTO drandj (USERID,sid,sRevenue,TIMESTAMP) " +
						"VALUES ("+userInfo[0]+","+userInfo[1]+","+userInfo[2]+","+userInfo[3]+")"; 
				// System.out.println(line);
				stmt.executeUpdate(sql);
			}

			fis = new FileInputStream(Config.INSTANCE.getProjectPath()+Config.INSTANCE.getDatasetFolder()+"joinOutput/NMJoinOperatorMaintainLRUOutput.txt");
			br = new BufferedReader(new InputStreamReader(fis));
			line=null;
			while((line=br.readLine())!=null)
			{
				String[] userInfo = line.split(" ");	
				sql = "INSERT INTO lruJ (USERID,sid,sRevenue,TIMESTAMP) " +
						"VALUES ("+userInfo[0]+","+userInfo[1]+","+userInfo[2]+","+userInfo[3]+")"; 
				//System.out.println(sql);
				stmt.executeUpdate(sql);
			}
			fis = new FileInputStream(Config.INSTANCE.getProjectPath()+Config.INSTANCE.getDatasetFolder()+"joinOutput/NMJoinOperatorMaintainRandomOutput.txt");
			br = new BufferedReader(new InputStreamReader(fis));
			line=null;
			while((line=br.readLine())!=null)
			{
				String[] userInfo = line.split(" ");	
				sql = "INSERT INTO RandJ (USERID,sid,sRevenue,TIMESTAMP) " +
						"VALUES ("+userInfo[0]+","+userInfo[1]+","+userInfo[2]+","+userInfo[3]+")"; 
				//System.out.println(sql);
				stmt.executeUpdate(sql);
			}
			
			fis = new FileInputStream(Config.INSTANCE.getProjectPath()+Config.INSTANCE.getDatasetFolder()+"joinOutput/NMJoinOperatorMaintainBasedOnWeightVOutput.txt");
			br = new BufferedReader(new InputStreamReader(fis));
			line=null;
			while((line=br.readLine())!=null)
			{
				String[] userInfo = line.split(" ");	
				sql = "INSERT INTO VJ (USERID,sid,sRevenue,TIMESTAMP) " +
						"VALUES ("+userInfo[0]+","+userInfo[1]+","+userInfo[2]+","+userInfo[3]+")"; 
				//System.out.println(sql);
				stmt.executeUpdate(sql);
			}
			
			fis = new FileInputStream(Config.INSTANCE.getProjectPath()+Config.INSTANCE.getDatasetFolder()+"joinOutput/NMJoinOperatorMaintainBasedOnOutdegreeUOutput.txt");
			br = new BufferedReader(new InputStreamReader(fis));
			line=null;
			while((line=br.readLine())!=null)
			{
				//System.out.println(">>>>>>>>>>>>>>>."+line);
				String[] userInfo = line.split(" ");	
				sql = "INSERT INTO UJ (USERID,sid,sRevenue,TIMESTAMP) " +
						"VALUES ("+userInfo[0]+","+userInfo[1]+","+userInfo[2]+","+userInfo[3]+")"; 
				//System.out.println(sql);
				stmt.executeUpdate(sql);
			}
			
			fis = new FileInputStream(Config.INSTANCE.getProjectPath()+Config.INSTANCE.getDatasetFolder()+"joinOutput/NMJoinOperatorMaintainBasedOnIndegreeVOutput.txt");
			br = new BufferedReader(new InputStreamReader(fis));
			line=null;
			while((line=br.readLine())!=null)
			{
				String[] userInfo = line.split(" ");	
				sql = "INSERT INTO inJ (USERID,sid,sRevenue,TIMESTAMP) " +
						"VALUES ("+userInfo[0]+","+userInfo[1]+","+userInfo[2]+","+userInfo[3]+")"; 
				//System.out.println(sql);
				stmt.executeUpdate(sql);
			}
			

			fis = new FileInputStream(Config.INSTANCE.getProjectPath()+Config.INSTANCE.getDatasetFolder()+"joinOutput/NMScoreIndegreeVOutput.txt");
			br = new BufferedReader(new InputStreamReader(fis));
			line=null;
			while((line=br.readLine())!=null)
			{
				String[] userInfo = line.split(" ");	
				sql = "INSERT INTO scoreV (USERID,sid,sRevenue,TIMESTAMP) " +
						"VALUES ("+userInfo[0]+","+userInfo[1]+","+userInfo[2]+","+userInfo[3]+")"; 
				//System.out.println(sql);
				stmt.executeUpdate(sql);
			}
			
			fis = new FileInputStream(Config.INSTANCE.getProjectPath()+Config.INSTANCE.getDatasetFolder()+"joinOutput/NMScoreOutdegreeUOutput.txt");
			br = new BufferedReader(new InputStreamReader(fis));
			line=null;
			while((line=br.readLine())!=null)
			{
				String[] userInfo = line.split(" ");	
				sql = "INSERT INTO scoreU (USERID,sid,sRevenue,TIMESTAMP) " +
						"VALUES ("+userInfo[0]+","+userInfo[1]+","+userInfo[2]+","+userInfo[3]+")"; 
				//System.out.println(sql);
				stmt.executeUpdate(sql);
			}
			
			fis = new FileInputStream(Config.INSTANCE.getProjectPath()+Config.INSTANCE.getDatasetFolder()+"joinOutput/NMScoreOutdegreeURemoveEdgeOutput.txt");
			br = new BufferedReader(new InputStreamReader(fis));
			line=null;
			while((line=br.readLine())!=null)
			{
				String[] userInfo = line.split(" ");	
				sql = "INSERT INTO scoreURemove (USERID,sid,sRevenue,TIMESTAMP) " +
						"VALUES ("+userInfo[0]+","+userInfo[1]+","+userInfo[2]+","+userInfo[3]+")"; 
				//System.out.println(sql);
				stmt.executeUpdate(sql);
			}
			
			
			
			fis.close();
			br.close();		
			stmt.close();
		}catch(Exception e){e.printStackTrace();}
	}
	public static TreeMap<Long,Integer> computeNMOJoin(){
		TreeMap<Long,Integer> result=new TreeMap<Long, Integer>();
		Connection c = null;
		Statement stmt = null;
		try {		
			c = DriverManager.getConnection(Config.INSTANCE.getDatasetDb());	      
			c.setAutoCommit(false);
			stmt = c.createStatement();
			String sql="select X.TIMESTAMP, count (X.fund) as windowCount from "+
	"(SELECT JoinOracleJ.TIMESTAMP as TIMESTAMP,JoinOracleJ.USERID as fund, group_concat(JoinOracleJ.sRevenue) as fundRevenue,group_concat(JoinOracleJ.sid) as fundStocks FROM JoinOracleJ  group by JoinOracleJ.TIMESTAMP , JoinOracleJ.USERID order by JoinOracleJ.TIMESTAMP  ASC) as X "+
	"group by X.TIMESTAMP order by X.TIMESTAMP  ASC";      
			System.out.println(sql);
			ResultSet rs = stmt.executeQuery( sql);

			while ( rs.next() ) {
				//String revenues  = rs.getString("fundRevenue");
				//String stocks = rs.getString("fundStocks");
				Integer windowEntries=rs.getInt("windowCount");
				Long timeStamp  = rs.getLong("TIMESTAMP");
				if (windowEntries==null);// error=0;
				result.put(timeStamp,windowEntries);
			}
			rs.close();
			stmt.close();
			c.close();
		}catch(Exception e){e.printStackTrace();}
		return result;
	}
	public static HashMap<Long,Integer> computeInJoinPrecision(){
		HashMap<Long,Integer> result=new HashMap<Long, Integer>();
		Connection c = null;
		Statement stmt = null;
		try {		
			c = DriverManager.getConnection(Config.INSTANCE.getDatasetDb());	      
			c.setAutoCommit(false);
			stmt = c.createStatement();
			//String sql="SELECT DWJ.TIMESTAMP AS TIME, SUM(ABS(DWJ.FOLLOWERCOUNT-OJ.FOLLOWERCOUNT)) AS ERROR "+
			//		  " FROM DWJ,OJ  WHERE DWJ.USERID=OJ.USERID AND DWJ.TIMESTAMP=OJ.TIMESTAMP group by OJ.TIMESTAMP";
			String sql=//"SELECT inJ.TIMESTAMP as TIMESTAMP , COUNT(*) as ERROR FROM inJ,OJ  WHERE inJ.USERID=OJ.USERID AND inJ.sid=OJ.sid AND inJ.TIMESTAMP=OJ.TIMESTAMP AND inJ.sRevenue <> OJ.sRevenue group by OJ.TIMESTAMP";
					"SELECT X.TIMESTAMP as TIMESTAMP , COUNT(*) as ERROR FROM "+  
"(SELECT oj.TIMESTAMP as TIMESTAMP,OJ.USERID as fund, group_concat(OJ.sRevenue) as fundRevenue,group_concat(OJ.sid) as fundStocks FROM OJ  group by OJ.TIMESTAMP , OJ.USERID order by OJ.TIMESTAMP  ASC) as X "+
","+
"(SELECT inJ.TIMESTAMP as TIMESTAMP,inJ.USERID as fund, group_concat(inJ.sRevenue) as fundRevenue,group_concat(inJ.sid) as fundStocks FROM inJ  group by inJ.TIMESTAMP , inJ.USERID order by inJ.TIMESTAMP  ASC) as Y "+
"WHERE X.fund=Y.fund AND X.TIMESTAMP=Y.TIMESTAMP AND X.fundRevenue <> Y.fundRevenue group by x.TIMESTAMP";
			System.out.println(sql);
			ResultSet rs = stmt.executeQuery( sql);

			while ( rs.next() ) {
				Integer error = rs.getInt("ERROR");
				Long timeStamp  = rs.getLong("TIMESTAMP");
				if (error==null) error=0;
				result.put(timeStamp,error);
			}
			rs.close();
			stmt.close();
			c.close();
		}catch(Exception e){e.printStackTrace();}
		return result;
	}
	public static HashMap<Long,Integer> computeUJoinPrecision(){
		HashMap<Long,Integer> result=new HashMap<Long, Integer>();
		Connection c = null;
		Statement stmt = null;
		try {		
			c = DriverManager.getConnection(Config.INSTANCE.getDatasetDb());	      
			c.setAutoCommit(false);
			stmt = c.createStatement();
			//String sql="SELECT DWJ.TIMESTAMP AS TIME, SUM(ABS(DWJ.FOLLOWERCOUNT-OJ.FOLLOWERCOUNT)) AS ERROR "+
			//		  " FROM DWJ,OJ  WHERE DWJ.USERID=OJ.USERID AND DWJ.TIMESTAMP=OJ.TIMESTAMP group by OJ.TIMESTAMP";
			String sql=//"SELECT inJ.TIMESTAMP as TIMESTAMP , COUNT(*) as ERROR FROM inJ,OJ  WHERE inJ.USERID=OJ.USERID AND inJ.sid=OJ.sid AND inJ.TIMESTAMP=OJ.TIMESTAMP AND inJ.sRevenue <> OJ.sRevenue group by OJ.TIMESTAMP";
					"SELECT X.TIMESTAMP as TIMESTAMP , COUNT(*) as ERROR FROM "+  
"(SELECT oj.TIMESTAMP as TIMESTAMP,OJ.USERID as fund, group_concat(OJ.sRevenue) as fundRevenue,group_concat(OJ.sid) as fundStocks FROM OJ  group by OJ.TIMESTAMP , OJ.USERID order by OJ.TIMESTAMP  ASC) as X "+
","+
"(SELECT UJ.TIMESTAMP as TIMESTAMP,UJ.USERID as fund, group_concat(UJ.sRevenue) as fundRevenue,group_concat(UJ.sid) as fundStocks FROM UJ  group by UJ.TIMESTAMP , UJ.USERID order by UJ.TIMESTAMP  ASC) as Y "+
"WHERE X.fund=Y.fund AND X.TIMESTAMP=Y.TIMESTAMP AND X.fundRevenue <> Y.fundRevenue group by x.TIMESTAMP";
			System.out.println(sql);
			ResultSet rs = stmt.executeQuery( sql);

			while ( rs.next() ) {
				Integer error = rs.getInt("ERROR");
				Long timeStamp  = rs.getLong("TIMESTAMP");
				if (error==null) error=0;
				result.put(timeStamp,error);
			}
			rs.close();
			stmt.close();
			c.close();
		}catch(Exception e){e.printStackTrace();}
		return result;
	}
	public static HashMap<Long,Integer> computeRandJoinPrecision(){
		HashMap<Long,Integer> result=new HashMap<Long, Integer>();
		Connection c = null;
		Statement stmt = null;
		try {		
			c = DriverManager.getConnection(Config.INSTANCE.getDatasetDb());	      
			c.setAutoCommit(false);
			stmt = c.createStatement();
			//String sql="SELECT DWJ.TIMESTAMP AS TIME, SUM(ABS(DWJ.FOLLOWERCOUNT-OJ.FOLLOWERCOUNT)) AS ERROR "+
			//		  " FROM DWJ,OJ  WHERE DWJ.USERID=OJ.USERID AND DWJ.TIMESTAMP=OJ.TIMESTAMP group by OJ.TIMESTAMP";
			String sql=//"SELECT inJ.TIMESTAMP as TIMESTAMP , COUNT(*) as ERROR FROM inJ,OJ  WHERE inJ.USERID=OJ.USERID AND inJ.sid=OJ.sid AND inJ.TIMESTAMP=OJ.TIMESTAMP AND inJ.sRevenue <> OJ.sRevenue group by OJ.TIMESTAMP";
					"SELECT X.TIMESTAMP as TIMESTAMP , COUNT(*) as ERROR FROM "+  
"(SELECT oj.TIMESTAMP as TIMESTAMP,OJ.USERID as fund, group_concat(OJ.sRevenue) as fundRevenue,group_concat(OJ.sid) as fundStocks FROM OJ  group by OJ.TIMESTAMP , OJ.USERID order by OJ.TIMESTAMP  ASC) as X "+
","+
"(SELECT RandJ.TIMESTAMP as TIMESTAMP,RandJ.USERID as fund, group_concat(RandJ.sRevenue) as fundRevenue,group_concat(RandJ.sid) as fundStocks FROM RandJ  group by RandJ.TIMESTAMP , RandJ.USERID order by RandJ.TIMESTAMP  ASC) as Y "+
"WHERE X.fund=Y.fund AND X.TIMESTAMP=Y.TIMESTAMP AND X.fundRevenue <> Y.fundRevenue group by x.TIMESTAMP";
			System.out.println(sql);
			ResultSet rs = stmt.executeQuery( sql);

			while ( rs.next() ) {
				Integer error = rs.getInt("ERROR");
				Long timeStamp  = rs.getLong("TIMESTAMP");
				if (error==null) error=0;
				result.put(timeStamp,error);
			}
			rs.close();
			stmt.close();
			c.close();
		}catch(Exception e){e.printStackTrace();}
		return result;
	}
	public static HashMap<Long,Integer> computeVJoinPrecision(){
		HashMap<Long,Integer> result=new HashMap<Long, Integer>();
		Connection c = null;
		Statement stmt = null;
		try {		
			c = DriverManager.getConnection(Config.INSTANCE.getDatasetDb());	      
			c.setAutoCommit(false);
			stmt = c.createStatement();
			//String sql="SELECT DWJ.TIMESTAMP AS TIME, SUM(ABS(DWJ.FOLLOWERCOUNT-OJ.FOLLOWERCOUNT)) AS ERROR "+
			//		  " FROM DWJ,OJ  WHERE DWJ.USERID=OJ.USERID AND DWJ.TIMESTAMP=OJ.TIMESTAMP group by OJ.TIMESTAMP";
			String sql=//"SELECT inJ.TIMESTAMP as TIMESTAMP , COUNT(*) as ERROR FROM inJ,OJ  WHERE inJ.USERID=OJ.USERID AND inJ.sid=OJ.sid AND inJ.TIMESTAMP=OJ.TIMESTAMP AND inJ.sRevenue <> OJ.sRevenue group by OJ.TIMESTAMP";
					"SELECT X.TIMESTAMP as TIMESTAMP , COUNT(*) as ERROR FROM "+  
"(SELECT oj.TIMESTAMP as TIMESTAMP,OJ.USERID as fund, group_concat(OJ.sRevenue) as fundRevenue,group_concat(OJ.sid) as fundStocks FROM OJ  group by OJ.TIMESTAMP , OJ.USERID order by OJ.TIMESTAMP  ASC) as X "+
","+
"(SELECT VJ.TIMESTAMP as TIMESTAMP,VJ.USERID as fund, group_concat(VJ.sRevenue) as fundRevenue,group_concat(VJ.sid) as fundStocks FROM VJ  group by VJ.TIMESTAMP , VJ.USERID order by VJ.TIMESTAMP  ASC) as Y "+
"WHERE X.fund=Y.fund AND X.TIMESTAMP=Y.TIMESTAMP AND X.fundRevenue <> Y.fundRevenue group by x.TIMESTAMP";
			System.out.println(sql);
			ResultSet rs = stmt.executeQuery( sql);

			while ( rs.next() ) {
				Integer error = rs.getInt("ERROR");
				Long timeStamp  = rs.getLong("TIMESTAMP");
				if (error==null) error=0;
				result.put(timeStamp,error);
			}
			rs.close();
			stmt.close();
			c.close();
		}catch(Exception e){e.printStackTrace();}
		return result;
	}
	
	public static HashMap<Long,Integer> computeVScoreJoinPrecision(){
		HashMap<Long,Integer> result=new HashMap<Long, Integer>();
		Connection c = null;
		Statement stmt = null;
		try {		
			c = DriverManager.getConnection(Config.INSTANCE.getDatasetDb());	      
			c.setAutoCommit(false);
			stmt = c.createStatement();
			//String sql="SELECT DWJ.TIMESTAMP AS TIME, SUM(ABS(DWJ.FOLLOWERCOUNT-OJ.FOLLOWERCOUNT)) AS ERROR "+
			//		  " FROM DWJ,OJ  WHERE DWJ.USERID=OJ.USERID AND DWJ.TIMESTAMP=OJ.TIMESTAMP group by OJ.TIMESTAMP";
			String sql=//"SELECT inJ.TIMESTAMP as TIMESTAMP , COUNT(*) as ERROR FROM inJ,OJ  WHERE inJ.USERID=OJ.USERID AND inJ.sid=OJ.sid AND inJ.TIMESTAMP=OJ.TIMESTAMP AND inJ.sRevenue <> OJ.sRevenue group by OJ.TIMESTAMP";
					"SELECT X.TIMESTAMP as TIMESTAMP , COUNT(*) as ERROR FROM "+  
"(SELECT oj.TIMESTAMP as TIMESTAMP,OJ.USERID as fund, group_concat(OJ.sRevenue) as fundRevenue,group_concat(OJ.sid) as fundStocks FROM OJ  group by OJ.TIMESTAMP , OJ.USERID order by OJ.TIMESTAMP  ASC) as X "+
","+
"(SELECT scoreV.TIMESTAMP as TIMESTAMP,scoreV.USERID as fund, group_concat(scoreV.sRevenue) as fundRevenue,group_concat(scoreV.sid) as fundStocks FROM scoreV  group by scoreV.TIMESTAMP , scoreV.USERID order by scoreV.TIMESTAMP  ASC) as Y "+
"WHERE X.fund=Y.fund AND X.TIMESTAMP=Y.TIMESTAMP AND X.fundRevenue <> Y.fundRevenue group by x.TIMESTAMP";
			System.out.println(sql);
			ResultSet rs = stmt.executeQuery( sql);

			while ( rs.next() ) {
				Integer error = rs.getInt("ERROR");
				Long timeStamp  = rs.getLong("TIMESTAMP");
				if (error==null) error=0;
				result.put(timeStamp,error);
			}
			rs.close();
			stmt.close();
			c.close();
		}catch(Exception e){e.printStackTrace();}
		return result;
	}
	
	
	public static HashMap<Long,Integer> computeUScoreJoinPrecision(){
		HashMap<Long,Integer> result=new HashMap<Long, Integer>();
		Connection c = null;
		Statement stmt = null;
		try {		
			c = DriverManager.getConnection(Config.INSTANCE.getDatasetDb());	      
			c.setAutoCommit(false);
			stmt = c.createStatement();
			//String sql="SELECT DWJ.TIMESTAMP AS TIME, SUM(ABS(DWJ.FOLLOWERCOUNT-OJ.FOLLOWERCOUNT)) AS ERROR "+
			//		  " FROM DWJ,OJ  WHERE DWJ.USERID=OJ.USERID AND DWJ.TIMESTAMP=OJ.TIMESTAMP group by OJ.TIMESTAMP";
			String sql=//"SELECT inJ.TIMESTAMP as TIMESTAMP , COUNT(*) as ERROR FROM inJ,OJ  WHERE inJ.USERID=OJ.USERID AND inJ.sid=OJ.sid AND inJ.TIMESTAMP=OJ.TIMESTAMP AND inJ.sRevenue <> OJ.sRevenue group by OJ.TIMESTAMP";
					"SELECT X.TIMESTAMP as TIMESTAMP , COUNT(*) as ERROR FROM "+  
"(SELECT oj.TIMESTAMP as TIMESTAMP,OJ.USERID as fund, group_concat(OJ.sRevenue) as fundRevenue,group_concat(OJ.sid) as fundStocks FROM OJ  group by OJ.TIMESTAMP , OJ.USERID order by OJ.TIMESTAMP  ASC) as X "+
","+
"(SELECT scoreU.TIMESTAMP as TIMESTAMP,scoreU.USERID as fund, group_concat(scoreU.sRevenue) as fundRevenue,group_concat(scoreU.sid) as fundStocks FROM scoreU  group by scoreU.TIMESTAMP , scoreU.USERID order by scoreU.TIMESTAMP  ASC) as Y "+
"WHERE X.fund=Y.fund AND X.TIMESTAMP=Y.TIMESTAMP AND X.fundRevenue <> Y.fundRevenue group by x.TIMESTAMP";
			System.out.println(sql);
			ResultSet rs = stmt.executeQuery( sql);

			while ( rs.next() ) {
				Integer error = rs.getInt("ERROR");
				Long timeStamp  = rs.getLong("TIMESTAMP");
				if (error==null) error=0;
				result.put(timeStamp,error);
			}
			rs.close();
			stmt.close();
			c.close();
		}catch(Exception e){e.printStackTrace();}
		return result;
	}
	
	public static HashMap<Long,Integer> computeUScoreRemoveJoinPrecision(){
		HashMap<Long,Integer> result=new HashMap<Long, Integer>();
		Connection c = null;
		Statement stmt = null;
		try {		
			c = DriverManager.getConnection(Config.INSTANCE.getDatasetDb());	      
			c.setAutoCommit(false);
			stmt = c.createStatement();
			//String sql="SELECT DWJ.TIMESTAMP AS TIME, SUM(ABS(DWJ.FOLLOWERCOUNT-OJ.FOLLOWERCOUNT)) AS ERROR "+
			//		  " FROM DWJ,OJ  WHERE DWJ.USERID=OJ.USERID AND DWJ.TIMESTAMP=OJ.TIMESTAMP group by OJ.TIMESTAMP";
			String sql=//"SELECT inJ.TIMESTAMP as TIMESTAMP , COUNT(*) as ERROR FROM inJ,OJ  WHERE inJ.USERID=OJ.USERID AND inJ.sid=OJ.sid AND inJ.TIMESTAMP=OJ.TIMESTAMP AND inJ.sRevenue <> OJ.sRevenue group by OJ.TIMESTAMP";
					"SELECT X.TIMESTAMP as TIMESTAMP , COUNT(*) as ERROR FROM "+  
"(SELECT oj.TIMESTAMP as TIMESTAMP,OJ.USERID as fund, group_concat(OJ.sRevenue) as fundRevenue,group_concat(OJ.sid) as fundStocks FROM OJ  group by OJ.TIMESTAMP , OJ.USERID order by OJ.TIMESTAMP  ASC) as X "+
","+
"(SELECT scoreURemove.TIMESTAMP as TIMESTAMP,scoreURemove.USERID as fund, group_concat(scoreURemove.sRevenue) as fundRevenue,group_concat(scoreURemove.sid) as fundStocks FROM scoreURemove  group by scoreURemove.TIMESTAMP , scoreURemove.USERID order by scoreURemove.TIMESTAMP  ASC) as Y "+
"WHERE X.fund=Y.fund AND X.TIMESTAMP=Y.TIMESTAMP AND X.fundRevenue <> Y.fundRevenue group by x.TIMESTAMP";
			System.out.println(sql);
			ResultSet rs = stmt.executeQuery( sql);

			while ( rs.next() ) {
				Integer error = rs.getInt("ERROR");
				Long timeStamp  = rs.getLong("TIMESTAMP");
				if (error==null) error=0;
				result.put(timeStamp,error);
			}
			rs.close();
			stmt.close();
			c.close();
		}catch(Exception e){e.printStackTrace();}
		return result;
	}
	
	public HashMap<Long,Integer> computeJoinPrecision(String jointype){
		HashMap<Long,Integer> result=new HashMap<Long, Integer>();
		Statement stmt = null;
		try {		
			stmt = c.createStatement();
			//String sql="SELECT DWJ.TIMESTAMP AS TIME, SUM(ABS(DWJ.FOLLOWERCOUNT-OJ.FOLLOWERCOUNT)) AS ERROR "+
			//		  " FROM DWJ,OJ  WHERE DWJ.USERID=OJ.USERID AND DWJ.TIMESTAMP=OJ.TIMESTAMP group by OJ.TIMESTAMP";
			String sql=//"SELECT inJ.TIMESTAMP as TIMESTAMP , COUNT(*) as ERROR FROM inJ,OJ  WHERE inJ.USERID=OJ.USERID AND inJ.sid=OJ.sid AND inJ.TIMESTAMP=OJ.TIMESTAMP AND inJ.sRevenue <> OJ.sRevenue group by OJ.TIMESTAMP";
					"SELECT X.TIMESTAMP as TIMESTAMP , COUNT(*) as ERROR FROM "+  
"(SELECT JoinOracleJ.TIMESTAMP as TIMESTAMP,JoinOracleJ.USERID as fund, group_concat(JoinOracleJ.sRevenue) as fundRevenue,group_concat(JoinOracleJ.sid) as fundStocks FROM JoinOracleJ  group by JoinOracleJ.TIMESTAMP , JoinOracleJ.USERID order by JoinOracleJ.TIMESTAMP  ASC) as X "+
","+
"(SELECT "+jointype+"J.TIMESTAMP as TIMESTAMP,"+jointype+"J.USERID as fund, group_concat("+jointype+"J.sRevenue) as fundRevenue,group_concat("+jointype+"J.sid) as fundStocks FROM "+jointype+"J  group by "+jointype+"J.TIMESTAMP , "+jointype+"J.USERID order by "+jointype+"J.TIMESTAMP  ASC) as Y "+
"WHERE X.fund=Y.fund AND X.TIMESTAMP=Y.TIMESTAMP AND X.fundRevenue <> Y.fundRevenue group by x.TIMESTAMP";
			System.out.println(sql);
			ResultSet rs = stmt.executeQuery( sql);

			while ( rs.next() ) {
				Integer error = rs.getInt("ERROR");
				Long timeStamp  = rs.getLong("TIMESTAMP");
				if (error==null) error=0;
				result.put(timeStamp,error);
			}
			rs.close();
			stmt.close();
			//c.close();
		}catch(Exception e){e.printStackTrace();}
		return result;
	}

	
	public static void main(String[] args){
		NMResultAnalyserByAggregate analyzer=new NMResultAnalyserByAggregate();
		
		//analyzer.EXP1();
		//analyzer.EXP2();
		analyzer.EXP3();
		/*try{
			insertMNResultToDB();
			BufferedWriter bw=new BufferedWriter(new FileWriter(new File(Config.INSTANCE.getProjectPath()+Config.INSTANCE.getDatasetFolder()+"joinOutput/NMcompareAggregate.csv")));
			TreeMap<Long,Integer> oracleNMCount=computeNMOJoin();
			HashMap<Long, Integer> inError=computeInJoinPrecision();//inError might not have some time stamps because all values of revenues are similar in that time stamp so no error
			HashMap<Long, Integer> VError=computeVJoinPrecision();
			HashMap<Long, Integer> UError=computeUJoinPrecision();
			HashMap<Long, Integer> RandError=computeRandJoinPrecision();
			
			HashMap<Long, Integer> VScoreError=computeVScoreJoinPrecision();
			HashMap<Long, Integer> UScoreError=computeUScoreJoinPrecision();
			
			HashMap<Long, Integer> UScoreRemoveError=computeUScoreRemoveJoinPrecision();
			
			HashMap<Long, Integer> drandError=computeJoinPrecision("drandJ");
			HashMap<Long, Integer> lruError=computeJoinPrecision("lruJ");
			Iterator<Long> itO = oracleNMCount.keySet().iterator();

			
			
			bw.write("timestampe,WST,in,U,V, Vscore, Uscore,rand,drand,lru, uScoreRemove\n");

			while(itO.hasNext()){
				long nextTime = itO.next();
				Integer OC=oracleNMCount.get(nextTime);
				Integer inJe=inError.get(nextTime);
				Integer uJe=UError.get(nextTime);
				Integer vJe=VError.get(nextTime);
				Integer RandJe=RandError.get(nextTime);
				
				Integer vScoreJe=VScoreError.get(nextTime);
				Integer uScoreJe=UScoreError.get(nextTime);
				
				Integer uScoreRemoveJe=UScoreRemoveError.get(nextTime);
				Integer randScoreJe=drandError.get(nextTime);
				Integer lruScoreJe=lruError.get(nextTime);
				
				
				//bw.write(nextTime+","+OC+","+(inJe==null?0:inJe)+","+(uJe==null?0:uJe)+","+(vJe==null?0:vJe)+","+(vScoreJe==null?0:vScoreJe)+","+(uScoreJe==null?0:uScoreJe)+","+(uScoreRemoveJe==null?0:uScoreRemoveJe)+" \n");				

				bw.write(nextTime+","+OC+","+(inJe==null?0:inJe)+","+(uJe==null?0:uJe)+","+(vJe==null?0:vJe)+","+
				(vScoreJe==null?0:vScoreJe)+","+
						(uScoreJe==null?0:uScoreJe)+","+
				(RandJe==null?0:RandJe)+","+(randScoreJe==null?0:randScoreJe)+","+(lruScoreJe==null?0:lruScoreJe)+","+(uScoreRemoveJe==null?0:uScoreRemoveJe) + "\n");				
			}
			bw.flush();
			bw.close();
		}catch(Exception e){e.printStackTrace();}*/
		
	}
}
