package acqua.query.result;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Iterator;
import java.util.TreeMap;

import acqua.config.Config;

public class NMResultAnalyserByEdges {
	
	public  Connection c = null;
	//public  Statement stmt = null;
	public InputStream    fis;
	public BufferedReader br;
	public NMResultAnalyserByEdges(){
		try{
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(Config.INSTANCE.getDatasetDb());

			c.setAutoCommit(true); // only required if autocommit state not known
			Statement stat = c.createStatement(); 
			stat.executeUpdate("PRAGMA synchronous = OFF;");
			stat.close();
		}catch(Exception e){e.printStackTrace();}
	}
	
		
	public  void insertMNResultToDB(String jointype){
		try{
			Statement	stmt = c.createStatement();
		stmt.executeUpdate(" DROP TABLE IF EXISTS "+jointype+"J ;");
		String sql = "CREATE TABLE  `"+jointype+"J` ( " +
				" `USERID`           INT    NOT NULL, " + 
				" `sid`     INT    NOT NULL, " + 
				" `sRevenue`    INT    NOT NULL, " + 
				" `TIMESTAMP`        BIGINT NOT NULL); "; 
		System.out.println(sql);
		stmt.executeUpdate(sql);
		fis = new FileInputStream(Config.INSTANCE.getProjectPath()+Config.INSTANCE.getDatasetFolder()+"joinOutput/NM"+jointype+"Output.txt");
		br = new BufferedReader(new InputStreamReader(fis));
		String line=null;
		while((line=br.readLine())!=null)
		{
			String[] userInfo = line.split(" ");	
			sql = "INSERT INTO "+jointype+"J (USERID,sid,sRevenue,TIMESTAMP) " +
					"VALUES ("+userInfo[0]+","+userInfo[1]+","+userInfo[2]+","+userInfo[3]+")"; 
			//System.out.println(sql);
			stmt.executeUpdate(sql);
		}		
		br.close();
		stmt.close();
		}catch(Exception e){e.printStackTrace();}
		
	}
	public HashMap<Long,Integer> computeJoinPrecision(String jointype){
		HashMap<Long,Integer> result=new HashMap<Long, Integer>();
		Statement stmt = null;
		try {		
			stmt = c.createStatement();
			//String sql="SELECT DWJ.TIMESTAMP AS TIME, SUM(ABS(DWJ.FOLLOWERCOUNT-OJ.FOLLOWERCOUNT)) AS ERROR "+
			//		  " FROM DWJ,OJ  WHERE DWJ.USERID=OJ.USERID AND DWJ.TIMESTAMP=OJ.TIMESTAMP group by OJ.TIMESTAMP";
			String sql="SELECT "+jointype+"J.TIMESTAMP as TIMESTAMP , COUNT(*) as ERROR FROM "+jointype+"J,JoinOracleJ  WHERE "+jointype+"J.USERID=JoinOracleJ.USERID AND "+jointype+"J.sid=JoinOracleJ.sid AND "+jointype+"J.TIMESTAMP=JoinOracleJ.TIMESTAMP AND "+jointype+"J.sRevenue <> JoinOracleJ.sRevenue group by JoinOracleJ.TIMESTAMP";
			System.out.println(sql);
			ResultSet rs = stmt.executeQuery( sql);

			while ( rs.next() ) {
				Integer error = rs.getInt("ERROR");
				Long timeStamp  = rs.getLong("TIMESTAMP");
				if (error==null) error=0;
				result.put(timeStamp,error);
			}
			rs.close();
			stmt.close();
			//c.close();
		}catch(Exception e){e.printStackTrace();}
		return result;
	}
	
	public void EXP1(){
		insertMNResultToDB("JoinOperatorMaintainBasedOnIndegreeV");
		//insertMNResultToDB("JoinOperatorMaintainBasedOnOutdegreeU");
		//insertMNResultToDB("JoinOperatorMaintainBasedOnWeightV");
		insertMNResultToDB("JoinOperatorMaintainLRU");
		insertMNResultToDB("JoinOperatorMaintainRandom");
		insertMNResultToDB("JoinOperatorMaintainRandomWithrepetition");
		insertMNResultToDB("JoinOracle");
		
		TreeMap<Long,Integer> oracleNMCount=computeNMOJoin();
		
		HashMap<Long, Integer> JoinOperatorMaintainBasedOnIndegreeVE=computeJoinPrecision("JoinOperatorMaintainBasedOnIndegreeV");
		//HashMap<Long, Integer> JoinOperatorMaintainBasedOnOutdegreeUE=computeJoinPrecision("JoinOperatorMaintainBasedOnOutdegreeU");
		HashMap<Long, Integer> JoinOperatorMaintainLRUE= computeJoinPrecision("JoinOperatorMaintainLRU");
		HashMap<Long, Integer> JoinOperatorMaintainRandomE= computeJoinPrecision("JoinOperatorMaintainRandom");
		HashMap<Long, Integer> JoinOperatorMaintainRandomWithrepetitionE= computeJoinPrecision("JoinOperatorMaintainRandomWithrepetition");
		try{
		BufferedWriter bw=new BufferedWriter(new FileWriter(new File(Config.INSTANCE.getProjectPath()+Config.INSTANCE.getDatasetFolder()+"joinOutput/NMcomparebyEdgeEXP1skewed.csv")));
		
		Iterator<Long> itO = oracleNMCount.keySet().iterator();

		bw.write("timestampe,WST,SBM-BGP,LRU,RND,RND-BGP \n");
		while(itO.hasNext()){
			long nextTime = itO.next();
			Integer OC=oracleNMCount.get(nextTime);
			Integer JoinOperatorMaintainBasedOnIndegreeVJE=JoinOperatorMaintainBasedOnIndegreeVE.get(nextTime);
			//Integer JoinOperatorMaintainBasedOnOutdegreeUJE=JoinOperatorMaintainBasedOnOutdegreeUE.get(nextTime);
			Integer JoinOperatorMaintainLRUJE=JoinOperatorMaintainLRUE.get(nextTime);
			Integer JoinOperatorMaintainRandomJE=JoinOperatorMaintainRandomE.get(nextTime);
			Integer JoinOperatorMaintainRandomWithrepetitionJE=JoinOperatorMaintainRandomWithrepetitionE.get(nextTime);
			bw.write(nextTime+","+OC+","+(JoinOperatorMaintainBasedOnIndegreeVJE==null?0:JoinOperatorMaintainBasedOnIndegreeVJE)+","
					+(JoinOperatorMaintainLRUJE==null?0:JoinOperatorMaintainLRUJE)+","
							+(JoinOperatorMaintainRandomJE==null?0:JoinOperatorMaintainRandomJE)+","
					+(JoinOperatorMaintainRandomWithrepetitionJE==null?0:JoinOperatorMaintainRandomWithrepetitionJE)+" \n");	
		}		
		bw.flush();
		bw.close();		
		}catch(Exception e){e.printStackTrace();}
	}
	public void EXP2(){
		insertMNResultToDB("JoinOperatorMaintainBasedOnIndegreeV");
		insertMNResultToDB("ScoreIndegreeV");
		insertMNResultToDB("JoinOracle");
		
		TreeMap<Long,Integer> oracleNMCount=computeNMOJoin();
		
		HashMap<Long, Integer> JoinOperatorMaintainBasedOnIndegreeVE=computeJoinPrecision("JoinOperatorMaintainBasedOnIndegreeV");
		HashMap<Long, Integer> ScoreIndegreeVE= computeJoinPrecision("ScoreIndegreeV");
		try{
		BufferedWriter bw=new BufferedWriter(new FileWriter(new File(Config.INSTANCE.getProjectPath()+Config.INSTANCE.getDatasetFolder()+"joinOutput/NMcomparebyEdgeEXP2skewed.csv")));
		
		Iterator<Long> itO = oracleNMCount.keySet().iterator();

		bw.write("timestampe,WST-BGP,SBM-BGP,IBM-BGP \n");
		while(itO.hasNext()){
			long nextTime = itO.next();
			Integer OC=oracleNMCount.get(nextTime);
			Integer JoinOperatorMaintainBasedOnIndegreeVJE=JoinOperatorMaintainBasedOnIndegreeVE.get(nextTime);
			//Integer JoinOperatorMaintainBasedOnOutdegreeUJE=JoinOperatorMaintainBasedOnOutdegreeUE.get(nextTime);
			Integer ScoreIndegreeVJE=ScoreIndegreeVE.get(nextTime);
			bw.write(nextTime+","+OC+","+(JoinOperatorMaintainBasedOnIndegreeVJE==null?0:JoinOperatorMaintainBasedOnIndegreeVJE)+","
					+(ScoreIndegreeVJE==null?0:ScoreIndegreeVJE)+" \n");	
		}		
		bw.flush();
		bw.close();
		}catch(Exception e){e.printStackTrace();}
	}
	public void EXP3(){
		insertMNResultToDB("ScoreIndegreeVFlexibleBudget");
		insertMNResultToDB("ScoreIndegreeV");
		insertMNResultToDB("JoinOracle");
		
		TreeMap<Long,Integer> oracleNMCount=computeNMOJoin();
		
		HashMap<Long, Integer> ScoreIndegreeVFlexibleBudgetE=computeJoinPrecision("ScoreIndegreeVFlexibleBudget");
		HashMap<Long, Integer> ScoreIndegreeVE= computeJoinPrecision("ScoreIndegreeV");
		try{
		BufferedWriter bw=new BufferedWriter(new FileWriter(new File(Config.INSTANCE.getProjectPath()+Config.INSTANCE.getDatasetFolder()+"joinOutput/NMcomparebyEdgeEXP3uniformCRu6GUw150,120s58.csv")));
		
		Iterator<Long> itO = oracleNMCount.keySet().iterator();

		bw.write("timestampe,WST-BGP,IBM-BGP, IBM-BGP-FBA \n");
		while(itO.hasNext()){
			long nextTime = itO.next();
			Integer OC=oracleNMCount.get(nextTime);
			Integer ScoreIndegreeVFlexibleBudgetJE=ScoreIndegreeVFlexibleBudgetE.get(nextTime);
			//Integer JoinOperatorMaintainBasedOnOutdegreeUJE=JoinOperatorMaintainBasedOnOutdegreeUE.get(nextTime);
			Integer ScoreIndegreeVJE=ScoreIndegreeVE.get(nextTime);
			bw.write(nextTime+","+OC+","+(ScoreIndegreeVJE==null?0:ScoreIndegreeVJE)+","+
			(ScoreIndegreeVFlexibleBudgetJE==null?0:ScoreIndegreeVFlexibleBudgetJE)
					+" \n");	
		}		
		bw.flush();
		bw.close();
		}catch(Exception e){e.printStackTrace();}
	}
	
	public static void insertMNResultToDB(){
		Connection c = null;
		Statement stmt = null;
		try {		
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(Config.INSTANCE.getDatasetDb());

			c.setAutoCommit(true); // only required if autocommit state not known
			Statement stat = c.createStatement(); 
			stat.executeUpdate("PRAGMA synchronous = OFF;");
			stat.close();

			stmt = c.createStatement();
			//stmt.executeQuery("DROP INDEX IF EXISTS timeIndex ON BKG;");
			stmt.executeUpdate(" DROP TABLE IF EXISTS inJ ;");
			String sql = "CREATE TABLE  `inJ` ( " +
					" `USERID`           INT    NOT NULL, " + 
					" `sid`     INT    NOT NULL, " + 
					" `sRevenue`    INT    NOT NULL, " + 
					" `TIMESTAMP`        BIGINT NOT NULL); "; 
			System.out.println(sql);
			stmt.executeUpdate(sql);
			
			stmt = c.createStatement();
			//stmt.executeQuery("DROP INDEX IF EXISTS timeIndex ON BKG;");
			stmt.executeUpdate(" DROP TABLE IF EXISTS drandJ ;");
			sql = "CREATE TABLE  `drandJ` ( " +
					" `USERID`           INT    NOT NULL, " + 
					" `sid`     INT    NOT NULL, " + 
					" `sRevenue`    INT    NOT NULL, " + 
					" `TIMESTAMP`        BIGINT NOT NULL); "; 
			System.out.println(sql);
			stmt.executeUpdate(sql);
			
			stmt = c.createStatement();
			//stmt.executeQuery("DROP INDEX IF EXISTS timeIndex ON BKG;");
			stmt.executeUpdate(" DROP TABLE IF EXISTS lruJ ;");
			sql = "CREATE TABLE  `lruJ` ( " +
					" `USERID`           INT    NOT NULL, " + 
					" `sid`     INT    NOT NULL, " + 
					" `sRevenue`    INT    NOT NULL, " + 
					" `TIMESTAMP`        BIGINT NOT NULL); "; 
			System.out.println(sql);
			stmt.executeUpdate(sql);
			
			stmt.executeUpdate(" DROP TABLE IF EXISTS UJ ;");
			sql = "CREATE TABLE  `UJ` ( " +
					" `USERID`           INT    NOT NULL, " + 
					" `sid`     INT    NOT NULL, " + 
					" `sRevenue`    INT    NOT NULL, " + 
					" `TIMESTAMP`        BIGINT NOT NULL); "; 
			System.out.println(sql);
			stmt.executeUpdate(sql);
			
			stmt.executeUpdate(" DROP TABLE IF EXISTS VJ ;");
			sql = "CREATE TABLE  `VJ` ( " +
					" `USERID`           INT    NOT NULL, " + 
					" `sid`     INT    NOT NULL, " + 
					" `sRevenue`    INT    NOT NULL, " + 
					" `TIMESTAMP`        BIGINT NOT NULL); "; 
			System.out.println(sql);
			stmt.executeUpdate(sql);
			
			stmt.executeUpdate(" DROP TABLE IF EXISTS OJ ;");
			sql = "CREATE TABLE  `OJ` ( " +
					" `USERID`           INT    NOT NULL, " + 
					" `sid`     INT    NOT NULL, " + 
					" `sRevenue`    INT    NOT NULL, " + 
					" `TIMESTAMP`        INT NOT NULL); "; 
			System.out.println(sql);
			stmt.executeUpdate(sql);
			
			stmt.executeUpdate(" DROP TABLE IF EXISTS scoreV ;");
			sql = "CREATE TABLE  `scoreV` ( " +
					" `USERID`           INT    NOT NULL, " + 
					" `sid`     INT    NOT NULL, " + 
					" `sRevenue`    INT    NOT NULL, " + 
					" `TIMESTAMP`        INT NOT NULL); "; 
			System.out.println(sql);
			stmt.executeUpdate(sql);
			
			stmt.executeUpdate(" DROP TABLE IF EXISTS scoreU ;");
			sql = "CREATE TABLE  `scoreU` ( " +
					" `USERID`           INT    NOT NULL, " + 
					" `sid`     INT    NOT NULL, " + 
					" `sRevenue`    INT    NOT NULL, " + 
					" `TIMESTAMP`        INT NOT NULL); "; 
			System.out.println(sql);
			stmt.executeUpdate(sql);
			
			stmt.executeUpdate(" DROP TABLE IF EXISTS fbV ;");
			sql = "CREATE TABLE  `fbV` ( " +
					" `USERID`           INT    NOT NULL, " + 
					" `sid`     INT    NOT NULL, " + 
					" `sRevenue`    INT    NOT NULL, " + 
					" `TIMESTAMP`        INT NOT NULL); "; 
			System.out.println(sql);
			stmt.executeUpdate(sql);
			stmt.executeUpdate(" DROP TABLE IF EXISTS RandJ ;");
			sql = "CREATE TABLE  `RandJ` ( " +
					" `USERID`           INT    NOT NULL, " + 
					" `sid`     INT    NOT NULL, " + 
					" `sRevenue`    INT    NOT NULL, " + 
					" `TIMESTAMP`        BIGINT NOT NULL); "; 
			System.out.println(sql);
			stmt.executeUpdate(sql);
			
			stmt.executeUpdate(" DROP TABLE IF EXISTS scoreURemove ;");
			sql = "CREATE TABLE  `scoreURemove` ( " +
					" `USERID`           INT    NOT NULL, " + 
					" `sid`     INT    NOT NULL, " + 
					" `sRevenue`    INT    NOT NULL, " + 
					" `TIMESTAMP`        INT NOT NULL); "; 
			System.out.println(sql);
			stmt.executeUpdate(sql);
			
			
			InputStream    fis;
			BufferedReader br;

			fis = new FileInputStream(Config.INSTANCE.getProjectPath()+Config.INSTANCE.getDatasetFolder()+"joinOutput/NMJoinOracleOutput.txt");
			br = new BufferedReader(new InputStreamReader(fis));
			String line=null;
			while((line=br.readLine())!=null)
			{
				String[] userInfo = line.split(" ");	
				sql = "INSERT INTO OJ (USERID,sid,sRevenue,TIMESTAMP) " +
						"VALUES ("+userInfo[0]+","+userInfo[1]+","+userInfo[2]+","+userInfo[3]+")"; 
				//System.out.println(sql);
				stmt.executeUpdate(sql);
			}

			fis = new FileInputStream(Config.INSTANCE.getProjectPath()+Config.INSTANCE.getDatasetFolder()+"joinOutput/NMJoinOperatorMaintainBasedOnWeightVOutput.txt");
			br = new BufferedReader(new InputStreamReader(fis));
			line=null;
			while((line=br.readLine())!=null)
			{
				String[] userInfo = line.split(" ");	
				sql = "INSERT INTO VJ (USERID,sid,sRevenue,TIMESTAMP) " +
						"VALUES ("+userInfo[0]+","+userInfo[1]+","+userInfo[2]+","+userInfo[3]+")"; 
				//System.out.println(sql);
				stmt.executeUpdate(sql);
			}
			
			fis = new FileInputStream(Config.INSTANCE.getProjectPath()+Config.INSTANCE.getDatasetFolder()+"joinOutput/NMJoinOperatorMaintainLRUOutput.txt");
			br = new BufferedReader(new InputStreamReader(fis));
			line=null;
			while((line=br.readLine())!=null)
			{
				String[] userInfo = line.split(" ");	
				sql = "INSERT INTO lruJ (USERID,sid,sRevenue,TIMESTAMP) " +
						"VALUES ("+userInfo[0]+","+userInfo[1]+","+userInfo[2]+","+userInfo[3]+")"; 
				//System.out.println(sql);
				stmt.executeUpdate(sql);
			}
			
			fis = new FileInputStream(Config.INSTANCE.getProjectPath()+Config.INSTANCE.getDatasetFolder()+"joinOutput/NMJoinOperatorMaintainRandomWithrepetitionOutput.txt");
			br = new BufferedReader(new InputStreamReader(fis));
			line=null;
			while((line=br.readLine())!=null)
			{
				String[] userInfo = line.split(" ");	
				sql = "INSERT INTO drandJ (USERID,sid,sRevenue,TIMESTAMP) " +
						"VALUES ("+userInfo[0]+","+userInfo[1]+","+userInfo[2]+","+userInfo[3]+")"; 
				//System.out.println(sql);
				stmt.executeUpdate(sql);
			}
			
			fis = new FileInputStream(Config.INSTANCE.getProjectPath()+Config.INSTANCE.getDatasetFolder()+"joinOutput/NMJoinOperatorMaintainBasedOnOutdegreeUOutput.txt");
			br = new BufferedReader(new InputStreamReader(fis));
			line=null;
			while((line=br.readLine())!=null)
			{
				String[] userInfo = line.split(" ");	
				sql = "INSERT INTO UJ (USERID,sid,sRevenue,TIMESTAMP) " +
						"VALUES ("+userInfo[0]+","+userInfo[1]+","+userInfo[2]+","+userInfo[3]+")"; 
				//System.out.println(sql);
				stmt.executeUpdate(sql);
			}
			
			fis = new FileInputStream(Config.INSTANCE.getProjectPath()+Config.INSTANCE.getDatasetFolder()+"joinOutput/NMJoinOperatorMaintainBasedOnIndegreeVOutput.txt");
			br = new BufferedReader(new InputStreamReader(fis));
			line=null;
			while((line=br.readLine())!=null)
			{
				String[] userInfo = line.split(" ");	
				sql = "INSERT INTO inJ (USERID,sid,sRevenue,TIMESTAMP) " +
						"VALUES ("+userInfo[0]+","+userInfo[1]+","+userInfo[2]+","+userInfo[3]+")"; 
				//System.out.println(sql);
				stmt.executeUpdate(sql);
			}
			
			fis = new FileInputStream(Config.INSTANCE.getProjectPath()+Config.INSTANCE.getDatasetFolder()+"joinOutput/NMScoreIndegreeVOutput.txt");
			br = new BufferedReader(new InputStreamReader(fis));
			line=null;
			while((line=br.readLine())!=null)
			{
				String[] userInfo = line.split(" ");	
				sql = "INSERT INTO scoreV (USERID,sid,sRevenue,TIMESTAMP) " +
						"VALUES ("+userInfo[0]+","+userInfo[1]+","+userInfo[2]+","+userInfo[3]+")"; 
				//System.out.println(sql);
				stmt.executeUpdate(sql);
			}
			
			fis = new FileInputStream(Config.INSTANCE.getProjectPath()+Config.INSTANCE.getDatasetFolder()+"joinOutput/NMScoreOutdegreeUOutput.txt");
			br = new BufferedReader(new InputStreamReader(fis));
			line=null;
			while((line=br.readLine())!=null)
			{
				String[] userInfo = line.split(" ");	
				sql = "INSERT INTO scoreU (USERID,sid,sRevenue,TIMESTAMP) " +
						"VALUES ("+userInfo[0]+","+userInfo[1]+","+userInfo[2]+","+userInfo[3]+")"; 
				//System.out.println(sql);
				stmt.executeUpdate(sql);
			}
			
			fis = new FileInputStream(Config.INSTANCE.getProjectPath()+Config.INSTANCE.getDatasetFolder()+"joinOutput/NMScoreIndegreeVFlexibleBudgetOutput.txt");
			br = new BufferedReader(new InputStreamReader(fis));
			line=null;
			while((line=br.readLine())!=null)
			{
				String[] userInfo = line.split(" ");	
				sql = "INSERT INTO fbV (USERID,sid,sRevenue,TIMESTAMP) " +
						"VALUES ("+userInfo[0]+","+userInfo[1]+","+userInfo[2]+","+userInfo[3]+")"; 
				//System.out.println(sql);
				stmt.executeUpdate(sql);
			}
			fis = new FileInputStream(Config.INSTANCE.getProjectPath()+Config.INSTANCE.getDatasetFolder()+"joinOutput/NMJoinOperatorMaintainRandomOutput.txt");
			br = new BufferedReader(new InputStreamReader(fis));
			line=null;
			while((line=br.readLine())!=null)
			{
				String[] userInfo = line.split(" ");	
				sql = "INSERT INTO RandJ (USERID,sid,sRevenue,TIMESTAMP) " +
						"VALUES ("+userInfo[0]+","+userInfo[1]+","+userInfo[2]+","+userInfo[3]+")"; 
				//System.out.println(sql);
				stmt.executeUpdate(sql);
			}
			

			fis = new FileInputStream(Config.INSTANCE.getProjectPath()+Config.INSTANCE.getDatasetFolder()+"joinOutput/NMScoreOutdegreeURemoveEdgeOutput.txt");
			br = new BufferedReader(new InputStreamReader(fis));
			line=null;
			while((line=br.readLine())!=null)
			{
				String[] userInfo = line.split(" ");	
				sql = "INSERT INTO scoreURemove (USERID,sid,sRevenue,TIMESTAMP) " +
						"VALUES ("+userInfo[0]+","+userInfo[1]+","+userInfo[2]+","+userInfo[3]+")"; 
				//System.out.println(sql);
				stmt.executeUpdate(sql);
			}
			
			
			fis.close();
			br.close();		
			stmt.close();
		}catch(Exception e){e.printStackTrace();}
	}
	public static TreeMap<Long,Integer> computeNMOJoin(){
		TreeMap<Long,Integer> result=new TreeMap<Long, Integer>();
		Connection c = null;
		Statement stmt = null;
		try {		
			c = DriverManager.getConnection(Config.INSTANCE.getDatasetDb());	      
			c.setAutoCommit(false);
			stmt = c.createStatement();
			String sql="SELECT JoinOracleJ.TIMESTAMP as TIMESTAMP, count(JoinOracleJ.USERID) as windowcount FROM JoinOracleJ  group by JoinOracleJ.TIMESTAMP order by JoinOracleJ.TIMESTAMP ASC";  //count number of edges per window    
			System.out.println(sql);
			ResultSet rs = stmt.executeQuery( sql);

			while ( rs.next() ) {
				//String revenues  = rs.getString("fundRevenue");
				//String stocks = rs.getString("fundStocks");
				Integer windowEntries=rs.getInt("windowCount");
				Long timeStamp  = rs.getLong("TIMESTAMP");
				if (windowEntries==null);// error=0;
				result.put(timeStamp,windowEntries);
			}
			rs.close();
			stmt.close();
			c.close();
		}catch(Exception e){e.printStackTrace();}
		return result;
	}
	public static HashMap<Long,Integer> computeInJoinPrecision(){
		HashMap<Long,Integer> result=new HashMap<Long, Integer>();
		Connection c = null;
		Statement stmt = null;
		try {		
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(Config.INSTANCE.getDatasetDb());	      
			c.setAutoCommit(false);
			stmt = c.createStatement();
			//String sql="SELECT DWJ.TIMESTAMP AS TIME, SUM(ABS(DWJ.FOLLOWERCOUNT-OJ.FOLLOWERCOUNT)) AS ERROR "+
			//		  " FROM DWJ,OJ  WHERE DWJ.USERID=OJ.USERID AND DWJ.TIMESTAMP=OJ.TIMESTAMP group by OJ.TIMESTAMP";
			String sql="SELECT inJ.TIMESTAMP as TIMESTAMP , COUNT(*) as ERROR FROM inJ,OJ  WHERE inJ.USERID=OJ.USERID AND inJ.sid=OJ.sid AND inJ.TIMESTAMP=OJ.TIMESTAMP AND inJ.sRevenue <> OJ.sRevenue group by OJ.TIMESTAMP";
			System.out.println(sql);
			ResultSet rs = stmt.executeQuery( sql);

			while ( rs.next() ) {
				Integer error = rs.getInt("ERROR");
				Long timeStamp  = rs.getLong("TIMESTAMP");
				if (error==null) error=0;
				result.put(timeStamp,error);
			}
			rs.close();
			stmt.close();
			c.close();
		}catch(Exception e){e.printStackTrace();}
		return result;
	}
	
	
	public static HashMap<Long,Integer> computeUScoreRemoveJoinPrecision(){
		HashMap<Long,Integer> result=new HashMap<Long, Integer>();
		Connection c = null;
		Statement stmt = null;
		try {		
			c = DriverManager.getConnection(Config.INSTANCE.getDatasetDb());	      
			c.setAutoCommit(false);
			stmt = c.createStatement();
			//String sql="SELECT DWJ.TIMESTAMP AS TIME, SUM(ABS(DWJ.FOLLOWERCOUNT-OJ.FOLLOWERCOUNT)) AS ERROR "+
			//		  " FROM DWJ,OJ  WHERE DWJ.USERID=OJ.USERID AND DWJ.TIMESTAMP=OJ.TIMESTAMP group by OJ.TIMESTAMP";
			String sql="SELECT scoreURemove.TIMESTAMP as TIMESTAMP , COUNT(*) as ERROR FROM scoreURemove,OJ  WHERE scoreURemove.USERID=OJ.USERID AND scoreURemove.sid=OJ.sid AND scoreURemove.TIMESTAMP=OJ.TIMESTAMP AND scoreURemove.sRevenue <> OJ.sRevenue group by OJ.TIMESTAMP";
			System.out.println(sql);
			ResultSet rs = stmt.executeQuery( sql);

			while ( rs.next() ) {
				Integer error = rs.getInt("ERROR");
				Long timeStamp  = rs.getLong("TIMESTAMP");
				if (error==null) error=0;
				result.put(timeStamp,error);
			}
			rs.close();
			stmt.close();
			c.close();
		}catch(Exception e){e.printStackTrace();}
		return result;
	}
	
	public static HashMap<Long,Integer> computedrandJoinPrecision(){
		HashMap<Long,Integer> result=new HashMap<Long, Integer>();
		Connection c = null;
		Statement stmt = null;
		try {		
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(Config.INSTANCE.getDatasetDb());	      
			c.setAutoCommit(false);
			stmt = c.createStatement();
			//String sql="SELECT DWJ.TIMESTAMP AS TIME, SUM(ABS(DWJ.FOLLOWERCOUNT-OJ.FOLLOWERCOUNT)) AS ERROR "+
			//		  " FROM DWJ,OJ  WHERE DWJ.USERID=OJ.USERID AND DWJ.TIMESTAMP=OJ.TIMESTAMP group by OJ.TIMESTAMP";
			String sql="SELECT drandJ.TIMESTAMP as TIMESTAMP , COUNT(*) as ERROR FROM drandJ,OJ  WHERE drandJ.USERID=OJ.USERID AND drandJ.sid=OJ.sid AND drandJ.TIMESTAMP=OJ.TIMESTAMP AND drandJ.sRevenue <> OJ.sRevenue group by OJ.TIMESTAMP";
			System.out.println(sql);
			ResultSet rs = stmt.executeQuery( sql);

			while ( rs.next() ) {
				Integer error = rs.getInt("ERROR");
				Long timeStamp  = rs.getLong("TIMESTAMP");
				if (error==null) error=0;
				result.put(timeStamp,error);
			}
			rs.close();
			stmt.close();
			c.close();
		}catch(Exception e){e.printStackTrace();}
		return result;
	}
	public static HashMap<Long,Integer> computelruJoinPrecision(){
		HashMap<Long,Integer> result=new HashMap<Long, Integer>();
		Connection c = null;
		Statement stmt = null;
		try {		
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(Config.INSTANCE.getDatasetDb());	      
			c.setAutoCommit(false);
			stmt = c.createStatement();
			//String sql="SELECT DWJ.TIMESTAMP AS TIME, SUM(ABS(DWJ.FOLLOWERCOUNT-OJ.FOLLOWERCOUNT)) AS ERROR "+
			//		  " FROM DWJ,OJ  WHERE DWJ.USERID=OJ.USERID AND DWJ.TIMESTAMP=OJ.TIMESTAMP group by OJ.TIMESTAMP";
			String sql="SELECT lruJ.TIMESTAMP as TIMESTAMP , COUNT(*) as ERROR FROM lruJ,OJ  WHERE lruJ.USERID=OJ.USERID AND lruJ.sid=OJ.sid AND lruJ.TIMESTAMP=OJ.TIMESTAMP AND lruJ.sRevenue <> OJ.sRevenue group by OJ.TIMESTAMP";
			System.out.println(sql);
			ResultSet rs = stmt.executeQuery( sql);

			while ( rs.next() ) {
				Integer error = rs.getInt("ERROR");
				Long timeStamp  = rs.getLong("TIMESTAMP");
				if (error==null) error=0;
				result.put(timeStamp,error);
			}
			rs.close();
			stmt.close();
			c.close();
		}catch(Exception e){e.printStackTrace();}
		return result;
	}
	public static HashMap<Long,Integer> computeUJoinPrecision(){
		HashMap<Long,Integer> result=new HashMap<Long, Integer>();
		Connection c = null;
		Statement stmt = null;
		try {		
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(Config.INSTANCE.getDatasetDb());	      
			c.setAutoCommit(false);
			stmt = c.createStatement();
			//String sql="SELECT DWJ.TIMESTAMP AS TIME, SUM(ABS(DWJ.FOLLOWERCOUNT-OJ.FOLLOWERCOUNT)) AS ERROR "+
			//		  " FROM DWJ,OJ  WHERE DWJ.USERID=OJ.USERID AND DWJ.TIMESTAMP=OJ.TIMESTAMP group by OJ.TIMESTAMP";
			String sql="SELECT UJ.TIMESTAMP as TIMESTAMP , COUNT(*) as ERROR FROM UJ,OJ  WHERE UJ.USERID=OJ.USERID AND UJ.sid=OJ.sid AND UJ.TIMESTAMP=OJ.TIMESTAMP AND UJ.sRevenue <> OJ.sRevenue group by OJ.TIMESTAMP";
			System.out.println(sql);
			ResultSet rs = stmt.executeQuery( sql);

			while ( rs.next() ) {
				Integer error = rs.getInt("ERROR");
				Long timeStamp  = rs.getLong("TIMESTAMP");
				if (error==null) error=0;
				result.put(timeStamp,error);
			}
			rs.close();
			stmt.close();
			c.close();
		}catch(Exception e){e.printStackTrace();}
		return result;
	}
	public static HashMap<Long,Integer> computeVJoinPrecision(){
		HashMap<Long,Integer> result=new HashMap<Long, Integer>();
		Connection c = null;
		Statement stmt = null;
		try {		
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(Config.INSTANCE.getDatasetDb());	      
			c.setAutoCommit(false);
			stmt = c.createStatement();
			//String sql="SELECT DWJ.TIMESTAMP AS TIME, SUM(ABS(DWJ.FOLLOWERCOUNT-OJ.FOLLOWERCOUNT)) AS ERROR "+
			//		  " FROM DWJ,OJ  WHERE DWJ.USERID=OJ.USERID AND DWJ.TIMESTAMP=OJ.TIMESTAMP group by OJ.TIMESTAMP";
			String sql="SELECT VJ.TIMESTAMP as TIMESTAMP , COUNT(*) as ERROR FROM VJ,OJ  WHERE VJ.USERID=OJ.USERID AND VJ.sid=OJ.sid AND VJ.TIMESTAMP=OJ.TIMESTAMP AND VJ.sRevenue <> OJ.sRevenue group by OJ.TIMESTAMP";
			System.out.println(sql);
			ResultSet rs = stmt.executeQuery( sql);

			while ( rs.next() ) {
				Integer error = rs.getInt("ERROR");
				Long timeStamp  = rs.getLong("TIMESTAMP");
				if (error==null) error=0;
				result.put(timeStamp,error);
			}
			rs.close();
			stmt.close();
			c.close();
		}catch(Exception e){e.printStackTrace();}
		return result;
	}
	public static HashMap<Long,Integer> computeVScoreJoinPrecision(){
		HashMap<Long,Integer> result=new HashMap<Long, Integer>();
		Connection c = null;
		Statement stmt = null;
		try {		
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(Config.INSTANCE.getDatasetDb());	      
			c.setAutoCommit(false);
			stmt = c.createStatement();
			//String sql="SELECT DWJ.TIMESTAMP AS TIME, SUM(ABS(DWJ.FOLLOWERCOUNT-OJ.FOLLOWERCOUNT)) AS ERROR "+
			//		  " FROM DWJ,OJ  WHERE DWJ.USERID=OJ.USERID AND DWJ.TIMESTAMP=OJ.TIMESTAMP group by OJ.TIMESTAMP";
			String sql="SELECT scoreV.TIMESTAMP as TIMESTAMP , COUNT(*) as ERROR FROM scoreV,OJ  WHERE scoreV.USERID=OJ.USERID AND scoreV.sid=OJ.sid AND scoreV.TIMESTAMP=OJ.TIMESTAMP AND scoreV.sRevenue <> OJ.sRevenue group by OJ.TIMESTAMP";
			System.out.println(sql);
			ResultSet rs = stmt.executeQuery( sql);

			while ( rs.next() ) {
				Integer error = rs.getInt("ERROR");
				Long timeStamp  = rs.getLong("TIMESTAMP");
				if (error==null) error=0;
				result.put(timeStamp,error);
			}
			rs.close();
			stmt.close();
			c.close();
		}catch(Exception e){e.printStackTrace();}
		return result;
	}
	public static HashMap<Long,Integer> computeUScoreJoinPrecision(){
		HashMap<Long,Integer> result=new HashMap<Long, Integer>();
		Connection c = null;
		Statement stmt = null;
		try {		
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(Config.INSTANCE.getDatasetDb());	      
			c.setAutoCommit(false);
			stmt = c.createStatement();
			//String sql="SELECT DWJ.TIMESTAMP AS TIME, SUM(ABS(DWJ.FOLLOWERCOUNT-OJ.FOLLOWERCOUNT)) AS ERROR "+
			//		  " FROM DWJ,OJ  WHERE DWJ.USERID=OJ.USERID AND DWJ.TIMESTAMP=OJ.TIMESTAMP group by OJ.TIMESTAMP";
			String sql="SELECT scoreU.TIMESTAMP as TIMESTAMP , COUNT(*) as ERROR FROM scoreU,OJ  WHERE scoreU.USERID=OJ.USERID AND scoreU.sid=OJ.sid AND scoreU.TIMESTAMP=OJ.TIMESTAMP AND scoreU.sRevenue <> OJ.sRevenue group by OJ.TIMESTAMP";
			System.out.println(sql);
			ResultSet rs = stmt.executeQuery( sql);

			while ( rs.next() ) {
				Integer error = rs.getInt("ERROR");
				Long timeStamp  = rs.getLong("TIMESTAMP");
				if (error==null) error=0;
				result.put(timeStamp,error);
			}
			rs.close();
			stmt.close();
			c.close();
		}catch(Exception e){e.printStackTrace();}
		return result;
	}
	public static HashMap<Long,Integer> computeRandJoinPrecision(){
		HashMap<Long,Integer> result=new HashMap<Long, Integer>();
		Connection c = null;
		Statement stmt = null;
		try {		
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(Config.INSTANCE.getDatasetDb());	      
			c.setAutoCommit(false);
			stmt = c.createStatement();
			//String sql="SELECT DWJ.TIMESTAMP AS TIME, SUM(ABS(DWJ.FOLLOWERCOUNT-OJ.FOLLOWERCOUNT)) AS ERROR "+
			//		  " FROM DWJ,OJ  WHERE DWJ.USERID=OJ.USERID AND DWJ.TIMESTAMP=OJ.TIMESTAMP group by OJ.TIMESTAMP";
			String sql="SELECT RandJ.TIMESTAMP as TIMESTAMP , COUNT(*) as ERROR FROM RandJ,OJ  WHERE RandJ.USERID=OJ.USERID AND RandJ.sid=OJ.sid AND RandJ.TIMESTAMP=OJ.TIMESTAMP AND RandJ.sRevenue <> OJ.sRevenue group by OJ.TIMESTAMP";
			System.out.println(sql);
			ResultSet rs = stmt.executeQuery( sql);

			while ( rs.next() ) {
				Integer error = rs.getInt("ERROR");
				Long timeStamp  = rs.getLong("TIMESTAMP");
				if (error==null) error=0;
				result.put(timeStamp,error);
			}
			rs.close();
			stmt.close();
			c.close();
		}catch(Exception e){e.printStackTrace();}
		return result;
	}
	
	public static HashMap<Long,Integer> computefbVJoinPrecision(){
		HashMap<Long,Integer> result=new HashMap<Long, Integer>();
		Connection c = null;
		Statement stmt = null;
		try {		
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(Config.INSTANCE.getDatasetDb());	      
			c.setAutoCommit(false);
			stmt = c.createStatement();
			//String sql="SELECT DWJ.TIMESTAMP AS TIME, SUM(ABS(DWJ.FOLLOWERCOUNT-OJ.FOLLOWERCOUNT)) AS ERROR "+
			//		  " FROM DWJ,OJ  WHERE DWJ.USERID=OJ.USERID AND DWJ.TIMESTAMP=OJ.TIMESTAMP group by OJ.TIMESTAMP";
			String sql="SELECT fbV.TIMESTAMP as TIMESTAMP , COUNT(*) as ERROR FROM fbV,OJ  WHERE fbV.USERID=OJ.USERID AND fbV.sid=OJ.sid AND fbV.TIMESTAMP=OJ.TIMESTAMP AND fbV.sRevenue <> OJ.sRevenue group by OJ.TIMESTAMP";
			System.out.println(sql);
			ResultSet rs = stmt.executeQuery( sql);

			while ( rs.next() ) {
				Integer error = rs.getInt("ERROR");
				Long timeStamp  = rs.getLong("TIMESTAMP");
				if (error==null) error=0;
				result.put(timeStamp,error);
			}
			rs.close();
			stmt.close();
			c.close();
		}catch(Exception e){e.printStackTrace();}
		return result;
	}
	
	public static void main(String[] args){
		NMResultAnalyserByEdges analyzer=new NMResultAnalyserByEdges();
			
		//analyzer.EXP1();
		//analyzer.EXP2();
		analyzer.EXP3();
			/*
			 * 
			try{			
			analyzer.insertMNResultToDB("JoinOperatorMaintainBasedOnIndegreeV");
			analyzer.insertMNResultToDB("JoinOperatorMaintainBasedOnOutdegreeU");
			analyzer.insertMNResultToDB("JoinOperatorMaintainBasedOnWeightV");
			analyzer.insertMNResultToDB("JoinOperatorMaintainLRU");
			analyzer.insertMNResultToDB("JoinOperatorMaintainRandom");
			analyzer.insertMNResultToDB("JoinOperatorMaintainRandomWithrepetition");
			analyzer.insertMNResultToDB("JoinOracle");
			analyzer.insertMNResultToDB("ScoreIndegreeVFlexibleBudget");
			analyzer.insertMNResultToDB("ScoreIndegreeV");
			analyzer.insertMNResultToDB("ScoreOutdegreeU");
			analyzer.insertMNResultToDB("ScoreOutdegreeURemoveEdge");
			
			TreeMap<Long,Integer> oracleNMCount=computeNMOJoin();
			
			HashMap<Long, Integer> JoinOperatorMaintainBasedOnIndegreeVE=analyzer.computeJoinPrecision("JoinOperatorMaintainBasedOnIndegreeV");
			HashMap<Long, Integer> JoinOperatorMaintainBasedOnOutdegreeUE=analyzer.computeJoinPrecision("JoinOperatorMaintainBasedOnOutdegreeU");
			HashMap<Long, Integer> JoinOperatorMaintainBasedOnWeightVE= analyzer.computeJoinPrecision("JoinOperatorMaintainBasedOnWeightV");
			HashMap<Long, Integer> JoinOperatorMaintainLRUE= analyzer.computeJoinPrecision("JoinOperatorMaintainLRU");
			HashMap<Long, Integer> JoinOperatorMaintainRandomE= analyzer.computeJoinPrecision("JoinOperatorMaintainRandom");
			HashMap<Long, Integer> JoinOperatorMaintainRandomWithrepetitionE= analyzer.computeJoinPrecision("JoinOperatorMaintainRandomWithrepetition");
			HashMap<Long, Integer> ScoreIndegreeVFlexibleBudgetE=analyzer.computeJoinPrecision("ScoreIndegreeVFlexibleBudget");
			HashMap<Long, Integer> ScoreIndegreeVE=analyzer.computeJoinPrecision("ScoreIndegreeV");
			HashMap<Long, Integer> ScoreOutdegreeUE= analyzer.computeJoinPrecision("ScoreOutdegreeU");
			HashMap<Long, Integer> ScoreOutdegreeURemoveEdgeE=analyzer.computeJoinPrecision("ScoreOutdegreeURemoveEdge");
			
			
			
			BufferedWriter bw=new BufferedWriter(new FileWriter(new File(Config.INSTANCE.getProjectPath()+Config.INSTANCE.getDatasetFolder()+"joinOutput/NMcomparebyEdge.csv")));
			
			Iterator<Long> itO = oracleNMCount.keySet().iterator();

			bw.write("timestampe,oracle,in,U,V, Vscore, Uscore, fbV,rand,drand,lru, uScoreRemove \n");
			while(itO.hasNext()){
				long nextTime = itO.next();
				Integer OC=oracleNMCount.get(nextTime);
				Integer JoinOperatorMaintainBasedOnIndegreeVJE=JoinOperatorMaintainBasedOnIndegreeVE.get(nextTime);
				Integer JoinOperatorMaintainBasedOnOutdegreeUJE=JoinOperatorMaintainBasedOnOutdegreeUE.get(nextTime);
				Integer JoinOperatorMaintainBasedOnWeightVJE=JoinOperatorMaintainBasedOnWeightVE.get(nextTime);
				Integer JoinOperatorMaintainLRUJE=JoinOperatorMaintainLRUE.get(nextTime);
				Integer JoinOperatorMaintainRandomJE=JoinOperatorMaintainRandomE.get(nextTime);
				Integer JoinOperatorMaintainRandomWithrepetitionJE=JoinOperatorMaintainRandomWithrepetitionE.get(nextTime);
				Integer ScoreIndegreeVFlexibleBudgetJE=ScoreIndegreeVFlexibleBudgetE.get(nextTime);
				Integer ScoreIndegreeVJE=ScoreIndegreeVE.get(nextTime);
				Integer ScoreOutdegreeUJE=ScoreOutdegreeUE.get(nextTime);
				Integer ScoreOutdegreeURemoveEdgeJE=ScoreOutdegreeURemoveEdgeE.get(nextTime);
							
				bw.write(nextTime+","+OC+","+(JoinOperatorMaintainBasedOnIndegreeVJE==null?0:JoinOperatorMaintainBasedOnIndegreeVJE)+","
				+(JoinOperatorMaintainBasedOnOutdegreeUJE==null?0:JoinOperatorMaintainBasedOnOutdegreeUJE)+","
						+(JoinOperatorMaintainBasedOnWeightVJE==null?0:JoinOperatorMaintainBasedOnWeightVJE)+","
				+(JoinOperatorMaintainLRUJE==null?0:JoinOperatorMaintainLRUJE)+","
						+(JoinOperatorMaintainRandomJE==null?0:JoinOperatorMaintainRandomJE)+","
				+(JoinOperatorMaintainRandomWithrepetitionJE==null?0:JoinOperatorMaintainRandomWithrepetitionJE)+","
						+(ScoreIndegreeVFlexibleBudgetJE==null?0:ScoreIndegreeVFlexibleBudgetJE)+","
				+(ScoreIndegreeVJE==null?0:ScoreIndegreeVJE)+","
						+(ScoreOutdegreeUJE==null?0:ScoreOutdegreeUJE)+","
				+(ScoreOutdegreeURemoveEdgeJE==null?0:ScoreOutdegreeURemoveEdgeJE)+" \n");				
			}
			bw.flush();
			bw.close();
		}catch(Exception e){e.printStackTrace();}*/
			
	}
}
