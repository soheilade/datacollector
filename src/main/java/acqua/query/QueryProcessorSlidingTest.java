package acqua.query;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import acqua.*;
import acqua.config.Config;
import acqua.data.RandomStreamParser;
import acqua.data.SlidingWindowFromStreamGeneratorFile;
import acqua.data.TwitterStreamCollector;
import acqua.query.join.*;
import acqua.query.join.bkg1.NMJoinOracle;
import acqua.query.join.bkg1.DWJoinOperator;
import acqua.query.join.bkg1.GNRUpperBound;
import acqua.query.join.bkg1.LRUWithOutWindowsLocality;
import acqua.query.join.bkg1.OracleJoinOperator;
import acqua.query.join.bkg1.OETJoinOperator;
import acqua.query.join.bkg1.LRUJoinOperator;
import acqua.query.join.bkg1.PrefectSlidingOET;
import acqua.query.join.bkg1.RandomCacheUpdateJoin;
import acqua.query.join.bkg1.RandomWithOutWindowsLocality;
import acqua.query.join.bkg1.SlidingOETJoinOperator;
import acqua.query.join.bkg1.WSJUpperBound;
import acqua.query.join.bkg1.subgraphStockIndegree;
import acqua.query.join.bkg2.OracleDoubleJoinOperator;
import acqua.query.join.bkg2.DoubleBkgJoinOperator;
import acqua.query.join.nmjoin.impl.NMJoinOperatorMaintainBasedOnIndegreeV;
import acqua.query.join.nmjoin.impl.NMJoinOperatorMaintainBasedOnOutdegreeU;
import acqua.query.join.nmjoin.impl.NMJoinOperatorMaintainBasedOnWeightV;
import acqua.query.join.nmjoin.impl.NMScoreIndegreeVFlexibleBudget;
import acqua.query.join.nmjoin.impl.OOFixedBudget;
import acqua.query.join.nmjoin.impl.OOFlexibleBudget;
import acqua.query.window.Entry;
import acqua.query.window.SlidingWindow;
import acqua.query.window.Window;

public class QueryProcessorSlidingTest {
	JoinOperator join;
	TwitterStreamCollector tsc;
	RandomStreamParser rsp;
	ArrayList<HashMap<Long, Integer>> slidedwindows;
	ArrayList<HashMap<Long, Long>> slidedwindowsTime;

	private static final Logger logger = LoggerFactory.getLogger("QueryProcessorSlidingTest");

	// HashMap<Long, Integer> initialCache;
	// public static long start=1416244306470L;//select min(TIMESTAMP) + 30000 from BKG
	// public static int windowSize=60;
	public QueryProcessorSlidingTest() {// class JoinOperator){
		// ----------twitter stream parser
		/*tsc= new TwitterStreamCollector();
		//tsc.extractWindow(Config.INSTANCE.getQueryWindowWidth(), Config.INSTANCE.getProjectPath()+Config.INSTANCE.getDatasetFolder()+"twitterStream.txt");		
		tsc.extractSlides(Config.INSTANCE.getQueryWindowWidth(),Config.INSTANCE.getQueryWindowSlide(), Config.INSTANCE.getProjectPath()+Config.INSTANCE.getDatasetFolder()+"twitterStream.txt");
		slidedwindows = tsc.aggregateSildedWindowsUser();
		slidedwindowsTime=tsc.aggregateSildedWindowsUserTime();*/
		// ----------------------random fund stream parser
		// rsp = new RandomStreamParser();
		// rsp.extractSlides(Config.INSTANCE.getQueryWindowWidth(), Config.INSTANCE.getQueryWindowSlide(), Config.INSTANCE.getProjectPath() + "data/stream/stream-1428595804910.csv");
		// slidedwindows = rsp.aggregateSildedWindowsFund();
		// slidedwindowsTime = rsp.aggregateSildedWindowsFundTime();
		// ------------------------
	}

	public void evaluateQuery(int joinType) {
		if (joinType == 1)
			join = new OracleJoinOperator();
		if (joinType == 2)
			join = new DWJoinOperator();
		if (joinType == 3)
			join = new LRUJoinOperator(Config.INSTANCE.getUpdateBudget());// update budget of 10
		if (joinType == 4)
			join = new RandomCacheUpdateJoin(Config.INSTANCE.getUpdateBudget());
		if (joinType == 5)
			join = new SlidingOETJoinOperator(Config.INSTANCE.getUpdateBudget(), true);
		if (joinType == 6)
			join = new PrefectSlidingOET(Config.INSTANCE.getUpdateBudget(), true);
		if (joinType == 7)
			join = new SlidingOETJoinOperator(Config.INSTANCE.getUpdateBudget(), false);
		if (joinType == 8)
			join = new PrefectSlidingOET(Config.INSTANCE.getUpdateBudget(), false);
		if (joinType == 9)
			join = new RandomWithOutWindowsLocality(Config.INSTANCE.getUpdateBudget());
		if (joinType == 10)
			join = new LRUWithOutWindowsLocality(Config.INSTANCE.getUpdateBudget());
		if (joinType == 11)
			join = new WSJUpperBound(Config.INSTANCE.getUpdateBudget());
		if (joinType == 12)
			join = new GNRUpperBound(Config.INSTANCE.getUpdateBudget());
		if (joinType == 13)
			join = new NMJoinOracle();
		if (joinType == 14)
			join = new NMJoinOperatorMaintainBasedOnOutdegreeU();

		if (joinType == 15)
			join = new NMJoinOperatorMaintainBasedOnWeightV();

		if (joinType == 16)
			join = new NMJoinOperatorMaintainBasedOnIndegreeV();
		if (joinType == 17)
			join = new OOFlexibleBudget(Config.INSTANCE.getUpdateBudget());
		if (joinType == 18)
			join = new OOFixedBudget(Config.INSTANCE.getUpdateBudget());
		if (joinType == 19)
			join = new NMScoreIndegreeVFlexibleBudget(Config.INSTANCE.getUpdateBudget());

		int windowCount = 0;
		SlidingWindow sw = new SlidingWindowFromStreamGeneratorFile(Config.INSTANCE.getQueryWindowWidth(), Config.INSTANCE.getQueryWindowSlide(), Config.INSTANCE.getQueryStartingTime(), Config.INSTANCE.getProjectPath() + Config.INSTANCE.getStreamFilePath());
		while (sw.hasNext()) {
			logger.debug("window: " + windowCount);
			Window w = sw.next();
			join.process(w.getEndingTime(), w.getFrequencyOfEntitiesAsMap(), w.getDistinctEntriesAsMap());// TwitterFollowerCollector.getInitialUserFollowersFromDB());//
			if (windowCount >= 200) {
				break;
			}
			windowCount++;
		}
		join.close();
	}

	public static void main(String[] args) {
		QueryProcessorSlidingTest qp = new QueryProcessorSlidingTest();

		//qp.evaluateQuery(13);
		//qp.evaluateQuery(18);
		
		//qp.evaluateQuery(17);
		
		qp.evaluateQuery(16);
		
		//qp.evaluateQuery(13);
		
		//qp.evaluateQuery(19);

		// for (int i = 13; i < 17; i++) {
		// // System.out.println(i);
		// qp.evaluateQuery(i);
		// }

	}
}
