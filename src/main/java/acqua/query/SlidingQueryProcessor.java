package acqua.query;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;

import acqua.config.Config;
import acqua.data.TwitterStreamCollector;
import acqua.query.join.JoinOperator;
import acqua.query.join.bkg1.OETSlidingJoinOperator;
import acqua.query.join.bkg1.SlidingOETJoinOperator;

public class SlidingQueryProcessor {
	JoinOperator join;
	TwitterStreamCollector tsc;
	ArrayList<HashMap<Long, Integer>> slidedwindows;
	ArrayList<HashMap<Long, Long>> slidedwindowsTime;

	public SlidingQueryProcessor() {
		// tsc= new TwitterStreamCollector();
		// tsc.extractSlides(Config.INSTANCE.getQueryWindowWidth(),Config.INSTANCE.getQueryWindowSlide(), Config.INSTANCE.getProjectPath()+Config.INSTANCE.getDatasetFolder()+"twitterStream.txt");
		// slidedwindows=tsc.aggregateSildedWindowsUser();
		// slidedwindowsTime=tsc.aggregateSildedWindowsUserTime();

	}

	public void evaluateQuery(int joinType) {
		if (joinType == 1)
			join = new OETSlidingJoinOperator(Config.INSTANCE.getUpdateBudget());
		if (joinType == 7)
			join = new SlidingOETJoinOperator(Config.INSTANCE.getUpdateBudget(), true);
		long time = Config.INSTANCE.getQueryStartingTime() + Config.INSTANCE.getQueryWindowWidth() * 1000;
		int windowCount = 0;
		// ArrayList<HashMap<Long, Integer>> slidedWindows = tsc.aggregateSildedWindowsUser();
		while (windowCount < 50) {
			// System.out.println(tsc.windows.get(windowCount).size());
			HashMap<Long, Long> currentCandidateTimeStamp = slidedwindowsTime.get(windowCount);
			// currentCandidateTimeStamp.put(-1L, time);
			join.process(time, slidedwindows.get(windowCount), currentCandidateTimeStamp);// TwitterFollowerCollector.getInitialUserFollowersFromDB());//
			windowCount++;
			time = time + Config.INSTANCE.getQueryWindowSlide() * 1000;
		}
		join.close();

	}

	public static void main(String[] args) {
		SlidingQueryProcessor qp = new SlidingQueryProcessor();
		qp.evaluateQuery(1);
		qp.evaluateQuery(2);

	}
}
