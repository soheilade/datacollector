package acqua.query.join.bkg1;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import acqua.config.Config;
import acqua.data.TwitterFollowerCollector;
import acqua.query.join.bkg1.OETJoinOperator.User;

public class OETSlidingJoinOperator extends ApproximateJoinOperator{

	protected int updateBudget;
	protected HashMap<Long,Float> userChangeRates; 
	private HashMap<Long, Long> userExactExp;
	private double hitratioWithinUpdateBudget;
	private double windowHitratio;
	//query to create user(userid,changeRate) Table 
	//select bkg.USERID as USERID, cast(count(distinct(bkg.FOLLOWERCOUNT)) as real)/cast(count(distinct(bkg.tiMESTAMP)) as real) AS CHANGERATE from bkg group by bkg.USERID
	public OETSlidingJoinOperator(int ub) {
		updateBudget=ub;
		userChangeRates=new HashMap<Long, Float>();
		Connection c = null;
		Statement stmt = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(Config.INSTANCE.getDatasetDb());
			c.setAutoCommit(false);
			stmt = c.createStatement();
			stmt.executeUpdate("Drop table IF EXISTS User");
			//String sql = "CREATE TABLE  `User` as select bkg.USERID as USERID, cast(count(distinct(bkg.FOLLOWERCOUNT)) as real)/cast(count(distinct(bkg.tiMESTAMP)) as real) AS CHANGERATE from bkg group by bkg.USERID";
			stmt.executeUpdate("create table User ( USERID BIGINT, CHANGERATE real);");
		    String sql="insert into User SELECT A.USERID , round(cast(COUNT(*) as real)/cast((SELECT COUNT(TIMESTAMP) FROM BKG C WHERE C.USERID = A.USERID) as real),4) FROM BKG A, BKG B WHERE A.TIMESTAMP-B.TIMESTAMP>0 AND A.TIMESTAMP-B.TIMESTAMP< 100000 AND A.USERID = B.USERID AND A.FOLLOWERCOUNT<>B.FOLLOWERCOUNT GROUP BY A.USERID";
			stmt.execute(sql);

			sql="SELECT USERID, CHANGERATE from User ";
			//sql="SELECT userid, changeRate from FollowerLowChr ";
			//CREATE TABLE FollowerLowChr AS SELECT copyBK.uSERID as userid, CAST(CAST(count(distinct copyBK.FollowerCut) as float)/30 AS FLOAT) as changeRate from copyBK group by copyBK.uSERID
			System.out.println(sql);
			ResultSet rs = stmt.executeQuery( sql);

			while ( rs.next() ) {
				long userId = rs.getLong("USERID");
				float userChangeRate  = rs.getFloat("CHANGERATE");
				userChangeRates.put(userId, userChangeRate);
			}
			rs.close();
			stmt.close();
			c.close();
		} catch ( Exception e ) {
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			System.exit(0);
		}
		// TODO Auto-generated constructor stub
	}


	private HashMap<Long,Double> computeLife(Map<Long,Long> usersTimeStampOfTheCurrentSlidedWindow, long evaluationTime){
		HashMap<Long,Double> results=new HashMap<Long, Double>();
		Iterator<Long> usersIt= usersTimeStampOfTheCurrentSlidedWindow.keySet().iterator();
		while(usersIt.hasNext()){
			Long UserId=usersIt.next();
			Double num=java.lang.Math.ceil((usersTimeStampOfTheCurrentSlidedWindow.get(UserId)+Config.INSTANCE.getQueryWindowWidth()*1000-evaluationTime) / (Config.INSTANCE.getQueryWindowSlide()*1000));
			results.put(UserId, num);			
		}
		return results;
	}

	private HashMap<Long,Double> computeFutureValidWindows (Map<Long,Long> usersTimeStampOfTheCurrentSlidedWindow, long evaluationTime){
		HashMap<Long,Double> results=new HashMap<Long, Double>();
		HashMap<Long,Double> exactVresults=new HashMap<Long, Double>();
		userExactExp=new HashMap<Long, Long>();
		Iterator<Long> usersIt= usersTimeStampOfTheCurrentSlidedWindow.keySet().iterator();
		while(usersIt.hasNext()){
			Long UserId=usersIt.next();
			System.out.println(UserId);
			//long tau = Long.parseLong(userLastChangeTime.get(UserId).toString());//latestUpdateTime
			long tau = Long.parseLong(userInfoUpdateTime.get(UserId).toString());//latestUpdateTime			
			float f=1;
			try{
				f= Float.parseFloat(userChangeRates.get(UserId).toString());
			}catch(Exception ee){ee.printStackTrace(); 
			System.out.println("couldn't find the change rate of "+UserId+ " and skip this user because it will not expire"); continue;}
			Double K = Math.ceil((evaluationTime-tau)*f/60000)+1;
			Double num=java.lang.Math.ceil((double)(tau+K*60000/f-evaluationTime) / (double)(Config.INSTANCE.getQueryWindowSlide()*1000));
			Long exactEXP=TwitterFollowerCollector.getUserNextExpFromDB(tau, UserId);
			userExactExp.put(UserId, exactEXP);
			Double exactV= java.lang.Math.ceil((double)(exactEXP-evaluationTime)/(double)(Config.INSTANCE.getQueryWindowSlide()*1000));
			exactVresults.put(UserId,exactV);
			results.put(UserId, num);			
		}
		//-------************************************************************************************
		//-------******DECIDE IF TO USE EXACT OR PREDICTED EXPIRATION TIME************
		//-------************************************************************************************
		//return results;
		return exactVresults;
		//************************************************************************************
	}
	
	public ArrayList<User> computeUsersInfo(Map<Long,Long> usersTimeStampOfTheCurrentSlidedWindow, long evaluationTime){
		ArrayList<User> result=new ArrayList<OETSlidingJoinOperator.User>();
		Iterator<Long> usersIt= usersTimeStampOfTheCurrentSlidedWindow.keySet().iterator();
		while(usersIt.hasNext()){
			Long UserId=usersIt.next();
			Double streamLife=java.lang.Math.ceil((usersTimeStampOfTheCurrentSlidedWindow.get(UserId)+Config.INSTANCE.getQueryWindowWidth()*1000-evaluationTime) / (Config.INSTANCE.getQueryWindowSlide()*1000));
			long tau = Long.parseLong(userInfoUpdateTime.get(UserId).toString());//latestUpdateTime			
			//long tau = Long.parseLong(userLastChangeTime.get(UserId).toString());//latestUpdateTime
			float f=1;
			try{
				f= Float.parseFloat(userChangeRates.get(UserId).toString());
			}catch(Exception ee){ee.printStackTrace(); System.out.println("couldn't find the change rate of "+UserId+ " and skip this user because it will not expire"); continue;}
			Double numberOfChangeRateIntervalPassedAfterLastUpdate = Math.ceil((evaluationTime-tau)*f/60000)+1;
			Long nextExpTime=tau+(long)((float)(numberOfChangeRateIntervalPassedAfterLastUpdate*60000)/f);
			Double bkgLife=java.lang.Math.ceil((double)(nextExpTime-evaluationTime) / (double)(Config.INSTANCE.getQueryWindowSlide()*1000));
			System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+bkgLife);
			Long exactEXP=TwitterFollowerCollector.getUserNextExpFromDB(tau, UserId);
			//userExactExp.put(UserId, exactEXP);
			Double exactV= java.lang.Math.ceil((double)(exactEXP-evaluationTime)/(double)(Config.INSTANCE.getQueryWindowSlide()*1000));
			result.add(new User(UserId,nextExpTime,exactEXP,streamLife,bkgLife));
		}
		return result;
	}

	@Override
	protected HashMap<Long,String> updatePolicy(Iterator<Long> candidateUserSetIterator, Map<Long,Long> usersTimeStampOfTheCurrentSlidedWindow, long evaluationTime) {
		ArrayList<User> userTimeInfo=computeUsersInfo(usersTimeStampOfTheCurrentSlidedWindow, evaluationTime);
		ArrayList<User> estimatedExpiredUsers=new ArrayList<OETSlidingJoinOperator.User>();
		ArrayList<User> notExpiredUsers=new ArrayList<OETSlidingJoinOperator.User>();
		HashSet<Long> actuallyExpiredUsers=new HashSet<Long>();
		int count=0;
		for(int i=0;i<userTimeInfo.size();i++){
			count++;
			User userTempe =userTimeInfo.get(i);
			if(isStale(evaluationTime, userTempe.userId))actuallyExpiredUsers.add(userTempe.userId);
			
			//-------************************************************************************************
			//-------******ALTERNATING AMONG SOET1 or SOET2(last update time is the time of updating replica or time of updating replica if there was a change )************
			//-------************************************************************************************
			//long latestUpdateTime = Long.parseLong(userInfoUpdateTime.get(userid).toString());
			long latestUpdateTime = Long.parseLong(estimatedLastChangeTime.get(userTempe.userId).toString());
			//************************************************************************************************
			//-------************************************************************************************
			//-------******ALTERNATING AMONG EXACT OR ESTIMATED EXPIRATION TIME ***********************
			//-------************************************************************************************
			if(userTempe.expirationTime<evaluationTime){//if user has been expired already
			//if(userTempe.exactExpTime<evaluationTime){//if user has been expired already
			//************************************************************************************************					
				estimatedExpiredUsers.add(userTempe);
			}
			else{
				//-------************************************************************************************
				//-------******ALTERNATING AMONG EXACT OR ESTIMATED EXPIRATION TIME ***********************
				//-------************************************************************************************
				//userNotExpired.add(new User(userid, expectedExpirationTime));
				notExpiredUsers.add(userTempe);
				//************************************************************************************************					
			}
				
		}
		
		

		Collections.sort(estimatedExpiredUsers, new Comparator<User>() {
			public int compare(User o1, User o2) {
				int res=(int)(Math.min(o1.nextStreamWindowCount,o1.nextValidWindowCount)-Math.min(o2.nextStreamWindowCount,o2.nextValidWindowCount));
				if(res==0) return (int)(o1.nextValidWindowCount-o2.nextValidWindowCount);
				else return res;
			}
		});
		HashMap<Long,String> result=new HashMap<Long,String>();
		Iterator<User> it = estimatedExpiredUsers.iterator();
		int counter=0;
		int countHit=0;
		while(it.hasNext()&&counter<updateBudget){
			User temp=it.next();
			long currentValue = this.followerReplica.get(temp.userId);
			long bkgValue = TwitterFollowerCollector.getUserFollowerFromDB(evaluationTime, temp.userId);
			if(currentValue!=bkgValue)
				{
					result.put(temp.userId,"<>");
					System.out.println(temp.userId+">>>"+ currentValue+ " <> "+bkgValue);
				}
			else
				{
					result.put(temp.userId,"=");
					System.out.println(temp.userId+">>>"+ currentValue+ " == "+bkgValue+" " + " at " + temp.exactExpTime);
				}
			
			//if(temp.exactExpTime==0) continue;
			if(actuallyExpiredUsers.contains(temp.userId)) countHit++;
			//result.add(temp.userId);
			counter++;
		}
		if(result.size()<updateBudget){
			Collections.sort(notExpiredUsers, new Comparator<User>() {
		        public int compare(User o1, User o2) {
		            return (int)(o1.expirationTime - o2.expirationTime);//users with the latest expiration time will come first and will be chosen
		        }
		    });
			it = notExpiredUsers.iterator();
			//counter=result.size();
			while(it.hasNext()&&counter<updateBudget){
				User temp=it.next();
				long currentValue = this.followerReplica.get(temp.userId);
				long bkgValue = TwitterFollowerCollector.getUserFollowerFromDB(evaluationTime, temp.userId);
				if(currentValue!=bkgValue)
					{
						result.put(temp.userId,"<>");
						System.out.println(temp.userId+">>>"+ currentValue+ " <> "+bkgValue);
					}
				else
					{
						result.put(temp.userId,"=");
						System.out.println(temp.userId+">>>"+ currentValue+ " == "+bkgValue+" " + " at " + temp.expirationTime);
					}
				//if(temp.exactExpTime==0) continue;
				//System.out.println("user Id: "+temp.userId+" "+"Expiration Time: "+temp.expirationTime);
				if(actuallyExpiredUsers.contains(temp.userId)) countHit++;
				//result.add(temp.userId);
				counter++;
			}
		}
		hitratioWithinUpdateBudget=(double)countHit/(double)counter;
		it =  estimatedExpiredUsers.iterator();
		counter=countHit=0;
		while(it.hasNext()){
			User temp=it.next();
			counter++;
			if(actuallyExpiredUsers.contains(temp.userId))
					countHit++;
		}
		windowHitratio=(double)countHit/(double)counter;
		try {
			statsFileWriter.write(estimatedExpiredUsers.size()+","+hitratioWithinUpdateBudget+","+windowHitratio);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("--------------------------------------------------");
		return result;
	}
	final class User{
		long userId;
		long expirationTime;
		long exactExpTime;
		double nextStreamWindowCount;
		double nextValidWindowCount;
		public User(long id,double sc,double vc){
			userId=id;
			nextStreamWindowCount=sc;
			nextValidWindowCount=vc;
		}
		public User(long id,long expirationTime){
			userId=id;
			this.expirationTime=expirationTime;
		}
		public User(long id,long expirationTime,long exactEXP, double sc,double vc){
			userId=id;
			this.expirationTime=expirationTime;
			this.exactExpTime=exactEXP;
			this.nextStreamWindowCount=sc;
			this.nextValidWindowCount=vc;
		}
	}

	
}
