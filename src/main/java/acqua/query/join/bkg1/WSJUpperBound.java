package acqua.query.join.bkg1;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import acqua.config.Config;
import acqua.data.TwitterFollowerCollector;

public class WSJUpperBound extends ApproximateJoinOperator{
	protected int updateBudget;
	private long currentTimestamp;
	
	public WSJUpperBound(int ub){
		updateBudget=ub;
	}
	public void process(long timeStamp, Map<Long, Integer> mentionList, Map<Long,Long> usersTimeStampOfTheCurrentSlidedWindow) {
		currentTimestamp =  timeStamp;
		super.process(timeStamp, mentionList,usersTimeStampOfTheCurrentSlidedWindow);
	}
	private double getchangerate(long userid){
		double cr=0;
		Connection c = null;
		Statement stmt = null;
		try {
			//System.out.println("start of user follower count:");
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(Config.INSTANCE.getDatasetDb());
			c.setAutoCommit(false);
			stmt = c.createStatement();
			String sql="SELECT changerate FROM User WHERE USERID = "+userid;  

			ResultSet rs = stmt.executeQuery(sql );	      
			while ( rs.next() ) {
				cr  = rs.getDouble("changerate");	         
			}
			rs.close();
			stmt.close();
			c.close();
		} catch ( Exception e ) {
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			System.exit(0);
		}
		return cr;
	}
	@Override
	protected HashMap<Long, String> updatePolicy(Iterator<Long> CandidateIds,
			Map<Long, Long> candidateUserSetIterator, long evaluationTime) {
		// TODO Auto-generated method stub
		final class User{
			long userId;
			long updateTimeDiff;
			public User(long id,long t){userId=id;updateTimeDiff=t;}
		}
		List<User> userUpdateLatency=new ArrayList<User>();
		while(CandidateIds.hasNext()){
			long userid=CandidateIds.next();
			long latestUpdateTime = userInfoUpdateTime.get(userid);
			//long latestUpdateTime = Long.parseLong(bkgLastChangeTime.get(userid).toString());
			userUpdateLatency.add(new User(userid, currentTimestamp-latestUpdateTime));				
		}
		Collections.sort(userUpdateLatency, new Comparator<User>() {

			public int compare(User o1, User o2) {
				int res=(int)(o2.updateTimeDiff - o1.updateTimeDiff);
				//if(res==0)
				//	res=(int)(o2.updateTimeDiff - o1.updateTimeDiff);
				return res;
			}
		});
		HashMap<Long,String> result=new HashMap<Long,String>();
		Iterator<User> it = userUpdateLatency.iterator();
		int counter=0;
		while(it.hasNext()&&counter<updateBudget){
			User temp = it.next();
			double replicaValue=followerReplica.get(temp.userId);
			double bkgValue=TwitterFollowerCollector.getUserFollowerFromDB(evaluationTime, temp.userId);
			if(replicaValue==bkgValue)
				{
				//result.put(temp.userId,"=");
				}
			else 
				{
					result.put(temp.userId, "<>");
					counter++;
				}
			//System.out.printf("id "+temp.userId+">>oldness "+temp.updateTimeDiff/60000+"  cr= "+getchangerate(temp.userId)+" chachedValue>> "+ replicaValue+ " actualValue>> "+bkgValue+" \n",(evaluationTime - temp.updateTimeDiff)/60000);
			
		}
		/*System.out.println("skipped users: ");
		while(it.hasNext()){
			User temp = it.next();
			double replicaValue=followerReplica.get(temp.userId);
			double bkgValue=TwitterFollowerCollector.getUserFollowerFromDB(evaluationTime, temp.userId);
			System.out.printf("id "+temp.userId+">>oldness "+temp.updateTimeDiff/60000+"  cr= "+getchangerate(temp.userId)+" chachedValue>> "+ replicaValue+ " actualValue>> "+bkgValue+" \n",(evaluationTime - temp.updateTimeDiff)/60000);
		}
		System.out.println("time"+(evaluationTime-Config.INSTANCE.getQueryStartingTime())/60000+"--------------------------------------------------------------------------------------------------------------------");
		*/return result;
	}
	
}
