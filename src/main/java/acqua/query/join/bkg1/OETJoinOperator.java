package acqua.query.join.bkg1;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import acqua.config.Config;
import acqua.data.TwitterFollowerCollector;
import acqua.query.join.bkg1.SlidingOETJoinOperator.User;

public class OETJoinOperator extends ApproximateJoinOperator{
	protected int updateBudget;
	protected HashMap<Long,Float> userChangeRates; 
//	private HashMap<Long, Long> userExactExp;
	private double hitratioWithinUpdateBudget;
	private double windowHitratio;
	//query to create user(userid,changeRate) Table 
	//select bkg.USERID as USERID, cast(count(distinct(bkg.FOLLOWERCOUNT)) as real)/cast(count(distinct(bkg.tiMESTAMP)) as real) AS CHANGERATE from bkg group by bkg.USERID
	
	public OETJoinOperator(int ub) {
		updateBudget=ub;
		userChangeRates=new HashMap<Long, Float>();
		Connection c = null;
	    Statement stmt = null;
	    try {
	      Class.forName("org.sqlite.JDBC");
	      c = DriverManager.getConnection(Config.INSTANCE.getDatasetDb());
	      c.setAutoCommit(false);
	      stmt = c.createStatement();
	      stmt.executeUpdate("Drop table IF EXISTS User");
	      //String sql = "CREATE TABLE  `User` as select bkg.USERID as USERID, cast(count(distinct(bkg.FOLLOWERCOUNT)) as real)/cast(count(distinct(bkg.tiMESTAMP)) as real) AS CHANGERATE from bkg group by bkg.USERID";
	      stmt.executeUpdate("create table User ( USERID BIGINT, CHANGERATE real);");
	      String sql="insert into User SELECT A.USERID , round(cast(COUNT(*) as real)/cast((SELECT COUNT(TIMESTAMP) FROM BKG C WHERE C.USERID = A.USERID) as real),4) FROM BKG A, BKG B WHERE A.TIMESTAMP-B.TIMESTAMP>0 AND A.TIMESTAMP-B.TIMESTAMP< 100000 AND A.USERID = B.USERID AND A.FOLLOWERCOUNT<>B.FOLLOWERCOUNT GROUP BY A.USERID";
		  stmt.execute(sql);
	      
	      sql="SELECT USERID, CHANGERATE from User ";
	      //sql="SELECT userid, changeRate from FollowerLowChr ";
	      //CREATE TABLE FollowerLowChr AS SELECT copyBK.uSERID as userid, CAST(CAST(count(distinct copyBK.FollowerCut) as float)/30 AS FLOAT) as changeRate from copyBK group by copyBK.uSERID
	      System.out.println(sql);
	      ResultSet rs = stmt.executeQuery( sql);
	      
	      while ( rs.next() ) {
	         long userId = rs.getLong("USERID");
	         float userChangeRate  = rs.getFloat("CHANGERATE");
	         userChangeRates.put(userId, userChangeRate);
	      }
	      rs.close();
	      stmt.close();
	      c.close();
	    } catch ( Exception e ) {
	      System.err.println( e.getClass().getName() + ": " + e.getMessage() );
	      System.exit(0);
	    }
		// TODO Auto-generated constructor stub
	}
	
	protected HashMap<Long,String> updatePolicy(Iterator<Long> candidateUserSetIterator,Map<Long,Long> usersTimeStampOfTheCurrentSlidedWindow, final long evaluationTime){
		//decide which rows to update and return the list
		//it must satisfy the updateBudget constraint!
		
		//the estimated expiration times
		List<User> userEstimatedCurrentExpirationTime=new ArrayList<User>();
		//number of expired users |X|
		int estimatedExpired = 0;

		//the real expiration times (for test purposes
		List<User> userExactExpirationTime=new ArrayList<User>();
		//the real expired element set rX
		HashSet<Long> actuallyExpiredUsers=new HashSet<Long>();
		
		//the return structure
		HashMap<Long,String> result=new HashMap<Long,String>();
		
		/********************************
		 * Compute the expiration times *
		 ********************************/
		while(candidateUserSetIterator.hasNext()){
			long userid=candidateUserSetIterator.next();
			
			if(isStale(evaluationTime, userid))actuallyExpiredUsers.add(userid);
			
			//for test purposes
			float f=1;
			try{
				f= userChangeRates.get(userid);
			}catch(Exception ee){
				ee.printStackTrace(); 
				System.out.println("couldn't find the change rate of "+userid+ " and skip this user because it will not expire");
				continue;
			}
			
			//read ~tp
			long previousExpirationTime = 
//					userInfoUpdateTime.get(userid);
					estimatedLastChangeTime.get(userid);
			
			Double numberOfChangeRateIntervalPassedAfterLastUpdate = Math.ceil((evaluationTime-previousExpirationTime)*f/60000)+1;
			long currentExpirationTime=previousExpirationTime+(long)((float)(numberOfChangeRateIntervalPassedAfterLastUpdate*60000)/f);
			
			//f's unit is based on minutes inverse so to convert it into milisecond invert we have to multiply with 60000
			
			if(currentExpirationTime < evaluationTime)
				estimatedExpired++;
			
			userEstimatedCurrentExpirationTime.add(new User(userid, currentExpirationTime,f));
			
			//for stats
			Long exactEXPTime=TwitterFollowerCollector.getUserNextExpFromDB(userInfoUpdateTime.get(userid), userid);//doesn't matter if we user userInfoUpdateTime or userLastChangeTime because the followerCount has not been changed 
			userExactExpirationTime.add(new User(userid,exactEXPTime,f));
		}
		
		/***************************************
		 * Filter the candidates *
		 ***************************************/

		List<User> candidates = 
				userEstimatedCurrentExpirationTime;
//				userExactExpirationTime;

		

		/***************************************
		 * Assign the scores to the candidates *
		 ***************************************/
		
		/************************
		 * Order the candidates *
		 ************************/
		
		Collections.sort(candidates, new CandidateComparator());

		/******************
		 * Pick the top u *
		 ******************/

		Iterator<User> it = candidates.iterator();
		int counter=0;
		int countHit=0;
		while(it.hasNext() && counter<updateBudget){
			User temp=it.next();
			//System.out.println("user Id: "+temp.userId+" "+"Expiration Time: "+temp.expirationTime);
			long replicaValue = this.followerReplica.get(temp.userId);
			long bkgValue = TwitterFollowerCollector.getUserFollowerFromDB(evaluationTime, temp.userId);
			if(replicaValue!=bkgValue)
				{
					result.put(temp.userId, "<>");
					System.out.printf("id "+temp.userId+" estimatedxep>> %d changerate>> "+userChangeRates.get(temp.userId)+" chachedValue>> "+ replicaValue+ " actualValue>> "+bkgValue+" \n",(temp.expirationTime-evaluationTime)/60000);
				}
			else
				{
					result.put(temp.userId, "=");
					System.out.printf("id "+temp.userId+" estimatedxep>> %d changerate>> "+userChangeRates.get(temp.userId)+" chachedValue>> "+ replicaValue+ " actualValue>> "+bkgValue+" \n",(temp.expirationTime-evaluationTime)/60000);
				}
			if(actuallyExpiredUsers.contains(temp.userId)) countHit++;
			//if(temp.expirationTime==0) continue;
			//if(currentValue==bkgValue) continue;
			//result.add(temp.userId);
			counter++;
		}
		System.out.println("skipped users: ");		
		while(it.hasNext()){
			User temp= it.next();
			System.out.printf("id "+temp.userId+" >>estimatedexp >> %d changerate>> "+userChangeRates.get(temp.userId)+" cachedvalue>> "+followerReplica.get(temp.userId)+" actualValue "+TwitterFollowerCollector.getUserFollowerFromDB(evaluationTime, temp.userId)+" \n",(temp.expirationTime-evaluationTime)/60000);
		}
		
		hitratioWithinUpdateBudget=(double)countHit/(double)counter;
		it = userEstimatedCurrentExpirationTime.iterator();
		counter=countHit=0;
		while(it.hasNext()){
			User temp=it.next();
			if(temp.expirationTime<evaluationTime)
			{counter++;
			if(actuallyExpiredUsers.contains(temp.userId))
					countHit++;
			}
		}
		windowHitratio=(double)countHit/(double)counter;
		
		
		try {
			statsFileWriter.write(Integer.toString(estimatedExpired)+","+hitratioWithinUpdateBudget+","+windowHitratio);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("-------------------------------------------------------------------------------------------------------------------");
		if(result.size()<updateBudget)
			System.out.println("the update budget has not been used fully");
		return result;
	}
	
	final class User{
		long userId;
		long expirationTime, nextExpirationTime;
		float changeRate;
		public User(long id,long te,float changeRate){
			userId=id;
			expirationTime=te;
			this.changeRate=changeRate;
		}
	}
	
	private class CandidateComparator implements Comparator<User>{
			public int compare(User o1, User o2) {
				int res=(int)(o1.expirationTime - o2.expirationTime);
				if(res==0){
					if(o2.changeRate==o1.changeRate) return 0;
					return (o2.changeRate-o1.changeRate)<0?-1:1;

				}
				//System.out.printf("%f | %f ; %f | %f"+" >> "+ res+" \n",o1.expirationTime,o2.expirationTime,o1.changeRate,o2.changeRate);
				return res;//users with the latest expiration time will come first and will be chosen
			}

	        	
	        	/*long timeDiff = Math.abs(o1.expirationTime-o2.expirationTime);
	        	long changeDiff = Math.abs( (long)(60000/o1.changeRate) - (long)(60000/o2.changeRate));
	        	int res=0;
	        	if((float)timeDiff/(float)changeDiff < 2 ) res=(int)(o1.expirationTime - o2.expirationTime);
	        	else {
	        		res=
	        	}
	        	if(res==0)
        		{
	        		if(o2.changeRate==o1.changeRate) return 0;
	        		//return (o2.changeRate-o1.changeRate)<0?-1:1;
	        		return (changeDiff-timeDiff)<0?1:-1;
        			
        		}
	        	//System.out.printf("%f | %f ; %f | %f"+" >> "+ res+" \n",o1.expirationTime,o2.expirationTime,o1.changeRate,o2.changeRate);
	            return res;//users with the latest expiration time will come first and will be chosen
	        	
	            }*/
	        	/*int We1= (int)Math.ceil((o1.expirationTime-evaluationTime)/(Config.INSTANCE.getQueryWindowSlide()*1000));
	        	int We2= (int)Math.ceil((o2.expirationTime-evaluationTime)/(Config.INSTANCE.getQueryWindowSlide()*1000));
	        	int res=We1-We2;
	        	if(res==0)
	        	{
	        		if(o2.changeRate==o1.changeRate) return 0;
	        		res=(o2.changeRate-o1.changeRate)<0?-1:1;
	        	}
	        	return res;
	        }*/
	}

}
