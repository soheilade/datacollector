package acqua.query.join.bkg1;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import acqua.config.Config;
import acqua.data.RemoteBKGManager;
import acqua.data.TwitterFollowerCollector;
import acqua.data.generator.vertex.Vertex;
import acqua.query.join.JoinOperator;

public class NMJoinOracle implements JoinOperator{
	private  FileWriter outputWriter;
	public NMJoinOracle(){
		try{
			String path= Config.INSTANCE.getProjectPath()+Config.INSTANCE.getDatasetFolder()+"joinOutput/"+this.getClass().getSimpleName()+"Output.txt";
			outputWriter = new FileWriter(new File(path));
		}catch(Exception e){e.printStackTrace();}
	}
	public void process(long timeStamp, Map<Long,Integer> mentionList,Map<Long,Long> usersTimeStampOfTheCurrentSlidedWindow){
		try {
			//per each evaluation  we extract the current value of all stock revenues
			//timeStamp -
			HashMap<Long, Integer> currentStockRevenue=RemoteBKGManager.INSTANCE.getAllCurrentStockRevenue(timeStamp);
			
			//FIXME: remove the dependency to QueryProcessor
			long windowDiff = timeStamp-Config.INSTANCE.getQueryStartingTime();
			if (windowDiff==0) return;
			int index=((int)windowDiff)/(Config.INSTANCE.getQueryWindowWidth()*1000);			
			//join current window of mentioned funds with current stock revenues and return result
			Iterator<Long> it= mentionList.keySet().iterator();
			while(it.hasNext()){
				long fundId=Long.parseLong(it.next().toString());
				List<Vertex> stocksOfCurrentFund = RemoteBKGManager.INSTANCE.getBipartiteMappingGraph().getNeighboursForU((int)fundId);
				for(int p=0;p<stocksOfCurrentFund.size();p++){
					int currentStock=stocksOfCurrentFund.get(p).getIntID();					
					outputWriter.write(fundId +" "+currentStock+" "+currentStockRevenue.get((long)currentStock)+" "+timeStamp+"\n");
				}				
			}
			//outputWriter.write("end\n");
			outputWriter.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void close(){try{outputWriter.flush();outputWriter.close();}catch(Exception e ){e.printStackTrace();}}
}
