package acqua.query.join.bkg1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import acqua.data.RemoteBKGManager;
import acqua.data.generator.matrix.biadjacencyMatrix;
import acqua.query.join.bkg1.BipartedMapping.Stock;
import acqua.query.join.nmjoin.ApproximateNMJoinOperator;

public class subgraphStockIndegree extends ApproximateNMJoinOperator{
protected int updateBudget;
	
	public subgraphStockIndegree(int ub){
		updateBudget=ub;
		
	}
	
	@Override
	protected HashMap<Long, String> updatePolicy(Iterator<Long> candidateFundSetIterator,
			Map<Long, Long> fundsTimeStampOfTheCurrentSlidedWindow, long evaluationTime) {
		//decide which stock to update and return the list
				//it must satisfy the updateBudget constraint!

				//the return structure
				HashMap<Long, String> result=new HashMap<Long, String>();

				/********************************
				 * Compute the indegree *
				 ********************************/
				List<Integer> windowFunds=new ArrayList<Integer>();
				while(candidateFundSetIterator.hasNext()){
					long fundId=candidateFundSetIterator.next();
					windowFunds.add((int)fundId);
				}
				biadjacencyMatrix subgarphOfWindowFunds = RemoteBKGManager.bipartiteMappingGraph.getSubGraphForU(windowFunds);
				
				HashMap<Integer,Stock> subgraphStockIndegree=new HashMap<Integer, Stock>();				
				for(int fundId: windowFunds){				
					List<Integer> adjacentStocks = subgarphOfWindowFunds.getNeighboursForU((int)fundId);
					for(int i=0;i<adjacentStocks.size();i++){
						int currentStock = adjacentStocks.get(i);
						int currentStockIndegree = subgarphOfWindowFunds.getNeighboursForV(currentStock).size();
						subgraphStockIndegree.put(currentStock, new Stock(currentStock,currentStockIndegree,RemoteBKGManager.changeRate.get(currentStock)));
					}
				}
				//this class doesn't check to see if a stock is expired or not
				//this class doesn't check to see the life of corresponding stock either
				/************************
				 * Order the stocks based on their indegree *
				 ************************/
				List <Stock> StockListOfWindow=new ArrayList<subgraphStockIndegree.Stock>(subgraphStockIndegree.values());
				Collections.sort(StockListOfWindow, new CandidateComparator());
				System.out.print("sorted stocks associated to current window funds: ");
				for(int i=0;i<StockListOfWindow.size();i++)
					System.out.print("id:"+StockListOfWindow.get(i).stockId+" indegree: "+StockListOfWindow.get(i).indegree+" changerate:"+StockListOfWindow.get(i).changeRate);
				System.out.println("---------------");
				/******************
				 * Pick the top u *
				 ******************/
				int counter=0;
				for(int i=0;i<StockListOfWindow.size()&& counter<updateBudget;i++){
					Stock temp=StockListOfWindow.get(i);
						result.put(Long.parseLong(Integer.toString(temp.stockId)),Integer.toString(temp.indegree));
					counter++;
				}
				return result;
		
	}
	final class Stock{
		int stockId;
		int indegree;
		int changeRate;

		public Stock(int id,int in, int cr){
			stockId=id;
			indegree=in;
			changeRate=cr;
		}
	}

	private class CandidateComparator implements Comparator<Stock>{
		//<0 o1<o2
		//=0 o1=o2
		//>0 o1>o2
		public int compare(Stock o1, Stock o2) {
			int res=o2.changeRate-o1.changeRate;
			if (res==0)
				res = o2.indegree-o1.indegree;
			//int res = o1.score-o2.score;
				return res;
		}
	}
}
