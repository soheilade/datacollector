package acqua.query.join.bkg1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;

import acqua.data.TwitterFollowerCollector;

public class RandomWithOutWindowsLocality extends ApproximateJoinOperator {
	protected int updateBudget;
	public RandomWithOutWindowsLocality(int ub) {
		updateBudget=ub;
		// TODO Auto-generated constructor stub
	}
	@Override
	protected HashMap<Long, String> updatePolicy(
			Iterator<Long> candidateUserSetIterator, Map<Long, Long> usersTimeStampOfTheCurrentSlidedWindow,
			long evaluationTime) {
		HashMap<Long,String> result=new HashMap<Long,String>();
		Iterator<Long> allReplicatedUsers = followerReplica.keySet().iterator();
		if(!allReplicatedUsers.hasNext()) return result;
		ArrayList<Long> A=new ArrayList<Long>();
		while(allReplicatedUsers.hasNext()){
			A.add(allReplicatedUsers.next());
		}
		Random rand = new Random(System.currentTimeMillis());
		ArrayList<Integer> indexes = new ArrayList<Integer>();
		while(indexes.size()<updateBudget){
			indexes.add(rand.nextInt(A.size()));
		}
		int counter=0;
		while(counter<updateBudget){
				Long temp = A.get(indexes.get(counter));
				if(followerReplica.get(temp)==TwitterFollowerCollector.getUserFollowerFromDB(evaluationTime, temp))
					result.put(temp,"=");
				else
					result.put(temp,"<>");
				counter++;
			}
			System.out.println("-----------------------------------------------------------------");
			return result;
		
	}
}
