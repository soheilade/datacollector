package acqua.query.join.event;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import acqua.config.Config;

public class UpdateEventNM extends UpdateEvent {

	public HashMap<Integer, Integer> fundToScore = new HashMap<Integer, Integer>();
	public int totalIndegreeByScore = 0;
	public HashMap<Integer, UpdateEvent> StockToScore = new HashMap<Integer, UpdateEvent>();
	public double totalOutdegreeByScore = 0;

	public UpdateEventNM(int id, int changeRate, long currentTempEvluationTime, int remainingEva, long currentTime) {
		super(id, changeRate, currentTempEvluationTime, remainingEva, currentTime);
		totalIndegreeByScore = 0;
		fundToScore = new HashMap<Integer, Integer>();
		totalOutdegreeByScore = 0;
		StockToScore = new HashMap<Integer, UpdateEvent>();
	}

	public UpdateEventNM (int id, double score, HashMap<Integer, UpdateEvent>StockToScore){
		super(id, 1, 0, 0, 0);
		this.IDToUpdateInBKG = id;
		this.totalOutdegreeByScore = score;
		this.StockToScore = StockToScore;
	}
	
	public static UpdateEventNM planOneUpdateEventForV(int originalID, int changeRate, long leavingWindowTime, long evaluationTime) {
		long timeInFrontOfTheStock = leavingWindowTime - evaluationTime;
		int evaInFrontOfTheStock = (int) Math.ceil((double) timeInFrontOfTheStock / (double) Config.INSTANCE.getQueryWindowSlide());
		UpdateEventNM tempEvent = new UpdateEventNM(originalID, changeRate, evaluationTime, evaInFrontOfTheStock, evaluationTime);
		return tempEvent;
	}

	@Override
	public String toString() {
		return expiredEvalutionTime + " " + IDToUpdateInBKG + " " + totalIndegreeByScore;
	}
	
	public String toFundFullString() {
		return expiredEvalutionTime + " " + IDToUpdateInBKG + " " + StockToScore;
	}
	public String toFundScoreString() {
		return expiredEvalutionTime + " " + IDToUpdateInBKG + " " + String.format( "%.2f", totalOutdegreeByScore );
	}

	public void addIndegreeToFundToScore(int originalID, long leavingWindowTime, long evaluationTime, int changeRateV) {
		long timeInFrontOfTheFund = leavingWindowTime - evaluationTime;
		int evaInFrontOfTheStock = (int) Math.ceil((double) timeInFrontOfTheFund / (double) Config.INSTANCE.getQueryWindowSlide());
		int score = this.calculateScore(changeRateV, evaluationTime, evaInFrontOfTheStock);
		if (score > this.scoreForUpdate)
			throw new RuntimeException("the socre for each fund should be less than the overall score");
		this.fundToScore.put(originalID, score);
		this.totalIndegreeByScore += score;
	}

	public void addOutdegreeToStockToScore(int originalID, long leavingWindowTime, long evaluationTime, int changeRateV) {
		long timeInFrontOfTheFund = leavingWindowTime - evaluationTime;
		int evaInFrontOfTheStock = (int) Math.ceil((double) timeInFrontOfTheFund / (double) Config.INSTANCE.getQueryWindowSlide());
		int score = this.calculateScore(changeRateV, evaluationTime, evaInFrontOfTheStock);
		if (score > this.scoreForUpdate)
			throw new RuntimeException("the socre for each fund should be less than the overall score");
		this.fundToScore.put(originalID, score);
		this.totalIndegreeByScore += score;
	}

	public static class Comparators {
		public static Comparator<UpdateEventNM> TOTALINDEGREEVSCORE = new Comparator<UpdateEventNM>() {
			public int compare(UpdateEventNM o1, UpdateEventNM o2) {
				if (Integer.compare(o1.totalIndegreeByScore, o2.totalIndegreeByScore) != 0)
					return Integer.compare(o1.totalIndegreeByScore, o2.totalIndegreeByScore);
				else
					return Integer.compare(o1.IDToUpdateInBKG, o2.IDToUpdateInBKG);
			}
		};
		public static Comparator<UpdateEventNM> TOTALOUTDEGREEUSCORE = new Comparator<UpdateEventNM>() {
			public int compare(UpdateEventNM o1, UpdateEventNM o2) {
				if (Double.compare(o1.totalOutdegreeByScore, o2.totalOutdegreeByScore) != 0)
					return Double.compare(o1.totalOutdegreeByScore, o2.totalOutdegreeByScore);
				else
					return Integer.compare(o1.IDToUpdateInBKG, o2.IDToUpdateInBKG);
			}
		};
	}

}