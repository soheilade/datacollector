package acqua.query.join.event;

import java.util.Comparator;

import acqua.config.Config;

public class UpdateEvent {
	public int IDToUpdateInBKG;
	public int changeRate;
	public long expiredEvalutionTime;
	public long creatTime; // not sure if useful
	public int remainingEva;
	public int scoreForUpdate;
	public int scheduledUpdateTime;

	public int calculateScore(int changeRate, long currentTempEvluationTime, int remainingEva) {
		long tempExpirationTimeAfterScheduledTime = this.scheduledUpdateTime + changeRate - currentTempEvluationTime;
		int validSlideAfterScheduledTime = (int) Math.ceil((double) tempExpirationTimeAfterScheduledTime / (double) Config.INSTANCE.getQueryWindowSlide());
		int score = Math.min(validSlideAfterScheduledTime, remainingEva);
		return score;
	}
	public UpdateEvent(int id, int changeRate, long currentTempEvluationTime, int remainingEva, long currentTime) {
		this.IDToUpdateInBKG = id;
		this.creatTime = currentTime;
		this.changeRate = changeRate;
		// the evaluation time it needs update
		this.expiredEvalutionTime = currentTempEvluationTime;
		this.remainingEva = remainingEva;
		// get scheduledUpdateTime from change rate
		this.scheduledUpdateTime = (int) currentTempEvluationTime / changeRate;
		this.scheduledUpdateTime = this.scheduledUpdateTime * changeRate;
		 this.scoreForUpdate = this.calculateScore(changeRate, currentTempEvluationTime, remainingEva);
	}
	public static UpdateEvent planOneUpdateEvent(int originalID, int changeRate, long leavingWindowTime, long evaluationTime) {
		long timeInFrontOfTheStock = leavingWindowTime - evaluationTime;
		int evaInFrontOfTheStock = (int) Math.ceil((double) timeInFrontOfTheStock / (double) Config.INSTANCE.getQueryWindowSlide());

		UpdateEvent tempEvent = new UpdateEvent(originalID, changeRate, evaluationTime, evaInFrontOfTheStock, evaluationTime);
		// score can be 0, since a data can leave the window before it expires
		// if (tempEvent.scoreForUpdate == 0 && evaluationTime != Config.INSTANCE.getQueryWindowWidth())
		// throw new RuntimeException("score cannot be 0");
		return tempEvent;
	}

	@Override
	public String toString() {
		return expiredEvalutionTime + " " + IDToUpdateInBKG + " " + scoreForUpdate;
	}

	public String toFullString() {
		return IDToUpdateInBKG + " " + expiredEvalutionTime + " " + remainingEva + " " + scoreForUpdate;
	}

	public static class Comparators {

		public static Comparator<UpdateEvent> ID = new Comparator<UpdateEvent>() {
			public int compare(UpdateEvent o1, UpdateEvent o2) {
				if (Integer.compare(o1.IDToUpdateInBKG, o2.IDToUpdateInBKG) != 0)
					return Integer.compare(o1.IDToUpdateInBKG, o2.IDToUpdateInBKG);
				else if (Integer.compare(o1.scheduledUpdateTime, o2.scheduledUpdateTime) == 0)
					throw new RuntimeException("at one time update an data twice in SCHEDULEDTIME comparator");
				else
					return Integer.compare(o1.scheduledUpdateTime, o2.scheduledUpdateTime);
			}
		};
		public static Comparator<UpdateEvent> SCORE = new Comparator<UpdateEvent>() {
			public int compare(UpdateEvent o1, UpdateEvent o2) {
				if (Integer.compare(o1.scoreForUpdate, o2.scoreForUpdate) != 0)
					return Integer.compare(o1.scoreForUpdate, o2.scoreForUpdate);
//				else if (Integer.compare(o1.scheduledUpdateTime, o2.scheduledUpdateTime) != 0)
//					return Integer.compare(o2.scheduledUpdateTime, o1.scheduledUpdateTime);
				else
					return Integer.compare(o1.IDToUpdateInBKG, o2.IDToUpdateInBKG);
			}
		};
		public static Comparator<UpdateEvent> SCHEDULEDTIME = new Comparator<UpdateEvent>() {
			public int compare(UpdateEvent o1, UpdateEvent o2) {
				if (Integer.compare(o1.scheduledUpdateTime, o2.scheduledUpdateTime) != 0)
					return Integer.compare(o1.scheduledUpdateTime, o2.scheduledUpdateTime);
				// else if (Integer.compare(o1.IDToUpdateInBKG, o2.IDToUpdateInBKG) == 0) {
				// System.out.println("compare" + o1.IDToUpdateInBKG + " " + o2.IDToUpdateInBKG);
				// throw new RuntimeException("at one time update an data twice in SCHEDULEDTIME comparator");
				// }
				else
					return Integer.compare(o1.IDToUpdateInBKG, o2.IDToUpdateInBKG);
			}
		};
	}
}
