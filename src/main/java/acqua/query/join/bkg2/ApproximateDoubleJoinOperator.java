package acqua.query.join.bkg2;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import acqua.config.Config;
import acqua.data.TwitterFollowerCollector;
import acqua.query.join.JoinOperator;


public abstract class ApproximateDoubleJoinOperator implements JoinOperator {
	protected Map<Long, Integer> followerReplica;
	protected Map<Long,Integer>  statusCountReplica;
	protected Map<Long,Long> followerUpdateTime;
	protected Map<Long,Long> statusCountUpdateTime;
	protected abstract  HashMap<Long,Integer> updatePolicyFollowerStatusCount(Iterator<Long> candidateUserSetIterator);
	protected long timeStamp;
	private FileWriter fileWriter;

	public ApproximateDoubleJoinOperator(){
		followerReplica=new HashMap<Long, Integer>();
		followerUpdateTime = new HashMap<Long, Long>();
		statusCountReplica = new HashMap<Long, Integer>();
		statusCountUpdateTime = new HashMap<Long, Long>();
		followerReplica = TwitterFollowerCollector.getInitialUserFollowersFromDB(); // ==>  firstWindow
		statusCountReplica = TwitterFollowerCollector.getInitialUserStatusCountFromDB();
		Iterator<Long> it = followerReplica.keySet().iterator();
		while(it.hasNext()){
			followerUpdateTime.put(Long.parseLong(it.next().toString()),Config.INSTANCE.getQueryStartingTime());//follower info is according to the end of first window
		}
		it=statusCountReplica.keySet().iterator();
		while(it.hasNext()){
			statusCountUpdateTime.put(Long.parseLong(it.next().toString()),Config.INSTANCE.getQueryStartingTime());//follower info is according to the end of first window
		}
		try{
			fileWriter = new FileWriter(new File("D:/softwareData/git-clone-https---soheilade-bitbucket.org-soheilade-acqua.git/acquaProj/joinOutput/"+this.getClass().getSimpleName()+"Output.txt"));
		}catch(Exception e){e.printStackTrace();}
	}
	public void process(long timeStamp,Map<Long,Integer> mentionList){			
		try {
			this.timeStamp=timeStamp;
			//process the join			
			long windowDiff = timeStamp-Config.INSTANCE.getQueryStartingTime();
			if (windowDiff==0) return;
			int index=((int)windowDiff)/(Config.INSTANCE.getQueryWindowWidth()*1000);			
			//HashMap<Long,Integer> mentionList = tsc.windows.get(index);		
			//invoke FollowerTable::getFollowers(user,ts) and updates the replica for a subset of users that exist in stream
			HashMap<Long,Integer> updated=updatePolicyFollowerStatusCount(mentionList.keySet().iterator());
			Iterator<Long> it=updated.keySet().iterator();
			while(it.hasNext()){
				long tempUpdateUserId=it.next();
				if(updated.get(tempUpdateUserId)==1){
					followerReplica.put(tempUpdateUserId,TwitterFollowerCollector.getUserFollowerFromDB(timeStamp, tempUpdateUserId));
					followerUpdateTime.put(tempUpdateUserId, timeStamp);
				}
				if(updated.get(tempUpdateUserId)==2){
					statusCountReplica.put(tempUpdateUserId,TwitterFollowerCollector.getUserStatusCountFromDB(timeStamp, tempUpdateUserId));
					statusCountUpdateTime.put(tempUpdateUserId, timeStamp);
				}
			}		
			//we join mentionList with current replica and return result	
			it= mentionList.keySet().iterator();
			while(it.hasNext()){
				long userId=Long.parseLong(it.next().toString());
				Integer userFollowers = followerReplica.get(userId);
				Integer StatusCount = statusCountReplica.get(userId);
				fileWriter.write(userId +" "+mentionList.get(userId)+" "+userFollowers+" "+StatusCount+" "+timeStamp+"\n");
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void close(){try{fileWriter.flush();fileWriter.close();}catch(Exception e){e.printStackTrace();}}


}
