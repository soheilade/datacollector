package acqua.query.join.bkg2;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import acqua.data.TwitterFollowerCollector;
import acqua.query.join.JoinOperator;

public class OracleDoubleJoinOperator implements JoinOperator{
	public  FileWriter outputWriter;
	public OracleDoubleJoinOperator(){
		try{
			outputWriter = new FileWriter(new File("D:/softwareData/git-clone-https---soheilade-bitbucket.org-soheilade-acqua.git/acquaProj/joinOutput/"+this.getClass().getSimpleName()+"Output.txt"));
		}catch(Exception e){e.printStackTrace();}
	}
	public void process(long timeStamp, Map<Long, Integer> streamWindow) {
		try {
			HashMap<Long, Integer> currentFollowerCount=TwitterFollowerCollector.getFollowerListFromDB(timeStamp);
			HashMap<Long, Integer> currentStatusCount=TwitterFollowerCollector.getStsCountListFromDB(timeStamp);
			//process the join			
			//we join mentionList with current replica and return result	
			Iterator<Long> it= streamWindow.keySet().iterator();
			while(it.hasNext()){
				long userId=Long.parseLong(it.next().toString());
				Integer userFollowers = currentFollowerCount.get(userId);
				Integer StatusCount = currentStatusCount.get(userId);
				outputWriter.write(userId +" "+streamWindow.get(userId)+" "+userFollowers+" "+StatusCount+" "+timeStamp+"\n");
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void close() {
		try{outputWriter.flush();outputWriter.close();}catch(Exception e){e.printStackTrace();}		
	}

}
