package acqua.query.join.bkg2;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import acqua.config.Config;

public class DoubleBkgJoinOperator extends ApproximateDoubleJoinOperator  {
	protected int updateBudget;
	protected HashMap<Long,Float> statusChangeRate; 
	protected HashMap<Long,Float> followerChangeRate;
	
	public DoubleBkgJoinOperator(int updateBudget){
		this.updateBudget=updateBudget;
		statusChangeRate=new HashMap<Long, Float>();
		followerChangeRate = new HashMap<Long, Float>();
		Connection c = null;
		Statement stmt = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(Config.INSTANCE.getDatasetDb());
			c.setAutoCommit(false);
			stmt = c.createStatement();
			String sql="SELECT userid, ChangeRate from  ";
			sql="SELECT userid, changeRate from StatusCR ";
			//CREATE TABLE FollowerLowChr AS SELECT copyBK.uSERID as userid, CAST(CAST(count(distinct copyBK.FollowerCut) as float)/30 AS FLOAT) as changeRate from copyBK group by copyBK.uSERID
			System.out.println(sql);
			ResultSet rs = stmt.executeQuery( sql);

			while ( rs.next() ) {
				long userId = rs.getLong("userid");
				float userChangeRate  = rs.getFloat("changeRate");
				statusChangeRate.put(userId, userChangeRate);
			}
			rs.close();
			sql="SELECT userid, changeRate from FollowerLowChr ";
			//CREATE TABLE FollowerLowChr AS SELECT copyBK.uSERID as userid, CAST(CAST(count(distinct copyBK.FollowerCut) as float)/30 AS FLOAT) as changeRate from copyBK group by copyBK.uSERID
			System.out.println(sql);
			ResultSet rs2 = stmt.executeQuery( sql);

			while ( rs2.next() ) {
				long userId = rs2.getLong("userid");
				float userChangeRate  = rs2.getFloat("changeRate");
				followerChangeRate.put(userId, userChangeRate);
			}
			rs2.close();
			stmt.close();
			c.close();
		} catch ( Exception e ) {
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			System.exit(0);
		}
	}
	//it return the list of users with 1 which shows that user from join counterpart 1 should be updated or 2 which shows that user from join counterpart2 should be updated 
	protected HashMap<Long, Integer> updatePolicyFollowerStatusCount(
			Iterator<Long> candidateUserSetIterator) {
		final class User{
			long userId;
			float statusExpirationTime;
			float followerExpirationTime;
			public User(long id,float set,float fet){userId=id;statusExpirationTime=set;followerExpirationTime=fet;}
		}
		List<User> userExpirationTime=new ArrayList<User>();
		while(candidateUserSetIterator.hasNext()){
			long userid=Long.parseLong(candidateUserSetIterator.next().toString());
			float f= Float.parseFloat(statusChangeRate.get(userid).toString());
			long latestUpdateTime = Long.parseLong(statusCountUpdateTime.get(userid).toString());
			long statusExpectedExpirationTime=latestUpdateTime+(long)(60000/f);
			long followerExpectedExpirationTime=latestUpdateTime+(long)(60000/f);
			//System.out.println(expectedExpirationTime);
			userExpirationTime.add(new User(userid, statusExpectedExpirationTime,followerExpectedExpirationTime));				
		}
		Collections.sort(userExpirationTime, new Comparator<User>() {
			public int compare(User o1, User o2) {
				int c1=0,c2=0;
				if(o1.followerExpirationTime < timeStamp) c1++;
				if(o1.statusExpirationTime < timeStamp) c1++;
				if(o2.followerExpirationTime < timeStamp) c2++;
				if(o2.statusExpirationTime < timeStamp) c2++;
				if(c1==c2) return 0;
				if(c1==1) return 1; //c2 is 0 or 2
				if(c2==1) return -1; // c1 is 0 or 2
				if(c1==2) return 1; // c2 is 0
				return -1; //c1 is 0
			}
		});
		HashMap<Long, Integer> result=new HashMap<Long,Integer>();
		Iterator<User> it = userExpirationTime.iterator();
		int counter=0;
		while(it.hasNext()&&counter<updateBudget){
			User temp=it.next();
			//System.out.println("user Id: "+temp.userId+" "+"Expiration Time: "+temp.ExpirationTime);
			if(temp.followerExpirationTime<timeStamp)
				result.put(temp.userId,1);
			else if (temp.statusExpirationTime<timeStamp)
				result.put(temp.userId, 2);
			counter++;
		}
		System.out.println("--------------------------------------------------");
		return result;
	}
	public void process(long timeStamp, Map<Long, Integer> streamWindow,
			Map<Long, Long> usersTimeStampOfTheCurrentSlidedWindow) {
		// TODO Auto-generated method stub
		
	}		

}
