package acqua.query.join.nmjoin;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import acqua.config.Config;
import acqua.data.TwitterFollowerCollector;
import acqua.data.RemoteBKGManager;
import acqua.data.generator.matrix.biadjacencyMatrix;
import acqua.data.generator.vertex.Vertex;
import acqua.query.join.JoinOperator;
import acqua.query.join.event.UpdateEvent;

public abstract class ApproximateNMJoinOperator implements JoinOperator {
	protected HashMap<Integer, Integer> bkgReplica;
	protected HashMap<Integer, Long> replicaInfoUpdateTime; // the time on which the item was updated

	protected FileWriter answersFileWriter;
	protected FileWriter selectedCondidatesFileWriter;
	protected FileWriter statsFileWriter;

	public int updateBudget;
	public int currentBudget;
	public int totalUpdateBudgetSpend;

	private static final Logger LoggerNM = LogManager.getLogger("ApproximateNMJoinOperator");

	public ApproximateNMJoinOperator() {
		HashMap<Integer, String> bkgInfo = new HashMap<Integer, String>();
		bkgReplica = new HashMap<Integer, Integer>();
		replicaInfoUpdateTime = new HashMap<Integer, Long>();

		bkgInfo = RemoteBKGManager.INSTANCE.getInitialBkgInfoFromDB(); // initial information(revenue) of stocks

		Iterator<Integer> it = bkgInfo.keySet().iterator();

		// iterate through all stocks
		while (it.hasNext()) {
			Integer bkgInfoUnit = it.next();// current stock id
			String bkgInfoUnitValue = bkgInfo.get(bkgInfoUnit);// revenue and valid to of current stock
			String[] revenueTime = bkgInfoUnitValue.split(",");
			// System.out.println();
			bkgReplica.put(bkgInfoUnit, Integer.parseInt(revenueTime[0]));// fill the replica
			replicaInfoUpdateTime.put(bkgInfoUnit, Long.parseLong(revenueTime[1]));// follower info is according to the end of first window
		}

		try {
			answersFileWriter = new FileWriter(new File(Config.INSTANCE.getProjectPath() + Config.INSTANCE.getDatasetFolder() + "joinOutput/" + this.getClass().getSimpleName() + getSuffix() + "Output.txt"));
			selectedCondidatesFileWriter = new FileWriter(new File(Config.INSTANCE.getProjectPath() + Config.INSTANCE.getDatasetFolder() + "Debug/" + this.getClass().getSimpleName() + getSuffix() + "selectedupdateEntries.txt"));
			statsFileWriter = new FileWriter(new File(Config.INSTANCE.getProjectPath() + Config.INSTANCE.getDatasetFolder() + "Debug/" + this.getClass().getSimpleName() + getSuffix() + "estimationErrorPerWindow.txt"));
			statsFileWriter.write("p,s,p&s,totalNumberOfCandidatesinML,numberOfExpiredCandidatesinML,numberOfExpiredCandidatesAfterTheMaintenanceinML,numberOfExpiredElementsInTheView,numberOfExpiredElementsInTheViewAfterTheMaintenance \n");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	protected String getSuffix() {
		return "";
	}

	public void process(long evaluationTime, Map<Long, Integer> mentionList, Map<Long, Long> usersTimeStampOfTheCurrentSlidedWindow) {
		try {
			// skip the first iteration
			long windowDiff = evaluationTime - Config.INSTANCE.getQueryStartingTime();
			if (windowDiff == 0)
				return;

			// updates the replica for a subset of users that exist in stream
			HashMap<Long, String> electedElements = updatePolicy(mentionList.keySet().iterator(), usersTimeStampOfTheCurrentSlidedWindow, evaluationTime);

			/*
			 * for stats and debug
			 */
			selectedCondidatesFileWriter.write("associated Stock List to Current Window: ");
			HashMap<Vertex, String> adjacentStockIndegree = new HashMap<Vertex, String>();
			for (long f : mentionList.keySet()) {
				List<Vertex> adjacentStocks = RemoteBKGManager.INSTANCE.getBipartiteMappingGraph().getNeighboursForU((int) f);
				for (int i = 0; i < adjacentStocks.size(); i++) {
					Vertex currentStock = adjacentStocks.get(i);
					int currentStockIndegree = RemoteBKGManager.INSTANCE.getBipartiteMappingGraph().getNeighboursForV(currentStock.getIntID()).size();
					int newValue = RemoteBKGManager.INSTANCE.getCurrentStockRevenueFromDB((int) evaluationTime, currentStock.getIntID());
					if (!bkgReplica.get(currentStock.getIntID()).equals(newValue))
						adjacentStockIndegree.put(currentStock, currentStockIndegree + " Not");
					else
						adjacentStockIndegree.put(currentStock, currentStockIndegree + " valid");
				}
			}
			selectedCondidatesFileWriter.write(adjacentStockIndegree + "=>" + electedElements.toString() + "," + evaluationTime + "\n");

			for (String ss : electedElements.values()) {
				String[] oidInfo = ss.split(" ");

				long changeRate = RemoteBKGManager.INSTANCE.getChangeRate().get(Integer.parseInt(oidInfo[1]));
				long lastUpdateTime = evaluationTime / changeRate;
				lastUpdateTime = lastUpdateTime * changeRate;
				replicaInfoUpdateTime.put(Integer.parseInt(oidInfo[1]), lastUpdateTime);

				int newValue = RemoteBKGManager.INSTANCE.getCurrentStockRevenueFromDB((int) evaluationTime, Integer.parseInt(oidInfo[1]));

				if (!bkgReplica.get(Integer.parseInt(oidInfo[1])).equals(newValue)) {
					bkgReplica.put(Integer.parseInt(oidInfo[1]), newValue);
				} else {
					throw new RuntimeException("the Vs choosn by update policy are still valid");
				}
			}

			// perform the join
			Iterator<Long> it = mentionList.keySet().iterator();
			HashMap<Integer, Integer> tempResultsForDRAND = new HashMap<Integer, Integer>();

			if (this.getClass().getName().contains("NMJoinOperatorMaintainRandomWithrepetition")) {
				for (String ss : electedElements.values()) {
					String[] oidInfo = ss.split(" ");
					tempResultsForDRAND.put(Integer.parseInt(oidInfo[1]), Integer.parseInt(oidInfo[2]));
				}

			}
			while (it.hasNext()) {
				int fundId = Integer.parseInt(it.next().toString());
				List<Vertex> adjacentStocks = RemoteBKGManager.INSTANCE.getBipartiteMappingGraph().getNeighboursForU(fundId);
				for (int i = 0; i < adjacentStocks.size(); i++) {
					int currentStock = adjacentStocks.get(i).getIntID();
					// if(this.getClass() == "")
					// else

					if (this.getClass().getName().contains("NMJoinOperatorMaintainRandomWithrepetition")) {
						if (tempResultsForDRAND.containsKey(currentStock)) {
							int tempNnumber = tempResultsForDRAND.get(currentStock);
							if (tempNnumber > 0) {
								answersFileWriter.write(fundId + " " + currentStock + " " + (bkgReplica.get(currentStock)) + " " + evaluationTime + "\n");
								tempResultsForDRAND.put(currentStock, (tempNnumber - 1));
							} else {
								answersFileWriter.write(fundId + " " + currentStock + " " + (bkgReplica.get(currentStock) - 1) + " " + evaluationTime + "\n");
							}
						} else
							answersFileWriter.write(fundId + " " + currentStock + " " + bkgReplica.get(currentStock) + " " + evaluationTime + "\n");
					} else
						answersFileWriter.write(fundId + " " + currentStock + " " + bkgReplica.get(currentStock) + " " + evaluationTime + "\n");
				}

			}
			// answersFileWriter.write("end\n");
			answersFileWriter.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void close() {
		try {
			answersFileWriter.flush();
			answersFileWriter.close();
			selectedCondidatesFileWriter.flush();
			selectedCondidatesFileWriter.close();
			statsFileWriter.flush();
			statsFileWriter.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/********************************
	 * get current subgraph in the window *
	 ********************************/
	public List<Vertex> getFundsInCurrnetWindow(Iterator<Long> CandidateIds) {
		List<Vertex> fundsOfCurrentWindow = new ArrayList<Vertex>();
		while (CandidateIds.hasNext()) {
			long fundId = CandidateIds.next();
			fundsOfCurrentWindow.add(RemoteBKGManager.INSTANCE.getBipartiteMappingGraph().UMap.get((int) fundId));
		}
		return fundsOfCurrentWindow;
	}

	public List<Vertex> getFundsInAWindow(List<Vertex> funds, Map<Long, Long> fundsEnteringTime, long currentTime) {
		List<Vertex> fundsOfCurrentWindow = new ArrayList<Vertex>();
		for (Vertex fund : funds) {
			long enteringTime = fundsEnteringTime.get((long) Vertex.getOriginalVertex(fund).getIntID());
			long leavingTime = enteringTime + Config.INSTANCE.getQueryWindowWidth();
			if (leavingTime > currentTime) {
				fundsOfCurrentWindow.add(fund);
			}
		}
		return fundsOfCurrentWindow;
	}

	/********************************
	 * get expired Vs [validFrom,validTo) *
	 ********************************/
	public boolean isExpiredV(int id, long evaluationTime) {
		int originalID = id;
		if (replicaInfoUpdateTime.get(originalID) + (long) RemoteBKGManager.INSTANCE.getChangeRate().get(originalID) <= evaluationTime) {
			return true;
		}
		return false;
	}

	public List<Vertex> getExpiredVs(Collection<Vertex> collection, long evaluationTime) {
		List<Vertex> result = new ArrayList<Vertex>();
		for (Vertex curStock : collection) {
			int originalID = curStock.originalVertex.getIntID();
			if (replicaInfoUpdateTime.get(originalID) + (long) RemoteBKGManager.INSTANCE.getChangeRate().get(originalID) <= evaluationTime) {
				result.add(curStock);
			}
		}
		return result;
	}

	public List<Vertex> getExpiredVs(Collection<Vertex> collection, long evaluationTime, HashMap<Integer, Long> tempLastUpdateTime) {
		List<Vertex> result = new ArrayList<Vertex>();
		for (Vertex curStock : collection) {
			int originalID = curStock.originalVertex.getIntID();
			if (tempLastUpdateTime.containsKey(originalID)) {
				if (tempLastUpdateTime.get(originalID) + (long) RemoteBKGManager.INSTANCE.getChangeRate().get(originalID) <= evaluationTime) {
					result.add(curStock);
				}
			} else {
				if (replicaInfoUpdateTime.get(originalID) + (long) RemoteBKGManager.INSTANCE.getChangeRate().get(originalID) <= evaluationTime) {
					result.add(curStock);
				}
			}
		}
		return result;
	}

	public void spendUpdateBudget(int cost) {
		// logger.debug("spend budget " + cost);
		this.currentBudget -= cost;
		this.totalUpdateBudgetSpend += cost;
		if (this.currentBudget < 0)
			throw new RuntimeException("currnet budget is less than 0");
	}

	public void addBudgetForNewEvaluation() {
		this.currentBudget += this.updateBudget;
	}

	public int getStandardBudget() {
		return this.updateBudget;
	}

	public int getCurrentBudget() {
		return this.currentBudget;
	}

	public int getTotalScoreForAListOfUpdateEvents(List<UpdateEvent> input) {
		int result = 0;
		for (UpdateEvent up : input) {
			result += up.scoreForUpdate;
		}
		return result;
	}

	protected abstract HashMap<Long, String> updatePolicy(Iterator<Long> CandidateIds, Map<Long, Long> candidateUserSetIterator, long evaluationTime);
}
