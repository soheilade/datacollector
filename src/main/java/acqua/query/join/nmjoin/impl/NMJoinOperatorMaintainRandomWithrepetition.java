package acqua.query.join.nmjoin.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;

import acqua.config.Config;
import acqua.data.RemoteBKGManager;
import acqua.data.generator.matrix.biadjacencyMatrix;
import acqua.data.generator.vertex.Vertex;
import acqua.query.join.nmjoin.ApproximateNMJoinOperator;

public class NMJoinOperatorMaintainRandomWithrepetition extends ApproximateNMJoinOperator{
	@Override
	protected HashMap<Long, String> updatePolicy(Iterator<Long> candidateFundSetIterator, Map<Long, Long> fundsTimeStampOfTheCurrentSlidedWindow, long evaluationTime) {

		List<Vertex> fundsOfCurrentWindow = getFundsInCurrnetWindow(candidateFundSetIterator);
		biadjacencyMatrix subgraphOfCurrentWindow = RemoteBKGManager.INSTANCE.getBipartiteMappingGraph().getSubGraphForU(fundsOfCurrentWindow);

		
		List<Vertex> expriedVsInCurrentWindow = getExpiredVs(subgraphOfCurrentWindow.VMap.values(), evaluationTime);
		
		List<Vertex> expriedVsInCurrentWindowWithRepetition= new ArrayList<Vertex>();
		
		for(Vertex v : expriedVsInCurrentWindow ){
			int vrepeat = subgraphOfCurrentWindow.getNeighboursForV(v.getIntID()).size();
			for(int y=0;y<vrepeat;y++){
				expriedVsInCurrentWindowWithRepetition.add(v);
			}
		}
		
		HashMap<Long, String> electedElements = new HashMap<Long, String>();
		int remainingBudget=0;//Config.INSTANCE.getUpdateBudget();
		
		Collections.shuffle(expriedVsInCurrentWindowWithRepetition);
		
		while (remainingBudget< Config.INSTANCE.getUpdateBudget() && expriedVsInCurrentWindowWithRepetition.size()>remainingBudget){
			Vertex randomVertex = expriedVsInCurrentWindowWithRepetition.get(remainingBudget);
			long originalID = (long)Vertex.getOriginalVertex(randomVertex).getIntID();
			if( electedElements.containsKey(originalID) ) {
				String ss = electedElements.get(originalID);
				String[] frequencyStr = ss.split(" ");
				int frequency = Integer.parseInt(frequencyStr[2]) + 1;
				electedElements.put(originalID, originalID+" "+ originalID+" "+frequency);
			} else{
				electedElements.put(originalID, originalID+" "+ originalID+" 1");
			}
				
			//electedElements.put((long)randomVertex.getIntID(), randomVertex.toFullStringWithOriID());
			remainingBudget++;
		}		
		return electedElements;
	}
}
