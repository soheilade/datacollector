package acqua.query.join.nmjoin.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import acqua.config.Config;
import acqua.data.RemoteBKGManager;
import acqua.data.generator.matrix.biadjacencyMatrix;
import acqua.data.generator.vertex.Vertex;
import acqua.query.join.event.UpdateEvent;
import acqua.query.join.nmjoin.ApproximateNMJoinOperator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OOFixedBudget extends ApproximateNMJoinOperator {
	private static final Logger logger = LoggerFactory.getLogger("OOJoinOperatorMaintainFixedBudget");
	
	public OOFixedBudget(int ub) {
		this.updateBudget = ub;
		this.currentBudget = 0;
		this.totalUpdateBudgetSpend = 0;
		
	}

	public UpdateEvent planOneUpdateEvent(int originalID, int changeRate, long leavingWindowTime, long evaluationTime) {
		long timeInFrontOfTheStock = leavingWindowTime - evaluationTime;
		int evaInFrontOfTheStock = (int) Math.ceil((double) timeInFrontOfTheStock / (double) Config.INSTANCE.getQueryWindowSlide());

		UpdateEvent tempEvent = new UpdateEvent(originalID, changeRate, evaluationTime, evaInFrontOfTheStock, evaluationTime);
		// score can be 0, since a data can leave the window before it expires
		// if (tempEvent.scoreForUpdate == 0 && evaluationTime != Config.INSTANCE.getQueryWindowWidth())
		// throw new RuntimeException("score cannot be 0");
		return tempEvent;
	}

	@Override
	protected HashMap<Long, String> updatePolicy(Iterator<Long> candidateFundSetIterator, Map<Long, Long> fundsTimeStampOfTheCurrentSlidedWindow, long evaluationTime) {
		logger.debug("current time is " + evaluationTime);
		// init result data structure
		HashMap<Long, String> electedElements = new HashMap<Long, String>();
		int budgetToSpend = 0;
		// adjust update budget
		this.addBudgetForNewEvaluation();

		// get funds in current window
		List<Vertex> fundsOfCurrentWindow = getFundsInCurrnetWindow(candidateFundSetIterator);
		biadjacencyMatrix subgraphOfCurrentWindow = RemoteBKGManager.INSTANCE.getBipartiteMappingGraph().getSubGraphForU(fundsOfCurrentWindow);

		List<Vertex> expriedVsInCurrentWindow = getExpiredVs(subgraphOfCurrentWindow.VMap.values(), evaluationTime);

		// plan event for the current expired data
		List<UpdateEvent> currentExpriedUpdate = new ArrayList<UpdateEvent>();
		for (Vertex e : expriedVsInCurrentWindow) {
			int originalID = Vertex.getOriginalVertex(e).getIntID();
			int changeRate = RemoteBKGManager.INSTANCE.getChangeRate().get(originalID);
			// plan update events
			// TODO here is a short cut, need to convert from fund to stock
			long leavingWindowTime = fundsTimeStampOfTheCurrentSlidedWindow.get((long) originalID);
			leavingWindowTime += (long) Config.INSTANCE.getQueryWindowWidth();
			UpdateEvent tempEvent = planOneUpdateEvent(originalID, changeRate, leavingWindowTime, evaluationTime);
			currentExpriedUpdate.add(tempEvent);

		}
		logger.debug("currentExpriedUpdate " + currentExpriedUpdate.toString());
		// give some advantage to fixed budget
		// by giving a simple debit card
		// no planning ahead
		int tempBudget = this.getCurrentBudget();

		// choose Top K
		List<UpdateEvent> selectedEvents = chooseTopKByScore(tempBudget, currentExpriedUpdate);
		logger.debug("selected events " + selectedEvents.toString());
		// compose result
		for (UpdateEvent tempEvent : selectedEvents) {
			electedElements.put((long) tempEvent.IDToUpdateInBKG, "0 " + tempEvent.IDToUpdateInBKG + " " + tempEvent.scoreForUpdate);
		}

		// spend update budget
		budgetToSpend = electedElements.size();
		this.spendUpdateBudget(budgetToSpend);
		logger.debug("budget left " + this.getCurrentBudget()  + " i have spend "+ this.totalUpdateBudgetSpend);
		
		return electedElements;
	}

	public List<UpdateEvent> chooseTopKByScore(int tempBudget, List<UpdateEvent> currentExpriedUpdate) {
		TreeSet<UpdateEvent> currentExpiredUpdateByScore = new TreeSet<UpdateEvent>(UpdateEvent.Comparators.SCORE);
		currentExpiredUpdateByScore.addAll(currentExpriedUpdate);
		Iterator<UpdateEvent> it = currentExpiredUpdateByScore.descendingIterator();
		List<UpdateEvent> selectedEvents = new ArrayList<UpdateEvent>();

		while (tempBudget > 0 && it.hasNext()) {
			UpdateEvent tempEvent = it.next();
			selectedEvents.add(tempEvent);
			tempBudget--;
		}
		return selectedEvents;
	}

	public void spendUpdateBudget(int cost) {
		// logger.debug("spend budget " + cost);
		this.currentBudget -= cost;
		this.totalUpdateBudgetSpend +=cost;
		if (this.currentBudget < 0)
			throw new RuntimeException("currnet budget is less than 0");
	}

	public void addBudgetForNewEvaluation() {
		this.currentBudget += this.updateBudget;
	}

	public int getStandardBudget() {
		return this.updateBudget;
	}

	public int getCurrentBudget() {
		return this.currentBudget;
	}
}
