package acqua.query.join.nmjoin.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

import acqua.config.Config;
import acqua.data.RemoteBKGManager;
import acqua.data.generator.matrix.biadjacencyMatrix;
import acqua.data.generator.matrix.biadjacencyMatrixOutdegreeU;
import acqua.data.generator.vertex.Vertex;
import acqua.query.join.event.UpdateEvent;
import acqua.query.join.event.UpdateEventNM;
import acqua.query.join.nmjoin.ApproximateNMJoinOperator;

public class NMScoreOutdegreeURemoveEdge extends ApproximateNMJoinOperator {
	public HashMap<Integer, UpdateEvent> getVscoreMap(Map<Long, Long> fundsTimeStampOfTheCurrentSlidedWindow, biadjacencyMatrix subgraphForUpating, List<Vertex> expriedVsInCurrentWindow, long evaluationTime, int budget) {
		HashMap<Integer, UpdateEvent> VsScoreMap = new HashMap<Integer, UpdateEvent>();
		for (Vertex e : expriedVsInCurrentWindow) {
			List<Vertex> neighbours = subgraphForUpating.getNeighboursForV(e.getIntID());
			long lastTime = 0;
			int originalIDV = Vertex.getOriginalVertex(e).getIntID();
			// get leaving time
			for (Vertex v : neighbours) {
				int originalIDU = Vertex.getOriginalVertex(v).getIntID();
				if (fundsTimeStampOfTheCurrentSlidedWindow.get((long) originalIDU) > lastTime)
					lastTime = fundsTimeStampOfTheCurrentSlidedWindow.get((long) originalIDU);
			}
			// for each fund calculate the score
			lastTime += Config.INSTANCE.getQueryWindowWidth();
			int changeRate = RemoteBKGManager.INSTANCE.getChangeRate().get(originalIDV);
			UpdateEvent tempEventV = UpdateEvent.planOneUpdateEvent(originalIDV, changeRate, lastTime, evaluationTime);
			VsScoreMap.put(originalIDV, tempEventV);
		}
		return VsScoreMap;
	}

	@Override
	protected HashMap<Long, String> updatePolicy(Iterator<Long> candidateFundSetIterator, Map<Long, Long> fundsTimeStampOfTheCurrentSlidedWindow, long evaluationTime) {

		List<Vertex> fundsOfCurrentWindow = getFundsInCurrnetWindow(candidateFundSetIterator);
		biadjacencyMatrix subgraphOfCurrentWindow = RemoteBKGManager.INSTANCE.getBipartiteMappingGraph().getSubGraphForU(fundsOfCurrentWindow);

		List<Vertex> expriedVsInCurrentWindow = getExpiredVs(subgraphOfCurrentWindow.VMap.values(), evaluationTime);
		biadjacencyMatrix subgraphForUpating = subgraphOfCurrentWindow.getSubGraphForV(expriedVsInCurrentWindow);

		List<Vertex> expriedVsInSubgraphForUpating = new ArrayList<Vertex>(subgraphForUpating.VMap.values());
		HashMap<Integer, UpdateEvent> VsScoreMap = this.getVscoreMap(fundsTimeStampOfTheCurrentSlidedWindow, subgraphForUpating, expriedVsInSubgraphForUpating, evaluationTime, Config.INSTANCE.getUpdateBudget());

		biadjacencyMatrixOutdegreeU outdegreedSubgraphForUpdating = new biadjacencyMatrixOutdegreeU(subgraphForUpating);
		HashMap<Long, String> electedElements = outdegreedSubgraphForUpdating.removeWithInBudget(Config.INSTANCE.getUpdateBudget(), VsScoreMap);

		return electedElements;
	}

}
