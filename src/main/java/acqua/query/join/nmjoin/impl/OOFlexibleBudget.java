package acqua.query.join.nmjoin.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import acqua.config.Config;
import acqua.data.RemoteBKGManager;
import acqua.data.generator.matrix.biadjacencyMatrix;
import acqua.data.generator.vertex.Vertex;
import acqua.query.join.event.UpdateEvent;
import acqua.query.join.nmjoin.ApproximateNMJoinOperator;

import org.apache.xml.resolver.helpers.Debug;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OOFlexibleBudget extends OOFixedBudget {
	TreeSet<UpdateEvent> plannedUpdateEventsByTime;
	HashMap<Long, String> electedElements;
	int improvementInTheo = 0;

	private static final Logger logger = LoggerFactory.getLogger("OOFixedBudget");

	public OOFlexibleBudget(int ub) {
		super(ub);
		this.electedElements = new HashMap<Long, String>();
		this.plannedUpdateEventsByTime = new TreeSet<UpdateEvent>(UpdateEvent.Comparators.SCHEDULEDTIME);
	}

	public TreeSet<UpdateEvent> spendBudgetOnPlannedUpdateEvents(TreeSet<UpdateEvent> inputTreeSet, long currentTime) {
		if (inputTreeSet.size() > 0 && this.currentBudget > 0) {
			UpdateEvent tempEvent = inputTreeSet.first();
			while ((tempEvent != null) && (tempEvent.expiredEvalutionTime == currentTime) && (this.currentBudget > 0)) {
				tempEvent = inputTreeSet.pollFirst();
				this.electedElements.put((long) tempEvent.IDToUpdateInBKG, "0 " + tempEvent.IDToUpdateInBKG + " " + tempEvent.scoreForUpdate);
				this.spendUpdateBudget(1);
				if (inputTreeSet.size() > 0)
					tempEvent = inputTreeSet.first();
				else
					break;
			}
			return inputTreeSet;
		} else
			return inputTreeSet;
	}

	public void spendBudgetOnPlannedUpdateEventsAtBeginning(long currentTime) {
		logger.debug("planned Event " + this.plannedUpdateEventsByTime.toString());
		spendBudgetOnPlannedUpdateEvents(this.plannedUpdateEventsByTime, currentTime);
	}

	public TreeSet<UpdateEvent> spendBudgetOnNewUpdateEvents(List<UpdateEvent> newUpdateEvents, long currentTime) {
		TreeSet<UpdateEvent> currentExpiredUpdateByScore = new TreeSet<UpdateEvent>(UpdateEvent.Comparators.SCHEDULEDTIME);
		currentExpiredUpdateByScore.addAll(newUpdateEvents);
		TreeSet<UpdateEvent> resultSet = spendBudgetOnPlannedUpdateEvents(currentExpiredUpdateByScore, currentTime);
		return resultSet;
	}

	

	public List<UpdateEvent> getCurrentPlannedEvents(long evaluationTime) {
		List<UpdateEvent> results = new ArrayList<UpdateEvent>();

		if (this.plannedUpdateEventsByTime.size() > 0) {
			UpdateEvent tempEvent = this.plannedUpdateEventsByTime.first();
			while ((tempEvent != null) && (tempEvent.expiredEvalutionTime == evaluationTime)) {
				tempEvent = this.plannedUpdateEventsByTime.pollFirst();
				results.add(tempEvent);
				if (this.plannedUpdateEventsByTime.size() > 0)
					tempEvent = this.plannedUpdateEventsByTime.first();
				else
					break;
			}
		}
		return results;

	}

	/**
	 * there two two ways of exeucting planeed events
	 * 1) really execute it, but need two steps:
	 * 		exclude obseluted planned event
	 * 		remeber temp last update time
	 * 2) just get the credit of planned event
	 * 		plan everything as before.
	 */
	@Override
	protected HashMap<Long, String> updatePolicy(Iterator<Long> candidateFundSetIterator, Map<Long, Long> fundsTimeStampOfTheCurrentSlidedWindow, long evaluationTime) {
		logger.debug("current time is " + evaluationTime + " i have budget " + this.currentBudget);
		this.addBudgetForNewEvaluation();

		// init result data structure
		this.electedElements = new HashMap<Long, String>();

		HashMap<Integer, Long> tempLastUpdateTime = new HashMap<Integer, Long>();
		// get current planned Event from this.plannedUpdateEvnet
		List<UpdateEvent> plannedInCurrentTime = getCurrentPlannedEvents(evaluationTime);
		// get current update budget in this iteration
		int budgetForCurrentTime = this.getCurrentBudget() - this.plannedUpdateEventsByTime.size();
		// exlcude those planned events that have been done by unplanned income

		List<UpdateEvent> plannedButObselotedInCurrentTime = new ArrayList<UpdateEvent>();
		// if the planned event has been updated before
		// the planned event becomes obseloted
		for (UpdateEvent e : plannedInCurrentTime) {
			if (replicaInfoUpdateTime.get(e.IDToUpdateInBKG) + (long) RemoteBKGManager.INSTANCE.getChangeRate().get(e.IDToUpdateInBKG) > evaluationTime) {
				logger.debug("found an obseloted planned event");
				plannedButObselotedInCurrentTime.add(e);
			}
		}
		plannedInCurrentTime.removeAll(plannedButObselotedInCurrentTime);
		
		// execute those events
		for (UpdateEvent e : plannedInCurrentTime) {
			this.electedElements.put((long) e.IDToUpdateInBKG, "0 " + e.IDToUpdateInBKG + " " + e.scoreForUpdate);
			this.spendUpdateBudget(1);
			budgetForCurrentTime--;
			tempLastUpdateTime.put(e.IDToUpdateInBKG, (long) e.scheduledUpdateTime);
		}

		// get stocks in current window
		List<Vertex> fundsOfCurrentWindow = getFundsInCurrnetWindow(candidateFundSetIterator);
		biadjacencyMatrix subgraphOfCurrentWindow = RemoteBKGManager.INSTANCE.getBipartiteMappingGraph().getSubGraphForU(fundsOfCurrentWindow);
		List<Vertex> expriedVsInCurrentWindow = getExpiredVs(subgraphOfCurrentWindow.VMap.values(), evaluationTime, tempLastUpdateTime);

		// plan event for the current expired data
		List<UpdateEvent> currentExpriedUpdate = new ArrayList<UpdateEvent>();
		for (Vertex e : expriedVsInCurrentWindow) {
			int originalID = Vertex.getOriginalVertex(e).getIntID();
			int changeRate = RemoteBKGManager.INSTANCE.getChangeRate().get(originalID);
			// plan update events
			// TODO here is a short cut, need to convert from fund to stock
			long leavingWindowTime = fundsTimeStampOfTheCurrentSlidedWindow.get((long) originalID);
			leavingWindowTime += (long) Config.INSTANCE.getQueryWindowWidth();
			UpdateEvent tempEvent = planOneUpdateEvent(originalID, changeRate, leavingWindowTime, evaluationTime);
			currentExpriedUpdate.add(tempEvent);
		}
		logger.debug("currentExpriedUpdate " + currentExpriedUpdate.toString());

		// choose Top K
		List<UpdateEvent> selectedEvents = new ArrayList<UpdateEvent>();
		selectedEvents = chooseTopKByScore(budgetForCurrentTime, currentExpriedUpdate);
		logger.debug("selected events " + selectedEvents.toString());

		// currentExpriedUpdate is the temp update event
		// we check the future updates
		// if we are sure, there is not enough budget in future
		// we change the plan
		int currentTopKScore = getTotalScoreForAListOfUpdateEvents(selectedEvents);
		List<UpdateEvent> currentSelctedEvents = selectedEvents;

		logger.debug("current average " + currentSelctedEvents);
		List<UpdateEvent> topKFinalEvents = new ArrayList<UpdateEvent>();

		List<UpdateEvent> currentTempSelctedEvents = new ArrayList<UpdateEvent>();
		List<UpdateEvent> tempTotalOrder = new ArrayList<UpdateEvent>();
		List<UpdateEvent> moveAroundEvents = new ArrayList<UpdateEvent>();

		for (int i = 1; i < Config.INSTANCE.getSlideNumber(); i++) {
			long futureTempEvluationTime = evaluationTime + i * Config.INSTANCE.getQueryWindowSlide();

			List<Vertex> expriedVsIncurrentTempEvluationTime = getExpiredVs(subgraphOfCurrentWindow.VMap.values(), futureTempEvluationTime, tempLastUpdateTime);
			List<UpdateEvent> futureUpdateEventsInTempEvluationTime = new ArrayList<UpdateEvent>();
			for (Vertex e : expriedVsIncurrentTempEvluationTime) {

				int originalID = Vertex.getOriginalVertex(e).getIntID();
				int changeRate = RemoteBKGManager.INSTANCE.getChangeRate().get(originalID);
				// plan update events
				// TODO here is a short cut, need to convert from fund to stock
				long leavingWindowTime = fundsTimeStampOfTheCurrentSlidedWindow.get((long) originalID);
				leavingWindowTime += (long) Config.INSTANCE.getQueryWindowWidth();
				if (leavingWindowTime > futureTempEvluationTime) {
					UpdateEvent tempEvent = planOneUpdateEvent(originalID, changeRate, leavingWindowTime, futureTempEvluationTime);
					futureUpdateEventsInTempEvluationTime.add(tempEvent);
				}
			}
			// normal score
			currentTempSelctedEvents = new ArrayList<UpdateEvent>();
			// TODO to check
			currentTempSelctedEvents = chooseTopKByScore(this.getStandardBudget(), futureUpdateEventsInTempEvluationTime);
			int futureTopKScore = getTotalScoreForAListOfUpdateEvents(currentTempSelctedEvents);

			int normalTotalScore = currentTopKScore + futureTopKScore;

			tempTotalOrder = new ArrayList<UpdateEvent>();
			tempTotalOrder.addAll(currentSelctedEvents);
			tempTotalOrder.addAll(futureUpdateEventsInTempEvluationTime);
			moveAroundEvents = new ArrayList<UpdateEvent>();
			// TODO to check
			moveAroundEvents = chooseTopKByScore(budgetForCurrentTime + this.getStandardBudget(), tempTotalOrder);
			int moveAroundScore = getTotalScoreForAListOfUpdateEvents(moveAroundEvents);

			if (moveAroundScore > normalTotalScore) {
				if (currentSelctedEvents.size() == this.getStandardBudget())
					logger.debug("we have a true deficiency ");
				logger.debug("we have a deficiency, before Move" + currentSelctedEvents.toString());
				logger.debug("we have a deficiency, before Move" + currentTempSelctedEvents.toString());

				logger.debug("we have a deficiency, after Move" + moveAroundEvents.toString());
				topKFinalEvents = moveAroundEvents;
				improvementInTheo += moveAroundScore - normalTotalScore;
				break;
			}
		}

		if (topKFinalEvents.size() == 0) {
			topKFinalEvents = selectedEvents;
		}
		TreeSet<UpdateEvent> tempSet = this.spendBudgetOnNewUpdateEvents(topKFinalEvents, evaluationTime);
		TreeSet<UpdateEvent> tempSetForFuture = new TreeSet<UpdateEvent>(UpdateEvent.Comparators.SCORE);
		// number of planned event equals to the number of budget have been move around
		int numPlannedEvents = this.getStandardBudget() - (topKFinalEvents.size() - tempSet.size());

		while (numPlannedEvents > 0 && tempSet.size() > 0) {
			UpdateEvent tempEvent = tempSet.pollFirst();
			tempSetForFuture.add(tempEvent);
			numPlannedEvents--;
		}

		this.plannedUpdateEventsByTime.addAll(tempSetForFuture);
		// spend update budget
		logger.debug("budget left " + this.getCurrentBudget() + "improvement in theo " + improvementInTheo + " i have spend " + this.totalUpdateBudgetSpend);
		return this.electedElements;

	}
}
