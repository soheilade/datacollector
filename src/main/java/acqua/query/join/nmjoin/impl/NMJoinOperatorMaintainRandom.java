package acqua.query.join.nmjoin.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;

import acqua.config.Config;
import acqua.data.RemoteBKGManager;
import acqua.data.generator.matrix.biadjacencyMatrix;
import acqua.data.generator.matrix.biadjacencyMatrixIndegreeV;
import acqua.data.generator.vertex.Vertex;
import acqua.query.join.nmjoin.ApproximateNMJoinOperator;

public class NMJoinOperatorMaintainRandom extends ApproximateNMJoinOperator  {
	@Override
	protected HashMap<Long, String> updatePolicy(Iterator<Long> candidateFundSetIterator, Map<Long, Long> fundsTimeStampOfTheCurrentSlidedWindow, long evaluationTime) {

		List<Vertex> fundsOfCurrentWindow = getFundsInCurrnetWindow(candidateFundSetIterator);
		biadjacencyMatrix subgraphOfCurrentWindow = RemoteBKGManager.INSTANCE.getBipartiteMappingGraph().getSubGraphForU(fundsOfCurrentWindow);

		List<Vertex> Vlist=new ArrayList<Vertex>(subgraphOfCurrentWindow.VMap.values());
		
		List<Vertex> expriedVsInCurrentWindow = getExpiredVs(subgraphOfCurrentWindow.VMap.values(), evaluationTime);
		
		Random r=new Random(System.currentTimeMillis());
		HashMap<Long, String> electedElements = new HashMap<Long, String>();
		while (electedElements.size()<Config.INSTANCE.getUpdateBudget()&&electedElements.size()<expriedVsInCurrentWindow.size()){
			int index = r.nextInt(expriedVsInCurrentWindow.size());
			Vertex randomVertex = expriedVsInCurrentWindow.get(index);
			electedElements.put((long)randomVertex.getIntID(), randomVertex.toFullStringWithOriID());			
		}
		
		return electedElements;
	}

}
