package acqua.query.join.nmjoin.impl;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import acqua.config.Config;
import acqua.data.RemoteBKGManager;
import acqua.data.generator.matrix.biadjacencyMatrix;
import acqua.data.generator.matrix.biadjacencyMatrixIndegreeV;
import acqua.data.generator.vertex.Vertex;
import acqua.query.join.nmjoin.ApproximateNMJoinOperator;

public class NMJoinOperatorMaintainBasedOnIndegreeV extends ApproximateNMJoinOperator {
	@Override
	protected HashMap<Long, String> updatePolicy(Iterator<Long> candidateFundSetIterator, Map<Long, Long> fundsTimeStampOfTheCurrentSlidedWindow, long evaluationTime) {

		List<Vertex> fundsOfCurrentWindow = getFundsInCurrnetWindow(candidateFundSetIterator);
		biadjacencyMatrix subgraphOfCurrentWindow = RemoteBKGManager.INSTANCE.getBipartiteMappingGraph().getSubGraphForU(fundsOfCurrentWindow);

		List<Vertex> expriedVsInCurrentWindow = getExpiredVs(subgraphOfCurrentWindow.VMap.values(), evaluationTime);
		biadjacencyMatrix subgraphForUpating = subgraphOfCurrentWindow.getSubGraphForV(expriedVsInCurrentWindow);

		biadjacencyMatrixIndegreeV weightedSubgraphForUpdating = new biadjacencyMatrixIndegreeV(subgraphForUpating);
		HashMap<Long, String> electedElements = weightedSubgraphForUpdating.getVsWithInBudget(Config.INSTANCE.getUpdateBudget());

		return electedElements;
	}
}
