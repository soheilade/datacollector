package acqua.query.join.nmjoin.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;

import acqua.config.Config;
import acqua.data.RemoteBKGManager;
import acqua.data.generator.matrix.biadjacencyMatrix;
import acqua.data.generator.vertex.Vertex;
import acqua.query.join.nmjoin.ApproximateNMJoinOperator;

public class NMJoinOperatorMaintainLRU extends ApproximateNMJoinOperator  {
	@Override
	protected HashMap<Long, String> updatePolicy(Iterator<Long> candidateFundSetIterator, Map<Long, Long> fundsTimeStampOfTheCurrentSlidedWindow, long evaluationTime) {

		final class Stock{
			int sId;
			long updateTimeDiff;
			public Stock(int id,long t){sId=id;updateTimeDiff=t;}
		}
		List<Vertex> fundsOfCurrentWindow = getFundsInCurrnetWindow(candidateFundSetIterator);
		biadjacencyMatrix subgraphOfCurrentWindow = RemoteBKGManager.INSTANCE.getBipartiteMappingGraph().getSubGraphForU(fundsOfCurrentWindow);

		List<Vertex> Vlist=new ArrayList<Vertex>(subgraphOfCurrentWindow.VMap.values());
		
		List<Vertex> expriedVsInCurrentWindow = getExpiredVs(subgraphOfCurrentWindow.VMap.values(), evaluationTime);
		
		HashMap<Integer,Vertex> expiredVs=new HashMap<Integer,Vertex>();
		
		List<Stock> rUpdateLatency=new ArrayList<Stock>();
		
		for(int c=0;c<expriedVsInCurrentWindow.size();c++){
			int sid=expriedVsInCurrentWindow.get(c).getIntID();
			long latestUpdateTime = replicaInfoUpdateTime.get(sid);
			expiredVs.put(sid, expriedVsInCurrentWindow.get(c));
			rUpdateLatency.add(new Stock(sid, evaluationTime-latestUpdateTime));				
		}
		Collections.sort(rUpdateLatency, new Comparator<Stock>() {

			public int compare(Stock o1, Stock o2) {
				int res=(int)(o2.updateTimeDiff - o1.updateTimeDiff);
				//if(res==0)
				//	res=(int)(o2.updateTimeDiff - o1.updateTimeDiff);
				return res;
			}
		});
		
		HashMap<Long, String> electedElements = new HashMap<Long, String>();
		int index=0;
		while (electedElements.size()<Config.INSTANCE.getUpdateBudget() && electedElements.size()<expriedVsInCurrentWindow.size()){
			//System.out.println(index+" "+rUpdateLatency.size());
			int tsid=rUpdateLatency.get(index).sId;
			Vertex lruVertex = expiredVs.get(tsid);
			index++;
			electedElements.put((long)lruVertex.getIntID(), lruVertex.toFullStringWithOriID());			
		}		
		return electedElements;
	}
}
