package acqua.query.join.nmjoin.impl;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import acqua.config.Config;
import acqua.data.RemoteBKGManager;
import acqua.data.generator.matrix.biadjacencyMatrix;
import acqua.data.generator.matrix.biadjacencyMatrixWeightedV;
import acqua.data.generator.vertex.Vertex;
import acqua.query.join.nmjoin.ApproximateNMJoinOperator;



//import org.apache.;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

public class NMJoinOperatorMaintainBasedOnWeightV extends ApproximateNMJoinOperator {
	private static final Logger logger = LogManager.getLogger(NMJoinOperatorMaintainBasedOnWeightV.class);

	@Override
	protected HashMap<Long, String> updatePolicy(Iterator<Long> candidateFundSetIterator, Map<Long, Long> fundsTimeStampOfTheCurrentSlidedWindow, long evaluationTime) {

		List<Vertex> fundsOfCurrentWindow = getFundsInCurrnetWindow(candidateFundSetIterator);
		biadjacencyMatrix subgraphOfCurrentWindow = RemoteBKGManager.INSTANCE.getBipartiteMappingGraph().getSubGraphForU(fundsOfCurrentWindow);

		List<Vertex> expriedVsInCurrentWindow = getExpiredVs(subgraphOfCurrentWindow.VMap.values(), evaluationTime);
		biadjacencyMatrix subgraphForUpating = subgraphOfCurrentWindow.getSubGraphForV(expriedVsInCurrentWindow);

		biadjacencyMatrixWeightedV weightedSubgraphForUpdating = new biadjacencyMatrixWeightedV(subgraphForUpating);

		HashMap<Long, String> electedElements = weightedSubgraphForUpdating.getVsWithInBudget(Config.INSTANCE.getUpdateBudget());

		return electedElements;
	}
}
/*
if (logger.isDebugEnabled() && electedElements.size() > 1) {
			logger.debug("current time is: " + evaluationTime);
			for (Vertex v : fundsOfCurrentWindow) {
				logger.debug(v.getIntID() + " ");
			}
			logger.debug(" fund are in current window\n");
			for (Vertex v : subgraphOfCurrentWindow.VMap.values()) {
				logger.debug(v.getIntID() + "\\" + v.originalVertex.getIntID() + " ");
			}
			logger.debug(" are stock in current window\n");

			// subgraphOfCurrentWindow.printBooleanMatrix();

			for (Vertex v : expriedVsInCurrentWindow) {
				logger.debug(v.getIntID() + "\\" + v.originalVertex.getIntID() + " ");
				// logger.debug(Vertex.getOriginalVertex(v) + " ");
			}
			logger.debug(" are expired\n");
			logger.debug("unweighted graph\n");
			subgraphOfCurrentWindow.printBooleanMatrix();
			logger.debug("weighted boolean graph\n");
			weightedSubgraphForUpdating.printBooleanMatrix();
			logger.debug("weighted graph\n");
			weightedSubgraphForUpdating.printWieghtedMatrix();
			for (Entry<Long, String> e : electedElements.entrySet()) {
				System.out.println(e.getKey() + " " + e.getValue());
				// logger.debug(Vertex.getOriginalVertex(v) + " ");
			}
			logger.debug(" are udpated\n");
		}
 */
