package acqua.query.join.nmjoin.impl;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import acqua.config.Config;
import acqua.data.RemoteBKGManager;
import acqua.data.generator.matrix.biadjacencyMatrix;
import acqua.data.generator.matrix.biadjacencyMatrixOutdegreeU;
import acqua.data.generator.vertex.Vertex;
import acqua.query.join.nmjoin.ApproximateNMJoinOperator;

/*********************
 * 
 * @author sohdeh the idea of updating based on U: it starts from the funds with lowest out-degree and it updates all its associated stocks. therefore their associated stocks become updated and their
 *         corresponding edges to funds are removed. again it chooses the fund with lowest remaining out-degree and continue the process. the order in which stocks are picked determines the
 *         maintenance order.
 * 
 */

public class NMJoinOperatorMaintainBasedOnOutdegreeU extends ApproximateNMJoinOperator {

	@Override
	protected HashMap<Long, String> updatePolicy(Iterator<Long> candidateFundSetIterator, Map<Long, Long> fundsTimeStampOfTheCurrentSlidedWindow, long evaluationTime) {

		List<Vertex> fundsOfCurrentWindow = getFundsInCurrnetWindow(candidateFundSetIterator);
		biadjacencyMatrix subgraphOfCurrentWindow = RemoteBKGManager.INSTANCE.getBipartiteMappingGraph().getSubGraphForU(fundsOfCurrentWindow);

		List<Vertex> expriedVsInCurrentWindow = getExpiredVs(subgraphOfCurrentWindow.VMap.values(), evaluationTime);
		biadjacencyMatrix subgraphForUpating = subgraphOfCurrentWindow.getSubGraphForV(expriedVsInCurrentWindow);

		biadjacencyMatrixOutdegreeU outdegreedSubgraphForUpdating = new biadjacencyMatrixOutdegreeU(subgraphForUpating);
		HashMap<Long, String> electedElements = outdegreedSubgraphForUpdating.removeWithInBudget(Config.INSTANCE.getUpdateBudget());

		return electedElements;
	}

}
