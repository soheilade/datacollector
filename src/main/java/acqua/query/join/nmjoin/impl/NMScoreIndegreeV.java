package acqua.query.join.nmjoin.impl;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import acqua.config.Config;
import acqua.data.RemoteBKGManager;
import acqua.data.generator.matrix.biadjacencyMatrix;
import acqua.data.generator.vertex.Vertex;
import acqua.query.join.event.UpdateEventNM;
import acqua.query.join.nmjoin.ApproximateNMJoinOperator;

public class NMScoreIndegreeV extends ApproximateNMJoinOperator {
	private static final Logger logger = LoggerFactory.getLogger("NMIndegreeVScore");

	public TreeSet<UpdateEventNM> planUpdateEvents(Map<Long, Long> fundsTimeStampOfTheCurrentSlidedWindow, biadjacencyMatrix subgraphForUpating, List<Vertex> expriedVsInCurrentWindow, long evaluationTime) {

		TreeSet<UpdateEventNM> results = new TreeSet<UpdateEventNM>(UpdateEventNM.Comparators.TOTALINDEGREEVSCORE);
		for (Vertex e : expriedVsInCurrentWindow) {
			List<Vertex> neighbours = subgraphForUpating.getNeighboursForV(e.getIntID());
			long lastTime = 0;
			int originalIDV = Vertex.getOriginalVertex(e).getIntID();
			// get leaving time
			for (Vertex v : neighbours) {
				int originalIDU = Vertex.getOriginalVertex(v).getIntID();
				if (fundsTimeStampOfTheCurrentSlidedWindow.get((long) originalIDU) > lastTime)
					lastTime = fundsTimeStampOfTheCurrentSlidedWindow.get((long) originalIDU);
			}
			// for each fund calculate the score
			lastTime += Config.INSTANCE.getQueryWindowWidth();
			int changeRate = RemoteBKGManager.INSTANCE.getChangeRate().get(originalIDV);
			UpdateEventNM tempEvent = UpdateEventNM.planOneUpdateEventForV(originalIDV, changeRate, lastTime, evaluationTime);
			for (Vertex v : neighbours) {
				int originalIDU = Vertex.getOriginalVertex(v).getIntID();
				long enterTime = fundsTimeStampOfTheCurrentSlidedWindow.get((long) originalIDU);
				long leavingTime = enterTime + Config.INSTANCE.getQueryWindowWidth();
				tempEvent.addIndegreeToFundToScore(originalIDU, leavingTime, evaluationTime, changeRate);
			}
			results.add(tempEvent);
			logger.debug("tempEvent:" + tempEvent + " scores " + tempEvent.fundToScore + " originalScore " + tempEvent.scoreForUpdate);
		}
		logger.debug("even list:" + results);
		return results;
	}

	@Override
	protected HashMap<Long, String> updatePolicy(Iterator<Long> candidateFundSetIterator, Map<Long, Long> fundsTimeStampOfTheCurrentSlidedWindow, long evaluationTime) {

		List<Vertex> fundsOfCurrentWindow = getFundsInCurrnetWindow(candidateFundSetIterator);
		biadjacencyMatrix subgraphOfCurrentWindow = RemoteBKGManager.INSTANCE.getBipartiteMappingGraph().getSubGraphForU(fundsOfCurrentWindow);

		List<Vertex> expriedVsInCurrentWindow = getExpiredVs(subgraphOfCurrentWindow.VMap.values(), evaluationTime);

		// plan event for the current expired data
		TreeSet<UpdateEventNM> currentExpriedUpdate = new TreeSet<UpdateEventNM>();

		currentExpriedUpdate = planUpdateEvents(fundsTimeStampOfTheCurrentSlidedWindow, subgraphOfCurrentWindow, expriedVsInCurrentWindow, evaluationTime);
		// choose TopK from currentExpriedUpdate
		int tempBudget = Config.INSTANCE.getUpdateBudget();
		HashMap<Long, String> electedElements = new HashMap<Long, String>();
		while (tempBudget > 0 && currentExpriedUpdate.size() > 0) {
			UpdateEventNM tempEvent = currentExpriedUpdate.pollLast();
			electedElements.put((long) tempEvent.IDToUpdateInBKG, tempEvent.IDToUpdateInBKG + " " + tempEvent.IDToUpdateInBKG + " " + tempEvent.totalIndegreeByScore);
			tempBudget--;
		}
		logger.debug("elements" + electedElements);

		return electedElements;
	}
}
