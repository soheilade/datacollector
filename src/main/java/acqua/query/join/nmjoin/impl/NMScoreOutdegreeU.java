package acqua.query.join.nmjoin.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import acqua.config.Config;
import acqua.data.RemoteBKGManager;
import acqua.data.generator.matrix.biadjacencyMatrix;
import acqua.data.generator.matrix.biadjacencyMatrixIndegreeV;
import acqua.data.generator.matrix.biadjacencyMatrixOutdegreeU;
import acqua.data.generator.vertex.Vertex;
import acqua.query.join.event.UpdateEvent;
import acqua.query.join.event.UpdateEventNM;
import acqua.query.join.nmjoin.ApproximateNMJoinOperator;

public class NMScoreOutdegreeU extends ApproximateNMJoinOperator {
	private static final Logger logger = LoggerFactory.getLogger("NMOutdegreeUScore");

	private HashMap<Long, UpdateEvent> getVsWithInBudget(TreeSet<UpdateEventNM> rankOfUEvents, TreeSet<UpdateEvent> rankOfVEvents, int budget) {
		int tempBudget = budget;
		HashMap<Long, UpdateEvent> electedElements = new HashMap<Long, UpdateEvent>();

		Iterator<UpdateEventNM> itUEvent = rankOfUEvents.descendingIterator();
		// here is another bin pakcing problem
		// we greedyly choose by score
		while (itUEvent.hasNext() && tempBudget > 0) {
			UpdateEventNM tempUEvent = itUEvent.next();
			if (tempUEvent.StockToScore.size() >= tempBudget) {
				TreeSet<UpdateEvent> tempVsForAnUEvent = new TreeSet<UpdateEvent>(UpdateEvent.Comparators.SCORE);
				tempVsForAnUEvent.addAll(tempUEvent.StockToScore.values());

				while (tempBudget > 0 && tempVsForAnUEvent.size() > 0) {
					UpdateEvent tempEvent = tempVsForAnUEvent.pollLast();
					if (!electedElements.containsKey(tempEvent.IDToUpdateInBKG)) {
						electedElements.put((long) tempEvent.IDToUpdateInBKG, tempEvent);
						tempBudget--;
					}
				}
			}
		}
		while (tempBudget > 0 && rankOfVEvents.size() > 0) {
			UpdateEvent tempEvent = rankOfVEvents.pollLast();
			if (!electedElements.containsKey(tempEvent.IDToUpdateInBKG)) {
				electedElements.put((long) tempEvent.IDToUpdateInBKG, tempEvent);
				tempBudget--;
			}
		}
		return electedElements;

	}

	public HashMap<Long, UpdateEvent> planUpdateEventsWithInBudget(Map<Long, Long> fundsTimeStampOfTheCurrentSlidedWindow, biadjacencyMatrix subgraphForUpating, List<Vertex> expriedVsInCurrentWindow, long evaluationTime, int budget) {
		TreeSet<UpdateEventNM> rankOfUEvents = new TreeSet<UpdateEventNM>(UpdateEventNM.Comparators.TOTALOUTDEGREEUSCORE);
		HashMap<Integer, UpdateEvent> VsScoreMap = new HashMap<Integer, UpdateEvent>();
		TreeSet<UpdateEvent> rankOfVEvents = new TreeSet<UpdateEvent>(UpdateEvent.Comparators.SCORE);
		for (Vertex e : expriedVsInCurrentWindow) {
			List<Vertex> neighbours = subgraphForUpating.getNeighboursForV(e.getIntID());
			long lastTime = 0;
			int originalIDV = Vertex.getOriginalVertex(e).getIntID();
			// get leaving time
			for (Vertex v : neighbours) {
				int originalIDU = Vertex.getOriginalVertex(v).getIntID();
				if (fundsTimeStampOfTheCurrentSlidedWindow.get((long) originalIDU) > lastTime)
					lastTime = fundsTimeStampOfTheCurrentSlidedWindow.get((long) originalIDU);
			}
			// for each fund calculate the score
			lastTime += Config.INSTANCE.getQueryWindowWidth();
			int changeRate = RemoteBKGManager.INSTANCE.getChangeRate().get(originalIDV);
			UpdateEvent tempEventV = UpdateEvent.planOneUpdateEvent(originalIDV, changeRate, lastTime, evaluationTime);
			VsScoreMap.put(originalIDV, tempEventV);
			rankOfVEvents.add(tempEventV);

		}

		for (Vertex e : subgraphForUpating.UMap.values()) {
			List<Vertex> neighbours = subgraphForUpating.getNeighboursForU(e.getIntID());
			int originalIDU = Vertex.getOriginalVertex(e).getIntID();
			HashMap<Integer, UpdateEvent> tempStockToScoreMap = new HashMap<Integer, UpdateEvent>();
			// get leaving time
			int minScore = Integer.MAX_VALUE;
			for (Vertex v : neighbours) {
				int originalIDV = Vertex.getOriginalVertex(v).getIntID();
				UpdateEvent tempVScore = VsScoreMap.get(originalIDV);
				tempStockToScoreMap.put(originalIDV, tempVScore);
				if (tempVScore.scoreForUpdate < minScore)
					minScore = tempVScore.scoreForUpdate;
			}
			double score = (double) minScore / (double) neighbours.size();
			UpdateEventNM eventForU = new UpdateEventNM(originalIDU, score, tempStockToScoreMap);
			rankOfUEvents.add(eventForU);
			logger.debug("planned an Uevent:" + eventForU.toFundFullString());
		}

		// if (logger.isDebugEnabled()) {
		// System.out.println("rankOfUEvents");
		// for (UpdateEventNM e : rankOfUEvents)
		// System.out.print("[" + e.toFundScoreString() + "] ");
		// System.out.print("\n");
		// }
		HashMap<Long, UpdateEvent> electedElements = this.getVsWithInBudget(rankOfUEvents, rankOfVEvents, budget);
		
		
		
		
		return electedElements;
	}

	@Override
	protected HashMap<Long, String> updatePolicy(Iterator<Long> candidateFundSetIterator, Map<Long, Long> fundsTimeStampOfTheCurrentSlidedWindow, long evaluationTime) {

		// biadjacencyMatrix subgraphForUpating = subgraphOfCurrentWindow.getSubGraphForV(expriedVsInCurrentWindow);
		// biadjacencyMatrixOutdegreeU outdegreedSubgraphForUpdating = new biadjacencyMatrixOutdegreeU(subgraphForUpating);
		// HashMap<Long, String> electedElements = outdegreedSubgraphForUpdating.removeWithInBudget(Config.INSTANCE.getUpdateBudget());

		List<Vertex> fundsOfCurrentWindow = getFundsInCurrnetWindow(candidateFundSetIterator);
		biadjacencyMatrix subgraphOfCurrentWindow = RemoteBKGManager.INSTANCE.getBipartiteMappingGraph().getSubGraphForU(fundsOfCurrentWindow);

		List<Vertex> expriedVsInCurrentWindow = getExpiredVs(subgraphOfCurrentWindow.VMap.values(), evaluationTime);
		biadjacencyMatrix subgraphForUpating = subgraphOfCurrentWindow.getSubGraphForV(expriedVsInCurrentWindow);
		List<Vertex> expriedVsInSubgraphForUpating = new ArrayList<Vertex>(subgraphForUpating.VMap.values());
		// plan event for the current expired data
		
		
		HashMap<Long, UpdateEvent> electedElements = new HashMap<Long, UpdateEvent>();
		electedElements = planUpdateEventsWithInBudget(fundsTimeStampOfTheCurrentSlidedWindow, subgraphForUpating, expriedVsInSubgraphForUpating, evaluationTime, Config.INSTANCE.getUpdateBudget());

		HashMap<Long, String> electedElementReturn = new HashMap<Long, String>();
		for (Entry<Long, UpdateEvent> e : electedElements.entrySet()) {
			electedElementReturn.put(e.getKey(), e.getValue().IDToUpdateInBKG + " " + e.getValue().IDToUpdateInBKG + " " + e.getValue().scoreForUpdate);
		}
		
		// biadjacencyMatrixOutdegreeU outdegreedSubgraphForUpdating = new biadjacencyMatrixOutdegreeU(subgraphForUpating);
		// HashMap<Long, String> electedElementsTest = outdegreedSubgraphForUpdating.removeWithInBudget(Config.INSTANCE.getUpdateBudget());
		// logger.debug("elecvted VeventTest:" + electedElementsTest);
		//
		return electedElementReturn;
	}

}
