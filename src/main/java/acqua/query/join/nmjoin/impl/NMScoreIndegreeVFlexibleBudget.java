package acqua.query.join.nmjoin.impl;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import acqua.config.Config;
import acqua.data.RemoteBKGManager;
import acqua.data.generator.matrix.biadjacencyMatrix;
import acqua.data.generator.vertex.Vertex;
import acqua.query.join.event.UpdateEvent;
import acqua.query.join.event.UpdateEventNM;
import acqua.query.join.nmjoin.ApproximateNMJoinOperator;

public class NMScoreIndegreeVFlexibleBudget extends ApproximateNMJoinOperator {
	private static final Logger logger = LoggerFactory.getLogger("NMScoreIndegreeVFlexibleBudget");

	TreeSet<UpdateEventNM> plannedUpdateEventsByTime = new TreeSet<UpdateEventNM>(UpdateEvent.Comparators.SCHEDULEDTIME);
	HashMap<Long, String> electedElements;
	int improvementInTheo = 0;
	long startingTime = 0;

	public NMScoreIndegreeVFlexibleBudget(int budget) {
		this.updateBudget = budget;
		this.currentBudget = 0;
		this.startingTime = System.currentTimeMillis();
	}

	public TreeSet<UpdateEventNM> planUpdateEvents(Map<Long, Long> fundsTimeStampOfTheCurrentSlidedWindow, biadjacencyMatrix subgraphForUpating, List<Vertex> expriedVsInCurrentWindow, long evaluationTime) {

		TreeSet<UpdateEventNM> results = new TreeSet<UpdateEventNM>(UpdateEventNM.Comparators.TOTALINDEGREEVSCORE);
		for (Vertex e : expriedVsInCurrentWindow) {
			List<Vertex> neighbours = subgraphForUpating.getNeighboursForV(e.getIntID());
			long lastTime = 0;
			int originalIDV = Vertex.getOriginalVertex(e).getIntID();
			// get leaving time
			for (Vertex v : neighbours) {
				int originalIDU = Vertex.getOriginalVertex(v).getIntID();
				if (fundsTimeStampOfTheCurrentSlidedWindow.get((long) originalIDU) > lastTime)
					lastTime = fundsTimeStampOfTheCurrentSlidedWindow.get((long) originalIDU);
			}
			// for each fund calculate the score
			lastTime += Config.INSTANCE.getQueryWindowWidth();
			int changeRate = RemoteBKGManager.INSTANCE.getChangeRate().get(originalIDV);
			UpdateEventNM tempEvent = UpdateEventNM.planOneUpdateEventForV(originalIDV, changeRate, lastTime, evaluationTime);
			for (Vertex v : neighbours) {
				int originalIDU = Vertex.getOriginalVertex(v).getIntID();
				long enterTime = fundsTimeStampOfTheCurrentSlidedWindow.get((long) originalIDU);
				long leavingTime = enterTime + Config.INSTANCE.getQueryWindowWidth();
				tempEvent.addIndegreeToFundToScore(originalIDU, leavingTime, evaluationTime, changeRate);
			}
			results.add(tempEvent);
			// logger.debug("tempEvent:" + tempEvent + " scores " + tempEvent.fundToScore + " originalScore " + tempEvent.scoreForUpdate);
		}
		//logger.debug("event list:" + results);
		return results;
	}

	List<UpdateEventNM> removeTopKFromTreeSet(TreeSet<UpdateEventNM> input, int budget) {
		List<UpdateEventNM> currentSelctedEvents = new ArrayList<UpdateEventNM>();
		int tempBudget = budget;
		while (tempBudget > 0 && input.size() > 0) {
			UpdateEventNM tempEvent = input.pollLast();
			currentSelctedEvents.add(tempEvent);
			tempBudget--;
		}
		return currentSelctedEvents;
	}
	List<UpdateEventNM> selectTopKFromTreeSet(TreeSet<UpdateEventNM> input, int budget) {
		List<UpdateEventNM> currentSelctedEvents = new ArrayList<UpdateEventNM>();
		int tempBudget = budget;
		Iterator<UpdateEventNM> it = input.descendingIterator();
		while (tempBudget > 0 && it.hasNext()) {
			currentSelctedEvents.add(it.next());
			tempBudget--;
		}
		return currentSelctedEvents;
	}

	public int getTotalScoreForAListOfUpdateEventNMs(List<UpdateEventNM> input) {
		int result = 0;
		for (UpdateEventNM up : input) {
			// logger.debug(up + " score "+ up.totalIndegreeByScore +" results " + result);
			result += up.totalIndegreeByScore;
		}
		return result;
	}

	@Override
	protected HashMap<Long, String> updatePolicy(Iterator<Long> candidateFundSetIterator, Map<Long, Long> fundsTimeStampOfTheCurrentSlidedWindow, long evaluationTime) {
		logger.debug("current time is " + evaluationTime + " i have budget " + this.currentBudget);
		addBudgetForNewEvaluation();

		HashMap<Long, String> electedElements = new HashMap<Long, String>();
		HashMap<Integer, Long> tempLastUpdateTime = new HashMap<Integer, Long>();

		
		List<UpdateEventNM> plannedCurrent = new ArrayList<UpdateEventNM>();
		List<UpdateEventNM> plannedFuture = new ArrayList<UpdateEventNM>();

		this.splitOutCurrentEvent(new ArrayList<UpdateEventNM>(this.plannedUpdateEventsByTime), plannedCurrent, plannedFuture, evaluationTime);

		this.plannedUpdateEventsByTime.removeAll(plannedCurrent);
		
		int budgetForCurrentTime = this.getCurrentBudget() - this.plannedUpdateEventsByTime.size();
		
		
		List<UpdateEventNM> plannedButObselotedInCurrentTime = new ArrayList<UpdateEventNM>();
		// if the planned event has been updated before
		// the planned event becomes obseloted
		for (UpdateEventNM e : plannedCurrent) {
			//if (replicaInfoUpdateTime.get(e.IDToUpdateInBKG) + (long) RemoteBKGManager.INSTANCE.getChangeRate().get(e.IDToUpdateInBKG) > evaluationTime) {
			//	logger.debug("found an obseloted planned event");
				plannedButObselotedInCurrentTime.add(e);
			//}
		}
		plannedCurrent.removeAll(plannedButObselotedInCurrentTime);
		
		for (UpdateEventNM tempEvent : plannedCurrent) {
			electedElements.put((long) tempEvent.IDToUpdateInBKG, tempEvent.IDToUpdateInBKG + " " + tempEvent.IDToUpdateInBKG + " " + tempEvent.totalIndegreeByScore);
			tempLastUpdateTime.put(tempEvent.IDToUpdateInBKG, (long) tempEvent.scheduledUpdateTime);
		}
		this.spendUpdateBudget(plannedCurrent.size());
		

		List<Vertex> fundsOfCurrentWindow = getFundsInCurrnetWindow(candidateFundSetIterator);

		// plan event for the current expired data
		TreeSet<UpdateEventNM> currentExpriedUpdate = new TreeSet<UpdateEventNM>();
		biadjacencyMatrix subgraphOfCurrentWindow = RemoteBKGManager.INSTANCE.getBipartiteMappingGraph().getSubGraphForU(fundsOfCurrentWindow);
		List<Vertex> expriedVsInCurrentWindow = getExpiredVs(subgraphOfCurrentWindow.VMap.values(), evaluationTime, tempLastUpdateTime);
		currentExpriedUpdate = planUpdateEvents(fundsTimeStampOfTheCurrentSlidedWindow, subgraphOfCurrentWindow, expriedVsInCurrentWindow, evaluationTime);

		// choose TopK from currentExpriedUpdate

		List<UpdateEventNM> currentSelctedEvents = removeTopKFromTreeSet(currentExpriedUpdate, budgetForCurrentTime);

		int currentTopKScore = getTotalScoreForAListOfUpdateEventNMs(currentSelctedEvents);

		
//		for (UpdateEventNM tempEvent : currentSelctedEvents) {
//			electedElements.put((long) tempEvent.IDToUpdateInBKG, tempEvent.IDToUpdateInBKG + " " + tempEvent.IDToUpdateInBKG + " " + tempEvent.totalIndegreeByScore);
//			tempLastUpdateTime.put(tempEvent.IDToUpdateInBKG, (long) tempEvent.scheduledUpdateTime);
//		}
		
		List<UpdateEventNM> futureSelctedEvents = new ArrayList<UpdateEventNM>();
		TreeSet<UpdateEventNM> moveAroundEventsInOrder = new TreeSet<UpdateEventNM>(UpdateEventNM.Comparators.TOTALINDEGREEVSCORE);
		List<UpdateEventNM> topKMoveAroundEvents = new ArrayList<UpdateEventNM>();

		int futureTopKScore = 0;
		int normalTotalScore = 0;
		int moveAroundScore = 0;

		List<UpdateEventNM> topFinalEvents = new ArrayList<UpdateEventNM>();
		// plan events for the future expired data
		for (int i = 1; i < Config.INSTANCE.getSlideNumber(); i++) {
			long futureTempEvluationTime = evaluationTime + i * Config.INSTANCE.getQueryWindowSlide();
			List<Vertex> fundsOfFutureWindow = getFundsInAWindow(fundsOfCurrentWindow, fundsTimeStampOfTheCurrentSlidedWindow, futureTempEvluationTime);

			TreeSet<UpdateEventNM> futureExpriedUpdate = new TreeSet<UpdateEventNM>();
			biadjacencyMatrix subgraphOfFutureWindow = RemoteBKGManager.INSTANCE.getBipartiteMappingGraph().getSubGraphForU(fundsOfFutureWindow);
			List<Vertex> expriedVsInFutureWindow = getExpiredVs(subgraphOfFutureWindow.VMap.values(), futureTempEvluationTime, tempLastUpdateTime);
			// if (expriedVsInFutureWindow.size() > expriedVsInCurrentWindow.size()) {
			// logger.debug("here");
			// }
			futureExpriedUpdate = planUpdateEvents(fundsTimeStampOfTheCurrentSlidedWindow, subgraphOfFutureWindow, expriedVsInFutureWindow, futureTempEvluationTime);
			// logger.debug("futureExpriedUpdate" + futureExpriedUpdate.toString());
			futureSelctedEvents = selectTopKFromTreeSet(futureExpriedUpdate, this.getStandardBudget());
			//futureSelctedEvents = removeTopKFromTreeSet(futureExpriedUpdate, this.getStandardBudget());
			
			futureTopKScore = getTotalScoreForAListOfUpdateEventNMs(futureSelctedEvents);
			normalTotalScore = currentTopKScore + futureTopKScore;

			moveAroundEventsInOrder = new TreeSet<UpdateEventNM>(UpdateEventNM.Comparators.TOTALINDEGREEVSCORE);

			moveAroundEventsInOrder.addAll(currentSelctedEvents);
			moveAroundEventsInOrder.addAll(futureExpriedUpdate);

			topKMoveAroundEvents = removeTopKFromTreeSet(moveAroundEventsInOrder, budgetForCurrentTime + this.getStandardBudget());

			moveAroundScore = getTotalScoreForAListOfUpdateEventNMs(topKMoveAroundEvents);
			// logger.debug("old score: " + normalTotalScore + " new score: " + moveAroundScore + " current score: " + currentTopKScore + " future score: " + futureTopKScore);
			if (moveAroundScore > normalTotalScore) {
				logger.debug("we have a deficiency");
				logger.debug("we have a deficiency, before Move" + currentSelctedEvents.toString());
				logger.debug("we have a deficiency, before Move" + futureExpriedUpdate.toString());
				logger.debug("we have a deficiency, after Move" + topKMoveAroundEvents.toString());
				
				improvementInTheo += moveAroundScore - normalTotalScore;
				topFinalEvents = topKMoveAroundEvents;
				break;
			}
		}

		if (topFinalEvents.size() == 0) {
			topFinalEvents = currentSelctedEvents;
		}

		List<UpdateEventNM> currentEvents = new ArrayList<UpdateEventNM>();
		List<UpdateEventNM> futureEvents = new ArrayList<UpdateEventNM>();

		this.splitOutCurrentEvent(topFinalEvents, currentEvents, futureEvents, evaluationTime);

		
		
		// number of planned event equals to the number of budget have been move around
		int numPlannedEvents = this.getStandardBudget() - currentEvents.size();
		TreeSet<UpdateEventNM> tempFutureOrdered = new TreeSet<UpdateEventNM>(UpdateEventNM.Comparators.TOTALINDEGREEVSCORE);
		tempFutureOrdered.addAll(futureEvents);
		while (numPlannedEvents > 0 && tempFutureOrdered.size() > 0) {
			UpdateEventNM tempEvent = tempFutureOrdered.pollLast();
			this.plannedUpdateEventsByTime.add(tempEvent);
			numPlannedEvents--;
		}


		TreeSet<UpdateEventNM> spendingBudget = new TreeSet<UpdateEventNM>(UpdateEventNM.Comparators.TOTALINDEGREEVSCORE);
		spendingBudget.addAll(currentEvents);
		int expenduture = 0;
		while (budgetForCurrentTime > 0 && spendingBudget.size() > 0) {
			UpdateEventNM tempEvent = spendingBudget.pollLast();
			electedElements.put((long) tempEvent.IDToUpdateInBKG, tempEvent.IDToUpdateInBKG + " " + tempEvent.IDToUpdateInBKG + " " + tempEvent.totalIndegreeByScore);
			budgetForCurrentTime--;
			expenduture++;
		}

		this.spendUpdateBudget(expenduture);

		logger.debug("elements" + electedElements);
		logger.debug("budget left " + this.getCurrentBudget() + "improvement in theo " + improvementInTheo + " i have spend " + this.totalUpdateBudgetSpend);

		try {
			String filename = "./log/NMScoreIndegreeVFlexibleBudget" + this.startingTime + ".txt";
			FileWriter fw = new FileWriter(filename, true); // the true will append the new data
			fw.write("currentBudgetLeft: " + this.getCurrentBudget() + " budgetUsedThisEva: " + expenduture + " currentPlannedEvents: " + this.plannedUpdateEventsByTime.size() +"\n");// appends the string to
																																												// the file
			fw.close();
		} catch (IOException ioe) 
		{
			
			System.err.println("IOExceptions: " + ioe.getMessage());
		}
		
		return electedElements;
	}

	public void splitOutCurrentEvent(List<UpdateEventNM> input, List<UpdateEventNM> current, List<UpdateEventNM> future, long evaluationTime) {
		for (UpdateEventNM e : input) {
			if (e.expiredEvalutionTime == evaluationTime) {
				current.add(e);
			} else if (e.expiredEvalutionTime > evaluationTime)
				future.add(e);
			else
				throw new RuntimeException("planned event in an earilier time");
		}
	}

}
