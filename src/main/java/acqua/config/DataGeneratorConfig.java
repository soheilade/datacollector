package acqua.config;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DataGeneratorConfig {
	public static final DataGeneratorConfig INSTANCE = new DataGeneratorConfig();
	private static final Logger logger = LoggerFactory.getLogger(DataGeneratorConfig.class); 
	
	private Configuration config;
	
	private DataGeneratorConfig(){
		try {
			config = new PropertiesConfiguration("acqua.properties");
		} catch (ConfigurationException e) {
			logger.error("Error while reading the configuration file", e);
		}
	}
	public String getBipartitePath(){
		return config.getString("bipartitegraph.path");
	}
	public Integer getUpdateBudget(){
		return config.getInt("query.updatebudget");
	}
	public boolean getSystemDebugMode(){
		return config.getBoolean("system.DEBUG");
	} 
	public Integer getStreamEndingTime(){
		return config.getInt("stream.end");
	}
	public Integer getFundVerticesCount(){
		return config.getInt("bipartiteFundCount");
	}
	public Integer getStockVerticesCount(){
		return config.getInt("bipartiteStockCount");
	}
	public Long getQueryStartingTime(){
		return config.getLong("query.start");
	}

	public Integer getQueryWindowSlide(){
		return config.getInt("query.window.slide");
	}
	public Integer getQueryWindowWidth(){
		return config.getInt("query.window.width");
	}
	public int getSlideNumber(){
		return (int) Math.ceil(config.getDouble("query.window.width")/config.getDouble("query.window.slide"));
	}
	
	
	public String getTwitterConsumerKey(){
		return config.getString("twitter.consumer.key");
	}
	
	public String getTwitterConsumerSecret(){
		return config.getString("twitter.consumer.secret");
	}

	public String getTwitterAccessTokenSecret(){
		return config.getString("twitter.accesstoken.secret");
	}
	
	public String getTwitterAccessToken(){
		return config.getString("twitter.accesstoken");
	}
	public String getStreamFilePath(){
		return config.getString("streamFile.path");
	}
	public String getProjectPath(){
		return "./";
		// use the current directory
		// we don't need to change anymore
		//return config.getString("filesystem.path");
	}
	
	public String getDatasetFolder(){
		//return "./";
		return config.getString("dataset.folder");
	}
	
	public String getDatasetDb(){
		return config.getString("dataset.db");
	}
	public Double getMatrixZipfSkewness(){
		return config.getDouble("dataGeneratorMatrix.zipf.Skewness");
	}
	public Double getJGraphZipfSkewness(){
		return config.getDouble("dataGeneratorJGraph.zipf.Skewness");
	}
}
