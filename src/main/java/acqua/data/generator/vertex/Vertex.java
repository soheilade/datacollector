package acqua.data.generator.vertex;

import java.util.Comparator;

import javax.management.RuntimeErrorException;

public class Vertex {
	protected String ID;
	protected boolean isU;
	protected double weight;
	protected int outDegree;

	public Vertex originalVertex;

	public static Vertex getOriginalVertex(Vertex input) {
		Vertex temp = input;
		while (temp.originalVertex != null)
			temp = temp.originalVertex;
		return temp;
	}
	public void setOriginalVertex(Vertex input) {
		//Vertex temp = input;
		//while (temp.originalVertex != null)
		//	temp = temp.originalVertex;
		this.originalVertex = input;
	}

	public boolean isU() {
		return isU;
	}

	public int getOutDegree() {
		return outDegree;
	}

	public void setOutDegree(int outDegree) {
		this.outDegree = outDegree;
	}
	public Vertex(int ID, boolean isU, double weight) {
		this.ID = String.valueOf(ID);
		this.isU = isU;
		this.weight = weight;
		this.originalVertex = null;
	}
	public Vertex(int ID, boolean isU, double weight, Vertex ori) {
		if(ori == null)
			throw new RuntimeException("no intermediate vertex should init with null ori");
		this.ID = String.valueOf(ID);
		this.isU = isU;
		this.weight = weight;
		this.originalVertex = ori;
	}

	@Override
	public String toString() {
		return this.ID;
	}

	public String toFullString() {
		if (isU)
			return this.ID + " " + this.outDegree;
		else
			return this.ID + " " + this.weight;
	}

	public String toFullStringWithOriID() {
		if (Vertex.getOriginalVertex(this.originalVertex) == null)
			throw new RuntimeException("no original ID");
		if (isU)
			return this.ID + " " + Vertex.getOriginalVertex(this.originalVertex).getIntID() + " " + this.outDegree;
		else
			return this.ID + " " + Vertex.getOriginalVertex(this.originalVertex).getIntID() + " " + this.weight;
	}

	public String getStrID() {
		return ID;
	}

	public int getIntID() {
		return Integer.parseInt(this.ID);
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	@Override
	public boolean equals(Object obj) {
		Vertex temp = (Vertex) obj;
		if (this.ID.equals(temp.ID) && this.isU == temp.isU)
			return true;
		else
			return false;
	}

	public static class Comparators {

		public static Comparator<Vertex> ID = new Comparator<Vertex>() {
			public int compare(Vertex o1, Vertex o2) {
				// if (Integer.parseInt(o1.ID) == Integer.parseInt(o2.ID))
				// throw new RuntimeException("two vertices have the same ID in ID comparator");
				return Integer.compare(Integer.parseInt(o1.ID), Integer.parseInt(o2.ID));
			}
		};
		public static Comparator<Vertex> WEIGHT = new Comparator<Vertex>() {
			public int compare(Vertex o1, Vertex o2) {
				if (Double.compare(o1.weight, o2.weight) != 0)
					return Double.compare(o1.weight, o2.weight);
				else
					return Integer.compare(Integer.parseInt(o1.ID), Integer.parseInt(o2.ID));
			}
		};
		public static Comparator<Vertex> OUTDEGREE = new Comparator<Vertex>() {
			public int compare(Vertex o1, Vertex o2) {
				if (Integer.compare(o1.outDegree, o2.outDegree) != 0)
					return Integer.compare(o1.outDegree, o2.outDegree);
				else
					return Integer.compare(Integer.parseInt(o1.ID), Integer.parseInt(o2.ID));
			}
		};

	}

}
