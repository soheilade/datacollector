package acqua.data.generator.intergrated;

import java.util.Map;

import acqua.data.generator.BKG.BKGGenerator;
import acqua.data.generator.stream.StreamGeneratorMulti;

public abstract class IntergratedGenerator {
	public int maxIDStream;
	public int maxIDBKG;
	public StreamGeneratorMulti streamGenMulti;
	public BKGGenerator bkgGen;
	public Map<String, Double> intervalAssignment;
	public Map<String, Double> lambdaAssignment;

	// To implement
	// public GraphGenerator graphGen;

	public abstract void generateIntervalAssignment();

	public abstract void generateLambdaAssignment();

	public IntergratedGenerator(int maxID) {
		this.maxIDStream = maxID;
		this.maxIDBKG = maxID;
	}
	public void printIntervalAssignment() {
		for (int i = 0; i < this.maxIDBKG; i++) {
			System.out.println(i + "\t" + intervalAssignment.get(String.valueOf(i)));
		}
	}
	public void printLambdaAssignment() {
		for (int i = 0; i < this.maxIDStream; i++) {
			System.out.println(i + "\t" + lambdaAssignment.get(String.valueOf(i)));
		}
	}


	// public static void main(String[] agrs) {
	// IntergratedGenerator myInterGen = new IntergratedGenerator(10);
	// while (myInterGen.streamGenMulti.hasNext()) {
	// String line = myInterGen.streamGenMulti.next().toString();
	// System.out.println(line);
	// }
	// // Config.INSTANCE.g;
	// // stream related parameter
	// // commom paramter: initialTimestamp, int maxId, long finalTimestamp
	// // stream arrival rate: homogenours VS. inhomogenous
	// // data appearance distribution:
	// // individual data arrival rate:
	//
	// }

}
