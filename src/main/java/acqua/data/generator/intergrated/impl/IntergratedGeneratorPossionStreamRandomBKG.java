package acqua.data.generator.intergrated.impl;

import java.util.Random;
import java.util.TreeMap;

import acqua.data.generator.BKG.BKGGenerator;
import acqua.data.generator.intergrated.IntergratedGenerator;
import acqua.data.generator.stream.impl.StreamGeneratorMultiBasic;
import acqua.data.generator.stream.impl.StreamGeneratorMultiNonHomoPoisson;

public class IntergratedGeneratorPossionStreamRandomBKG extends IntergratedGenerator {
	protected double maxLambda = 10.0;
	protected double maxInterval = 100.0;
	private Random rand;

	public IntergratedGeneratorPossionStreamRandomBKG(int maxID) {
		super(maxID);
		rand = new Random();
		generateIntervalAssignment();
		printIntervalAssignment();
		generateLambdaAssignment();
		printLambdaAssignment();

		streamGenMulti = new StreamGeneratorMultiNonHomoPoisson(maxID, this.lambdaAssignment);
		bkgGen = new BKGGenerator(maxID, this.intervalAssignment);
	}

	@Override
	public void generateIntervalAssignment() {
		this.intervalAssignment = new TreeMap<String, Double>();
		for (int i = 0; i < this.maxIDBKG; i++) {
			intervalAssignment
					.put(String.valueOf(i), (double) rand.nextInt((int) this.maxInterval));
		}
	}

	@Override
	public void generateLambdaAssignment() {
		this.lambdaAssignment = new TreeMap<String, Double>();
		for (int i = 0; i < this.maxIDBKG; i++) {
			lambdaAssignment.put(String.valueOf(i), (double) rand.nextInt((int) this.maxLambda));
		}

	}

	public static void main(String[] agrs) {
		IntergratedGeneratorPossionStreamRandomBKG myInterGen = new IntergratedGeneratorPossionStreamRandomBKG(
				10);
		while (myInterGen.streamGenMulti.hasNext()) {
			String line = myInterGen.streamGenMulti.next().toString();
			System.out.println(line);
		}
		// Config.INSTANCE.g;
		// stream related parameter
		// commom paramter: initialTimestamp, int maxId, long finalTimestamp
		// stream arrival rate: homogenours VS. inhomogenous
		// data appearance distribution:
		// individual data arrival rate:

	}

}
