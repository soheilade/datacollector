package acqua.data.generator.JGraph;
import org.jgrapht.*;
import org.jgrapht.graph.*;

public class MyGraph {

	private WeightedGraph<String, DefaultWeightedEdge> g = new SimpleWeightedGraph<String, DefaultWeightedEdge>(DefaultWeightedEdge.class);
    private DefaultWeightedEdge e1;

    public void addVertex(String name) {
        g.addVertex(name);
        //graph.addVertex(name);
    }

    public void addEdge(String v1,String v2) {
        g.addEdge(v1, v2);
        // e1=graph.addEdge(v1, v2);
    }

    /*public void setEdgeWeight() {
        graph.setEdgeWeight(e1, DEFAULT_EDGE_WEIGHT);          
    }*/

    public WeightedGraph<String, DefaultWeightedEdge> getGraph() {
        return g;
    }

    /*public SimpleWeightedGraph<String,DefaultWeightedEdge> getGraph() {
        return graph;
    }*/
}