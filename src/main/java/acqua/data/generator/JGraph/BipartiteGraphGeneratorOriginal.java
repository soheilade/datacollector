package acqua.data.generator.JGraph;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import org.jgrapht.Graph;
import org.jgrapht.UndirectedGraph;
import org.jgrapht.VertexFactory;
import org.jgrapht.WeightedGraph;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.SimpleGraph;
import org.jgrapht.graph.SimpleWeightedGraph;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class BipartiteGraphGeneratorOriginal{
	//private static final Logger logger = LoggerFactory.getLogger(BipartiteGraphGenerator.class);
	public static int fundVertices = 10;
	public static int stockVertices = 25;
	public static int numEdges = 150;
	public static int streamLength = 300;
	public static int updateBudget = 2;
	public static WeightedGraph<String, DefaultWeightedEdge> graph;
	public static int[] stockChangeIntervalLenght = new int[stockVertices];
	public static int[] stockValues = new int[stockVertices];
	public static int[] stockLastUpdate = new int[stockVertices];
	public static int windowSize=6;
	public static int slideSize=3;
	public static Random rand;
	public static void main(String[] args)
	{
		
		graph=new SimpleWeightedGraph<String, DefaultWeightedEdge>(DefaultWeightedEdge.class);

		VertexFactory<String> vertexFactory = new VertexFactory<String>(){
			int n = 0;
			public String createVertex()
			{
				String s = String.valueOf(n);
				n++;
				return s;
			}
		};
		
		generateGraph(graph, fundVertices, stockVertices, numEdges, vertexFactory);
		printgraph(graph);
		
		/*ArrayList<ArrayList<Integer>> fundStream = new ArrayList<ArrayList<Integer>>();
		rand = new Random(System.currentTimeMillis());
		for(int i=0; i<streamLength; i++){
			int randomSize=rand.nextInt();
			ArrayList<Integer> e=new ArrayList<Integer>();
			for (int j=0;j<randomSize;j++){
				e.add(rand.nextInt(fundVertices));
			}
			fundStream.add(e);
		}*/
		int[] fundStream = new int[streamLength];
		rand = new Random(System.currentTimeMillis());
		/*try{
		BufferedWriter bw=new BufferedWriter(new FileWriter(new File("D:/softwareData/biparted-graph/stream.txt")));	
		for(int i=0; i<fundStream.length; i++){
			fundStream[i] = rand.nextInt(fundVertices);
			bw.write(fundStream[i]+"\n");
		}
		bw.flush();
		bw.close();
		}catch(Exception e){}*/
		
		try{
			BufferedReader br=new BufferedReader(new FileReader(new File("D:/softwareData/biparted-graph/stream.txt")));	
			String str="";
			int i=0;
			while((str=br.readLine())!=null){
				fundStream[i] = Integer.valueOf(str);
				i++;
			}
			br.close();
		}catch(Exception e){}
		
		/*try{
			BufferedWriter bw=new BufferedWriter(new FileWriter(new File("D:/softwareData/biparted-graph/changeRate.txt")));	
			//BufferedReader br=new BufferedReader(new FileReader(new File("D:/softwareData/biparted-graph/changeRate.txt")));
			for(int i=0; i<stockVertices; i++){
				int r=rand.nextInt(streamLength);
				stockChangeIntervalLenght[i] = r;//Integer.valueOf(br.readLine());
				bw.write(r+"\n");
				stockValues[i] = rand.nextInt();
				stockLastUpdate[i] = 0;
			}
			bw.flush();
			bw.close();
		}catch(Exception e){}*/
		
		try{
			BufferedReader br=new BufferedReader(new FileReader(new File("D:/softwareData/biparted-graph/changeRate.txt")));
			for(int i=0; i<stockVertices; i++){
				stockChangeIntervalLenght[i] = Integer.valueOf(br.readLine());
				stockValues[i] = rand.nextInt();
				stockLastUpdate[i] = 0;
			}
			br.close();
		}catch(Exception e){}
		
		//a sliding window with size 5 and slide size 1
		for(int now = windowSize; now < streamLength; now+=slideSize){//every window
			//logger.info("Current window: from {} to {}", now-5, now);
			int freshResponse=0;
			Set<Integer> stockElems = new HashSet<Integer>();
			HashMap<Integer,Integer> fundElems = new HashMap<Integer,Integer>();
			
			for(int c=0; c<windowSize; c++){//find join counterparts of the funds in the current window and put them in stockElems
				int fundElem = fundStream[now-c];
				fundElems.put(fundElem,windowSize-c);
				//ArrayList<Integer> fundElem = fundStream.get(now*6+c);
				for(DefaultWeightedEdge edge: graph.edgesOf(String.valueOf(fundElem))){
					int stockElem = Integer.parseInt(graph.getEdgeTarget(edge))-fundVertices;
					stockElems.add(stockElem);					
				}
			}
			
			//update stock elements
			
			//updateGreedyIndegreeInSubgraph(stockElems,fundElems,now);
			//updateGreedyIndegree(stockElems, now);
			//updateGreedyWeightedSubGraph(stockElems,fundElems,now);
			//updateGreedyWeighted(stockElems, fundElems, now);
			updateLRU(stockElems, now);
			//updateSumL(stockElems, fundElems, now);
			
			//create the response
			
			int sum=0;
			boolean freshness= true;
			for(int c=0; c<windowSize; c++){//for each fund find the response and its freshness
				int fundElem = fundStream[now-c];
				//ArrayList<Integer> fundElem = fundStream.get(now*6+c);
				Set<Integer> fundStock = new HashSet<Integer>();		
				//find the join counterparts foreach stock and put it in fundStock
				for(DefaultWeightedEdge edge: graph.edgesOf(String.valueOf(fundElem))){
					int stockElem = Integer.parseInt(graph.getEdgeTarget(edge))-fundVertices;
					fundStock.add(stockElem);					
				}
				for(int k=0;k<fundStock.size();k++)//for each join counterpart if it is not fresh the response freshness will be false
				{
					if(stockLastUpdate[k]+stockChangeIntervalLenght[k]<now)  freshness=false;
					sum += stockValues[k];
				}
				//response
				//logger.info("?fund: {}, Stocks: {}, ?stockSum: {}", new Object[]{fundElem, stockElems, sum});
				if(freshness) freshResponse++;
			}
				System.out.println( freshResponse*100/windowSize);
		}//every window

		System.out.println(graph);
	}
	public static void printgraph(WeightedGraph<String, DefaultWeightedEdge> graph){
		Set<DefaultWeightedEdge> edges = graph.edgeSet();

		for (DefaultWeightedEdge e : edges) {
			System.out.println( graph.getEdgeSource(e)+"->"+ graph.getEdgeTarget(e)+"="+graph.getEdgeWeight(e));			
		}
	}
	
	public static void updateSumL(Set<Integer> stockElems,HashMap<Integer,Integer> fundElems, int now){
		HashMap<Integer, Integer> stockIndegree=new HashMap<Integer, Integer>();
		MyGraph subGraph=new MyGraph();
		for(int s : stockElems){
			subGraph.addVertex(String.valueOf(s+fundVertices));
			for(DefaultWeightedEdge edge: graph.edgesOf(String.valueOf(s+fundVertices))){
				int fundNode = Integer.parseInt(graph.getEdgeSource(edge));
				if(fundElems.keySet().contains(fundNode)){
					subGraph.addVertex(String.valueOf(fundNode));
					subGraph.addEdge(String.valueOf(fundNode),String.valueOf(s+fundVertices));			
				}
			}
		}
		//System.out.println(subGraph.getGraph());
		for(int s : stockElems){
			Set<DefaultWeightedEdge> degree=subGraph.getGraph().edgesOf(String.valueOf(s+fundVertices));
			int sum=0;
			for(DefaultWeightedEdge e : degree){
				sum+= fundElems.get(Integer.parseInt(subGraph.getGraph().getEdgeSource(e)));
			}
			stockIndegree.put(s, sum);
		}
		List<Map.Entry<Integer, Integer>> list =
		        new LinkedList( stockIndegree.entrySet() );
		    Collections.sort( list, new Comparator<Map.Entry<Integer, Integer>>()
		    {
		        public int compare( Map.Entry<Integer, Integer> o1, Map.Entry<Integer, Integer> o2 )
		        {
		            return (o2.getValue()).compareTo( o1.getValue() );
		        }
		    } );
		for(int gamma=0;gamma<updateBudget&&list.size()>gamma;gamma++){
			int stockNumber=list.get(gamma).getKey().intValue();
			//System.out.println(list.size()+","+stockNumber);
			stockLastUpdate[stockNumber]=now;
			stockValues[stockNumber]=rand.nextInt();
		}
	}
	public static void updateGreedyIndegree(Set<Integer> stockElems, int now){
		HashMap<Integer, Integer> stockIndegree=new HashMap<Integer, Integer>();
		for(int s : stockElems){
			Set degree=graph.edgesOf(String.valueOf(s+fundVertices));
			stockIndegree.put(s, degree.size());
		}
		List<Map.Entry<Integer, Integer>> list =
		        new LinkedList( stockIndegree.entrySet() );
		    Collections.sort( list, new Comparator<Map.Entry<Integer, Integer>>()
		    {
		        public int compare( Map.Entry<Integer, Integer> o1, Map.Entry<Integer, Integer> o2 )
		        {
		            return (o2.getValue()).compareTo( o1.getValue() );
		        }
		    } );
		for(int gamma=0;gamma<updateBudget&&list.size()>gamma;gamma++){
			int stockNumber=list.get(gamma).getKey().intValue();
			//System.out.println(list.size()+","+stockNumber);
			stockLastUpdate[stockNumber]=now;
			stockValues[stockNumber]=rand.nextInt();
		}
	}
	public static void updateLRU(Set<Integer> stockElems, int now){
		HashMap<Integer, Integer> stockU=new HashMap<Integer, Integer>();
		for(int s : stockElems){
			stockU.put(s, stockLastUpdate[s]);
		}
		List<Map.Entry<Integer, Integer>> list =
		        new LinkedList( stockU.entrySet() );
		    Collections.sort( list, new Comparator<Map.Entry<Integer, Integer>>()
		    {
		        public int compare( Map.Entry<Integer, Integer> o1, Map.Entry<Integer, Integer> o2 )
		        {
		            return (o1.getValue()).compareTo( o2.getValue() );
		        }
		    } );
		for(int gamma=0;gamma<updateBudget&&list.size()>gamma;gamma++){
			int stockNumber=list.get(gamma).getKey().intValue();
			//System.out.println(list.size()+","+stockNumber);
			stockLastUpdate[stockNumber]=now;
			stockValues[stockNumber]=rand.nextInt();
		}
	}
	public static void updateGreedyWeighted(Set<Integer> stockElems,HashMap<Integer,Integer> fundElems, int now){
		
		//System.out.println(subGraph.getGraph());
		HashMap<Integer, Double> stockInWeight= new HashMap<Integer, Double>();
		for(int s : stockElems){
			Set<DefaultWeightedEdge> degree=graph.edgesOf(String.valueOf(s+fundVertices));
			double sum=0;
			for(DefaultWeightedEdge e : degree){
				sum+=graph.getEdgeWeight(e);
			}
			stockInWeight.put(s, sum);
		}
		
		List<Map.Entry<Integer, Double>> list =
		        new LinkedList( stockInWeight.entrySet() );
		    Collections.sort( list, new Comparator<Map.Entry<Integer, Double>>()
		    {
		        public int compare( Map.Entry<Integer, Double> o1, Map.Entry<Integer, Double> o2 )
		        {
		            return (o2.getValue()).compareTo( o1.getValue() );
		        }
		    } );
		for(int gamma=0;gamma<updateBudget&&list.size()>gamma;gamma++){
			int stockNumber=list.get(gamma).getKey().intValue();
			//System.out.println(list.size()+","+stockNumber);
			stockLastUpdate[stockNumber]=now;
			stockValues[stockNumber]=rand.nextInt();
		}
	}	
	public static void updateGreedyWeightedSubGraph(Set<Integer> stockElems,HashMap<Integer,Integer> fundElems, int now){
		HashMap<Integer, Integer> stockIndegreeWeightSum=new HashMap<Integer, Integer>();
		MyGraph subGraph = new MyGraph();
		for(int s : stockElems){
			subGraph.addVertex(String.valueOf(s+fundVertices));
			for(DefaultWeightedEdge edge: graph.edgesOf(String.valueOf(s+fundVertices))){
				int fundNode = Integer.parseInt(graph.getEdgeSource(edge));
				if(fundElems.keySet().contains(fundNode)){
					subGraph.addVertex(String.valueOf(fundNode));
					DefaultWeightedEdge e1 = (DefaultWeightedEdge)graph.getEdge(String.valueOf(fundNode),String.valueOf(s+fundVertices));		
					DefaultWeightedEdge e2 = subGraph.getGraph().addEdge(String.valueOf(fundNode),String.valueOf(s+fundVertices));
					subGraph.getGraph().setEdgeWeight(e2, graph.getEdgeWeight(e1));
				}
			}
		}
		//System.out.println(subGraph.getGraph());
		HashMap<Integer, Double> stockInWeight= new HashMap<Integer, Double>();
		for(int s : stockElems){
			Set<DefaultWeightedEdge> degree=subGraph.getGraph().edgesOf(String.valueOf(s+fundVertices));
			double sum=0;
			for(DefaultWeightedEdge e : degree){
				sum+=subGraph.getGraph().getEdgeWeight(e);
			}
			stockInWeight.put(s, sum);
		}
		
		List<Map.Entry<Integer, Double>> list =
		        new LinkedList( stockInWeight.entrySet() );
		    Collections.sort( list, new Comparator<Map.Entry<Integer, Double>>()
		    {
		        public int compare( Map.Entry<Integer, Double> o1, Map.Entry<Integer, Double> o2 )
		        {
		            return (o2.getValue()).compareTo( o1.getValue() );
		        }
		    } );
		for(int gamma=0;gamma<updateBudget&&list.size()>gamma;gamma++){
			int stockNumber=list.get(gamma).getKey().intValue();
			//System.out.println(list.size()+","+stockNumber);
			stockLastUpdate[stockNumber]=now;
			stockValues[stockNumber]=rand.nextInt();
		}
	}	
	public static void updateGreedyIndegreeInSubgraph(Set<Integer> stockElems,HashMap<Integer,Integer> fundElems, int now){
		HashMap<Integer, Integer> stockIndegree=new HashMap<Integer, Integer>();
		MyGraph subGraph=new MyGraph();
		for(int s : stockElems){
			subGraph.addVertex(String.valueOf(s+fundVertices));
			for(DefaultWeightedEdge edge: graph.edgesOf(String.valueOf(s+fundVertices))){
				int fundNode = Integer.parseInt(graph.getEdgeSource(edge));
				if(fundElems.keySet().contains(fundNode)){
					subGraph.addVertex(String.valueOf(fundNode));
					subGraph.addEdge(String.valueOf(fundNode),String.valueOf(s+fundVertices));			
				}
			}
		}
		//System.out.println(subGraph.getGraph());
		for(int s : stockElems){
			Set degree=subGraph.getGraph().edgesOf(String.valueOf(s+fundVertices));
			stockIndegree.put(s, degree.size());
		}
		List<Map.Entry<Integer, Integer>> list =
		        new LinkedList( stockIndegree.entrySet() );
		    Collections.sort( list, new Comparator<Map.Entry<Integer, Integer>>()
		    {
		        public int compare( Map.Entry<Integer, Integer> o1, Map.Entry<Integer, Integer> o2 )
		        {
		            return (o2.getValue()).compareTo( o1.getValue() );
		        }
		    } );
		for(int gamma=0;gamma<updateBudget&&list.size()>gamma;gamma++){
			int stockNumber=list.get(gamma).getKey().intValue();
			//System.out.println(list.size()+","+stockNumber);
			stockLastUpdate[stockNumber]=now;
			stockValues[stockNumber]=rand.nextInt();
		}
	}
	// Creates a bipartite graph with the given numbers 
	// of vertices and edges
	public static <V, E> void generateGraph(WeightedGraph<V, E> graph,
			int numVertices0, int numVertices1, int numEdges,
			final VertexFactory<V> vertexFactory)
	{
		List<V> vertices0 = new ArrayList<V>();
		for (int i = 0; i < numVertices0; i++)
		{
			V v = vertexFactory.createVertex();
			graph.addVertex(v);
			vertices0.add(v);
		}
		List<V> vertices1 = new ArrayList<V>();
		for (int i = 0; i < numVertices1; i++)
		{
			V v = vertexFactory.createVertex();
			graph.addVertex(v);
			vertices1.add(v);
		}

		// Create edges between random vertices
		Random random = new Random(0);
		while (graph.edgeSet().size() < numEdges)
		{
			int i1 = random.nextInt(vertices1.size());
			V v1 = vertices1.get(i1);
			int i0 = random.nextInt(vertices0.size());
			V v0 = vertices0.get(i0);
			E edge=graph.addEdge(v0, v1);
		}
		//set the weight
		for(V v : vertices0 ){
			Set<E> edges = graph.edgesOf(v);
			for(E e: edges){
				graph.setEdgeWeight(e, (double)1/(double)edges.size());
			}
		}
	}

	
	// Creates a bipartite graph with the given numbers
	// of vertices and edges without isolated vertices
	public static <V, E> void generateGraphNoIsolatedVertices(
			Graph<V, E> graph, int numVertices0, int numVertices1, int numEdges,
			final VertexFactory<V> vertexFactory, 
			List<V> vertices0, List<V> vertices1)
	{
		int minNumEdges = Math.max(numVertices0, numVertices0);
		if (numEdges < minNumEdges)
		{
			System.out.println("At least " + minNumEdges + " are required to " +
					"connect each of the " + numVertices0 + " vertices " +
					"to any of the " + numVertices1 + " vertices");
			numEdges = minNumEdges;
		}

		for (int i = 0; i < numVertices0; i++)
		{
			V v = vertexFactory.createVertex();
			graph.addVertex(v);
			vertices0.add(v);
		}
		for (int i = 0; i < numVertices1; i++)
		{
			V v = vertexFactory.createVertex();
			graph.addVertex(v);
			vertices1.add(v);
		}

		// Connect each vertex of the larger set with
		// a random vertex of the smaller set
		Random random = new Random(0);
		List<V> larger = null;
		List<V> smaller = null;


		if (numVertices0 > numVertices1)
		{
			larger = new ArrayList<V>(vertices0);
			smaller = new ArrayList<V>(vertices1);
		}
		else
		{
			larger = new ArrayList<V>(vertices1);
			smaller = new ArrayList<V>(vertices0);
		}
		List<V> unmatched = new ArrayList<V>(smaller);
		for (V vL : larger)
		{
			int i = random.nextInt(unmatched.size());
			V vS = unmatched.get(i);
			unmatched.remove(i);
			if (unmatched.size() == 0)
			{
				unmatched = new ArrayList<V>(smaller);
			}
			graph.addEdge(vL, vS);
		}

		// Create the remaining edges between random vertices
		while (graph.edgeSet().size() < numEdges)
		{
			int i0 = random.nextInt(vertices0.size());
			V v0 = vertices0.get(i0);
			int i1 = random.nextInt(vertices1.size());
			V v1 = vertices1.get(i1);
			graph.addEdge(v0, v1);
		}

	}
}