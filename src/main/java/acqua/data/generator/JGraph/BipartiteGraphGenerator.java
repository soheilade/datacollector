package acqua.data.generator.JGraph;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.ResourceBundle;
import java.util.Set;

import org.jgrapht.Graph;
import org.jgrapht.alg.NeighborIndex;
import org.jgrapht.event.GraphEdgeChangeEvent;
import org.jgrapht.UndirectedGraph;
import org.jgrapht.VertexFactory;
import org.jgrapht.WeightedGraph;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.SimpleGraph;
import org.jgrapht.graph.SimpleWeightedGraph;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import acqua.config.Config;
import acqua.data.generator.matrix.ZipfGenerator;

public class BipartiteGraphGenerator {
	public static int fundVertices = 100;
	public static int stockVertices = 100;
	public static int numEdges = 1000;
	//public static int streamLength = 100;
	public static int updateBudget = 1;
	public static WeightedGraph<String, DefaultWeightedEdge> graph;

	public static void main(String[] args) throws IOException {

		graph = new SimpleWeightedGraph<String, DefaultWeightedEdge>(DefaultWeightedEdge.class);
		VertexFactory<String> vertexFactory = new MyVertexFactory();
		// generateGraphByRemoval(graph, fundVertices, stockVertices, numEdges, vertexFactory, false, true);
		// printgraphToFile(graph);
		// varifyGraph("./data/graph/L100Rand_R100Skew_1000.txt");
		printOne2OnegraphToFile();

	}

	public static void varifyGraph(String fileDir) throws IOException {
		File inputFile = new File(fileDir);
		BufferedReader read = new BufferedReader(new FileReader(inputFile));
		String input;
		Map<Integer, Integer> storeUKey = new HashMap<Integer, Integer>();
		Map<Integer, Integer> storeVKey = new HashMap<Integer, Integer>();
		while ((input = read.readLine()) != null) {
			String[] split = input.split("\t");
			//
			if (!storeUKey.containsKey(Integer.parseInt(split[0]))) {
				storeUKey.put(Integer.parseInt(split[0]), 1);
			} else {
				int temp = storeUKey.get(Integer.parseInt(split[0]));
				storeUKey.put(Integer.parseInt(split[0]), ++temp);
			}

			if (!storeVKey.containsKey(Integer.parseInt(split[1]))) {
				storeVKey.put(Integer.parseInt(split[1]), 1);
			} else {
				int temp = storeVKey.get(Integer.parseInt(split[1]));
				storeVKey.put(Integer.parseInt(split[1]), ++temp);
			}
		}

		for (Entry<Integer, Integer> e : storeUKey.entrySet()) {
			System.out.println(e.getKey() + "\t" + e.getValue());
		}
		System.out.println();
		for (Entry<Integer, Integer> e : storeVKey.entrySet()) {
			System.out.println(e.getKey() + "\t" + e.getValue());
		}

	}

	// generate a large number of graphs
	// collect out-degree and see the statistic
	public static void testpGraph(int numOfGraph) {
		int i = 0;

		// ArrayList<SimpleWeightedGraph<String, DefaultWeightedEdge>> graphs =
		// new ArrayList<SimpleWeightedGraph<String, DefaultWeightedEdge>>();
		int totalNumVertices = fundVertices + stockVertices;
		int[] outDegree = new int[totalNumVertices];
		i = 0;

		while (i < numOfGraph) {
			SimpleWeightedGraph<String, DefaultWeightedEdge> MyGraph = new SimpleWeightedGraph<String, DefaultWeightedEdge>(DefaultWeightedEdge.class);
			VertexFactory<String> vertexFactory = new MyVertexFactory();
			generateGraphByRemoval(MyGraph, fundVertices, stockVertices, numEdges, vertexFactory, true, false);
			// printgraph(graph);

			NeighborIndex<String, DefaultWeightedEdge> adjacentMatrix = new NeighborIndex<String, DefaultWeightedEdge>(MyGraph);

			printgraph(MyGraph);

			int m = 0;
			while (m < MyGraph.vertexSet().size()) {
				Set<String> neighbors = adjacentMatrix.neighborsOf(String.valueOf(m));
				// System.out.println(m + " has outdegree " + neighbors.size());
				outDegree[m] += neighbors.size();
				m++;
			}
			// graphs.add((SimpleWeightedGraph<String, DefaultWeightedEdge>)
			// MyGraph);
			i++;
		}
		i = 0;
		for (Integer e : outDegree) {
			System.out.println(i + " has total outdegree " + e);
			i++;
		}

	}

	public static void printOne2OnegraphToFile() {
		File file = new File("./data/graph/One2OneL" + fundVertices + "_R" + stockVertices + ".txt");
		BufferedWriter output;
		try {
			output = new BufferedWriter(new FileWriter(file));
			// output.write("test");

			// Set<DefaultWeightedEdge> edges = graph.edgeSet();

			for (int i = 0; i < fundVertices; i++) {
				output.write(i + "\t" + (i + fundVertices) + "\n");
				System.out.println(i + "\t" + (i + fundVertices));
			}
			output.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void printgraphToFile(WeightedGraph<String, DefaultWeightedEdge> graph) {
		Set<DefaultWeightedEdge> edges = graph.edgeSet();

		File file = new File("./data/graph/L" + fundVertices + "Skew_R" + stockVertices + "Rand_" + numEdges + ".txt");
		BufferedWriter output;
		try {
			output = new BufferedWriter(new FileWriter(file));
			// output.write("test");

			// Set<DefaultWeightedEdge> edges = graph.edgeSet();

			for (DefaultWeightedEdge e : edges) {
				output.write(graph.getEdgeSource(e) + "\t" + graph.getEdgeTarget(e) + "\n");
				System.out.println(graph.getEdgeSource(e) + "\t" + graph.getEdgeTarget(e));
			}

			output.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void printgraph(WeightedGraph<String, DefaultWeightedEdge> graph) {
		Set<DefaultWeightedEdge> edges = graph.edgeSet();

		for (DefaultWeightedEdge e : edges) {
			System.out.println(graph.getEdgeSource(e) + "->" + graph.getEdgeTarget(e) + "=" + graph.getEdgeWeight(e));
		}

		NeighborIndex<String, DefaultWeightedEdge> adjacentMatrix = new NeighborIndex<String, DefaultWeightedEdge>(graph);

		int i = 0;
		while (i < graph.vertexSet().size()) {
			Set<String> neighbors = adjacentMatrix.neighborsOf(String.valueOf(i));
			System.out.println(i + " has outdegree " + neighbors.size());
			i++;
			if (i == fundVertices)
				System.out.println();
		}

	}

	// Creates a bipartite graph with the given numbers
	// of vertices and edges by removing edges from a full bipartite graph
	// both left and right can be skewed
	public static <V, E> void generateGraphByRemoval(WeightedGraph<V, E> graph, int numverticesLeft, int numverticesRight, int numEdges, final VertexFactory<V> vertexFactory, boolean isLeftRandom, boolean isRightRandom) {

		// distribution generator
		int numEdgesToRemove = numverticesLeft * numverticesRight - numEdges;
		Random random = new Random(System.currentTimeMillis());
		ZipfGenerator zipfGeneratorLeft = new ZipfGenerator(numverticesLeft, Config.INSTANCE.getJGraphZipfSkewness());
		ZipfGenerator zipfGeneratorRight = new ZipfGenerator(numverticesRight, Config.INSTANCE.getJGraphZipfSkewness());

		List<V> verticesLeft = new ArrayList<V>(numverticesLeft);
		for (int i = 0; i < numverticesLeft; i++) {
			V v = vertexFactory.createVertex();
			graph.addVertex(v);
			verticesLeft.add(v);
		}

		List<V> verticesRight = new ArrayList<V>(numverticesRight);
		for (int i = 0; i < numverticesRight; i++) {
			V v = vertexFactory.createVertex();
			graph.addVertex(v);
			verticesRight.add(v);
		}
		// complete bipartite graph
		for (V vLeft : verticesLeft) {
			for (V vRight : verticesRight) {
				graph.addEdge(vLeft, vRight);
			}
		}

		while (numEdgesToRemove > 0) {
			int iLeft;
			if (isLeftRandom)
				iLeft = random.nextInt(verticesLeft.size());
			else
				iLeft = zipfGeneratorLeft.next();
			V vLeft = verticesLeft.get(iLeft);

			int iRight;
			if (isRightRandom)
				iRight = random.nextInt(verticesRight.size());
			else
				iRight = zipfGeneratorRight.next();
			V vRight = verticesRight.get(iRight);

			NeighborIndex<V, E> adjacentMatrix = new NeighborIndex<V, E>(graph);
			E tempEdge = graph.getEdge(vLeft, vRight);
			// remove an edge if
			// it is not null
			// both vetices it touches has degree >1
			if (tempEdge != null) {
				if (adjacentMatrix.neighborsOf(vLeft).size() > 1 && adjacentMatrix.neighborsOf(vRight).size() > 1) {
					// adjacentMatrix.edgeAdded(e);.edgeRemoved(tempEdge);
					// adjacentMatrix.
					graph.removeEdge(tempEdge);
					numEdgesToRemove--;
					// System.out.println("removing " + iLeft + " " + iRight);
				}
				// else {
				// System.out.println("no enough degree " + iLeft + " " +
				// iRight);
				// }
			}
			// else {
			// System.out.println("no edge " + iLeft + " " + iRight);
			// }

		}
	}

}