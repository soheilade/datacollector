package acqua.data.generator.matrix;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.TreeSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import acqua.config.Config;
import acqua.data.generator.vertex.Vertex;
import acqua.query.join.event.UpdateEvent;

public class biadjacencyMatrixOutdegreeU extends biadjacencyMatrixWeightedV {
	private static final Logger logger = LoggerFactory.getLogger("biadjacencyMatrixOutdegreeU");

	public static void main(String[] args) {
		biadjacencyMatrix myMatrix = new biadjacencyMatrix("./src/main/resources/test.txt");
		// test for getOutDegreeU
		biadjacencyMatrixOutdegreeU testMatrix = new biadjacencyMatrixOutdegreeU(myMatrix);
		TreeSet<Vertex> tempOrder = testMatrix.getSortedUbyOutDedegree();
		//
		// for (Vertex v : tempOrder) {
		// System.out.println(v.toFullString());
		// }
		HashMap<Long, String> tempResult;
		// tempResult = testMatrix.removeByBudget(0);
		// for (Vertex v : tempResult) {
		// System.out.println(v.toFullString());
		// }
		testMatrix.computeWeight();
		for (int i = 0; i < testMatrix.U; i++) {
			System.out.println(testMatrix.getWeightForSingleV(i));
		}
		tempResult = testMatrix.removeWithInBudget(7);
		testMatrix.computeWeight();
		System.out.println();
		for (Entry<Integer, Vertex> v : testMatrix.VMap.entrySet()) {
			System.out.println(v.getKey() + " " + v.getValue().toFullString());
		}
		System.out.println();
		for (Entry<Integer, Vertex> v : testMatrix.UMap.entrySet()) {
			System.out.println(v.getKey() + " " + v.getValue().toFullString());
		}

		testMatrix.printWieghtedMatrix();
		System.out.println("final results");
		for (Entry<Long, String> v : tempResult.entrySet()) {
			System.out.println(v.getKey() + " " + v.getValue());
		}
	}

	// initiate weight
	// initiate outdegree
	public biadjacencyMatrixOutdegreeU(biadjacencyMatrix booleanMatrix) {
		super(booleanMatrix);
		this.copyOriVertex(booleanMatrix);
		this.updateOutDegree();
	}

	public TreeSet<Vertex> getSortedUbyOutDedegree() {
		TreeSet<Vertex> result = new TreeSet<Vertex>(Vertex.Comparators.OUTDEGREE);
		result.addAll(this.UMap.values());
		return result;
	}

	public HashMap<Long, String> removeWithInBudget(int budget) {
		HashMap<Long, String> result = new HashMap<Long, String>();
		TreeSet<Vertex> tempSortedU = this.getSortedUbyOutDedegree();

		int tempBudget = budget;
		Vertex candidate = tempSortedU.pollFirst();
		while (tempBudget > 0 && (candidate != null)) {
			int cost = candidate.getOutDegree();
			if (cost > tempBudget) {
				// there is not enough budget anymore, pick by V
				this.computeWeight();

				TreeSet<Vertex> pickFromV = this.getSortedVbyWeight();
				Iterator<Vertex> tempV = pickFromV.descendingIterator();
				List<Vertex> tempRemovedVList = new ArrayList<Vertex>();
				for (int i = 0; i < tempBudget && tempV.hasNext(); i++) {
					Vertex temp = tempV.next();
					// System.out.println(temp.toFullString());
					result.put((long) temp.getIntID(), temp.toFullStringWithOriID());
					tempRemovedVList.add(temp);
				}
				this.removeVs(tempRemovedVList);
				tempBudget = 0;
				// end of the while loop
			} else {
				// there is enough budget for this candidate
				// remove it and update graph
				if (Config.INSTANCE.getSystemDebugMode()) {
					System.out.println("engouh budget boolean matrix before remove");
					this.printBooleanMatrix();
				}

				List<Vertex> tempNeighbours = this.getNeighboursForU(candidate.getIntID());
				for (Vertex tempNeighbour : tempNeighbours) {
					result.put((long) tempNeighbour.getIntID(), tempNeighbour.toFullStringWithOriID());
				}
				// not only remove U, but also remove V
				// update outdegree
				this.removeU(candidate);
				this.removeVs(tempNeighbours);
				this.updateOutDegree();

				tempBudget = tempBudget - cost;
				candidate = tempSortedU.pollFirst();

				if (Config.INSTANCE.getSystemDebugMode()) {
					System.out.println("engouh budget boolean matrix after remove");
					this.printBooleanMatrix();
				}

			}
		}
		return result;
	}

	List<Vertex> getTopUs(TreeSet<Vertex> tempSortedU) {
		List<Vertex> resultsList = new ArrayList<Vertex>();
		if (tempSortedU.size() > 0) {
			Iterator<Vertex> it = tempSortedU.iterator();
			Vertex tempSmallest = tempSortedU.first();

			int smallestOutdegree = tempSmallest.getOutDegree();
			while (it.hasNext()) {
				Vertex tempU = it.next();
				if (tempU.getOutDegree() == smallestOutdegree) {
					resultsList.add(tempU);
				} else if (tempU.getOutDegree() > smallestOutdegree) {
					break;
				}
			}
		}
		return resultsList;
	}

	Vertex selectTopUByScore(List<Vertex> input, HashMap<Integer, UpdateEvent> scoreForV) {
		int smallestScore = Integer.MIN_VALUE;
		Vertex UwithsmallestScore = null;
		for (Vertex e : input) {
			List<Vertex> tempNeighbours = this.getNeighboursForU(e.getIntID());
			int smallestScoreForU = Integer.MAX_VALUE;
			for (Vertex v : tempNeighbours) {
				int tempScore = scoreForV.get(Vertex.getOriginalVertex(v).getIntID()).scoreForUpdate;
				if (tempScore < smallestScoreForU) {
					smallestScoreForU = tempScore;
				}
			}
			if (smallestScoreForU > smallestScore) {
				smallestScore = smallestScoreForU;
				UwithsmallestScore = e;
			}
		}
		return UwithsmallestScore;
	}

	public HashMap<Long, String> removeWithInBudget(int budget, HashMap<Integer, UpdateEvent> scoreForV) {
		HashMap<Long, String> result = new HashMap<Long, String>();
		TreeSet<Vertex> tempSortedU = this.getSortedUbyOutDedegree();

		int tempBudget = budget;
		List<Vertex> tempTopUs = this.getTopUs(tempSortedU);
		// if (tempTopUs.size() > 1)
		// logger.debug("we need to choose");
		Vertex tempCandidate = null;
		if (tempSortedU.size() > 0)
			tempCandidate = tempSortedU.first();
		Vertex candidate = this.selectTopUByScore(tempTopUs, scoreForV);
		// logger.debug(tempSortedU.first().toFullString());
		tempSortedU.remove(candidate);
		// logger.debug(candidate.toFullString());
		if ((tempCandidate != null) && (tempCandidate.getIntID() != candidate.getIntID())) {
			logger.debug("one improve");
		}
		while (tempBudget > 0 && (candidate != null)) {
			int cost = candidate.getOutDegree();
			if (cost > tempBudget) {
				// there is not enough budget anymore, pick by V
				this.computeWeight();

				TreeSet<Vertex> pickFromV = this.getSortedVbyWeight();
				Iterator<Vertex> tempV = pickFromV.descendingIterator();
				List<Vertex> tempRemovedVList = new ArrayList<Vertex>();
				for (int i = 0; i < tempBudget && tempV.hasNext(); i++) {
					Vertex temp = tempV.next();
					// System.out.println(temp.toFullString());
					result.put((long) temp.getIntID(), temp.toFullStringWithOriID());
					tempRemovedVList.add(temp);
				}
				this.removeVs(tempRemovedVList);
				tempBudget = 0;
				// end of the while loop
			} else {
				// there is enough budget for this candidate
				// remove it and update graph
				if (Config.INSTANCE.getSystemDebugMode()) {
					System.out.println("engouh budget boolean matrix before remove");
					this.printBooleanMatrix();
				}

				List<Vertex> tempNeighbours = this.getNeighboursForU(candidate.getIntID());
				for (Vertex tempNeighbour : tempNeighbours) {
					result.put((long) tempNeighbour.getIntID(), tempNeighbour.toFullStringWithOriID());
				}
				// not only remove U, but also remove V
				// update outdegree
				this.removeU(candidate);
				this.removeVs(tempNeighbours);
				this.updateOutDegree();

				tempBudget = tempBudget - cost;
				candidate = tempSortedU.pollFirst();

				if (Config.INSTANCE.getSystemDebugMode()) {
					System.out.println("engouh budget boolean matrix after remove");
					this.printBooleanMatrix();
				}

			}
		}
		return result;
	}

	void removeU(Vertex inputU) {
		if (!inputU.isU())
			throw new RuntimeException("this is not a U");
		int UID = inputU.getIntID();
		List<Vertex> neighbours = this.getNeighboursForU(UID);
		for (Vertex tempV : neighbours) {
			removeEdgeFromMatrix(UID, tempV.getIntID());
		}
		this.UMap.remove(UID);
		// this.updateOutDegree();
	}

	void removeVs(List<Vertex> inputVList) {
		for (Vertex v : inputVList) {
			removeV(v);
		}
	}

	void removeV(Vertex inputV) {
		if (inputV.isU())
			throw new RuntimeException("this is not a V");
		int VID = inputV.getIntID();
		List<Vertex> neighbours = this.getNeighboursForV(VID);
		for (Vertex tempU : neighbours) {
			removeEdgeFromMatrix(tempU.getIntID(), VID);
		}
		this.VMap.remove(VID);
	}

	void removeEdgeFromMatrix(int U, int V) {
		this.adjUV[U][V] = false;
		// TODO: update Edge!!
	}

	void updateOutDegree() {
		for (Vertex tempU : this.UMap.values()) {
			tempU.setOutDegree(this.getNeighboursForU(tempU.getIntID()).size());
		}
	}
}
