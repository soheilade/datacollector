package acqua.data.generator.matrix;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeSet;

import acqua.data.generator.vertex.Vertex;

public class biadjacencyMatrixWeightedV extends biadjacencyMatrix {
	protected double[][] adjUVWeighted;

	public static void main(String[] args) {
		biadjacencyMatrix myMatrix = new biadjacencyMatrix("./src/main/resources/test.txt");
		// test for getWeight
		biadjacencyMatrixWeightedV testMatrix = new biadjacencyMatrixWeightedV(myMatrix);
		// testMatrix.printWieghtedMatrix();

		Map<Long, String> tempResult = testMatrix.getVsWithInBudget(3);
		for (Entry<Long, String> e : tempResult.entrySet()) {
			System.out.println(e.getKey() + " " + e.getValue());
		}
		// System.out.println("");
		for (Vertex e : testMatrix.getWeightForVs()) {
			System.out.println(e.toFullString());
		}

	}

	// distribute the weight of U to V
	public biadjacencyMatrixWeightedV(biadjacencyMatrix booleanMatrix) {
		super(booleanMatrix.U(), booleanMatrix.V());
		adjUVWeighted = new double[this.U][this.V];
		for (int tempU = 0; tempU < this.U; tempU++) {
			List<Vertex> tempNeighbours = booleanMatrix.getNeighboursForU(tempU);
			this.updateWeightForAU(tempU, tempNeighbours);
		}
		this.copyOriVertex(booleanMatrix);
		this.getWeightForVs();
	}

	public void computeWeight() {
		adjUVWeighted = new double[this.U][this.V];

		for (Entry<Integer, Vertex> tempU : this.UMap.entrySet()) {
			List<Vertex> tempNeighbours = this.getNeighboursForU(tempU.getKey());
			this.updateWeightForAU(tempU.getKey(), tempNeighbours);
		}
		this.getWeightForVs();
	}

	protected void updateWeightForAU(int tempU, List<Vertex> tempNeighbours) {
		double numOfNeighbours = tempNeighbours.size();
		double tempWeight = 1 / numOfNeighbours;
		for (Vertex tempNeighbour : tempNeighbours) {
			adjUVWeighted[tempU][tempNeighbour.getIntID()] += tempWeight;
			this.adjUV[tempU][tempNeighbour.getIntID()] = true;
		}
	}

	public void copyOriVertex(biadjacencyMatrix booleanMatrix) {
		for (Entry<Integer, Vertex> e : booleanMatrix.UMap.entrySet()) {
			this.UMap.get(e.getKey()).setOriginalVertex(Vertex.getOriginalVertex(e.getValue()));
		}
		for (Entry<Integer, Vertex> e : booleanMatrix.VMap.entrySet()) {
			this.VMap.get(e.getKey()).setOriginalVertex(Vertex.getOriginalVertex(e.getValue()));
		}
	}

	public double getWeightForSingleV(int inputV) {
		double result = 0;
		List<Vertex> tempNeighbours = this.getNeighboursForV(inputV);
		for (Vertex tempNeighbour : tempNeighbours) {
			result += this.adjUVWeighted[tempNeighbour.getIntID()][inputV];
		}
		this.VMap.get(inputV).setWeight(result);
		// System.out.println(this.VMap.get(inputV).toFullString());
		return result;
	}

	public TreeSet<Vertex> getWeightForVs() {
		TreeSet<Vertex> result = new TreeSet<Vertex>(Vertex.Comparators.WEIGHT);
		for (Integer tempV : this.VMap.keySet()) {
			getWeightForSingleV(tempV);
		}
		result.addAll(this.VMap.values());
		return result;
	}

	public TreeSet<Vertex> getOrderedVsByWeight() {
		TreeSet<Vertex> tempOrderedWeight = this.getWeightForVs();
		return tempOrderedWeight;
	}
	
	public HashMap<Long, String> getVsWithInBudget(int budget) {
		HashMap<Long, String> result = new HashMap<Long, String>();
		TreeSet<Vertex> tempOrderedWeight = this.getWeightForVs();
		Iterator<Vertex> it = tempOrderedWeight.descendingIterator();
		int tempBudget = budget;
		while (tempBudget > 0 && it.hasNext()) {
			Vertex tempV = it.next();
			result.put((long) tempV.getIntID(), tempV.toFullStringWithOriID());
			tempBudget--;
		}
		return result;
	}

	public TreeSet<Vertex> getSortedVbyWeight() {
		TreeSet<Vertex> result = new TreeSet<Vertex>(Vertex.Comparators.WEIGHT);
		result.addAll(this.VMap.values());
		return result;
	}

	public void printWieghtedMatrix() {
		for (int i = 0; i < this.U; i++) {
			for (int j = 0; j < this.V; j++) {
				System.out.print(String.format("%.2f", this.adjUVWeighted[i][j]));
				System.out.print(" ");
			}
			System.out.print("\n");
		}
	}

}
