package acqua.data.generator.matrix;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.TreeSet;

import acqua.data.generator.vertex.*;

public class biadjacencyMatrix {
	protected int U; // stream part
	protected int V; // static part
	protected int E; // number of edge, not sure if useful
	protected boolean[][] adjUV;
	protected AdjIteratorV itFromV;
	protected AdjIteratorU itFromU;

	public HashMap<Integer, Vertex> UMap = new HashMap<Integer, Vertex>();
	public HashMap<Integer, Vertex> VMap = new HashMap<Integer, Vertex>();

	public static void main(String[] args) {
		biadjacencyMatrix myMatrix = new biadjacencyMatrix("./src/main/resources/test.txt");

		// test for getSubGraphForU
		// List<Vertex> temp = new ArrayList<Vertex>();
		// temp.add(new Vertex(2, true, 0));
		// temp.add(new Vertex(3, true, 0));
		// biadjacencyMatrix newMatrix = myMatrix.getSubGraphForU(temp);
		// myMatrix.printBooleanMatrix();
		// System.out.println();
		// newMatrix.printBooleanMatrix();
		// for (Vertex e : newMatrix.VMap.values()) {
		// System.out.print(e.getIntID() + " " + e.originalVertex.getIntID());
		// System.out.println();
		// }

		// test for getSubGraphForV
		List<Vertex> temp = new ArrayList<Vertex>();
		temp.add(new Vertex(2, true, 0));
		temp.add(new Vertex(3, true, 0));
		biadjacencyMatrix newMatrix = myMatrix.getSubGraphForV(temp);
		myMatrix.printBooleanMatrix();
		System.out.println();
		newMatrix.printBooleanMatrix();
		System.out.println();

		for (Vertex e : newMatrix.UMap.values()) {
			System.out.print(e.getIntID() + " " + e.originalVertex.getIntID());
			System.out.println();
		}

		// test for getNeighbours
		// for (Vertex e : myMatrix.getNeighboursForU(0))
		// System.out.print(e.toString());
		// System.out.println();
		//
		// for (Vertex e : myMatrix.getNeighboursForU(1))
		// System.out.print(e.toString());
		// System.out.println();
		//
		// for (Vertex e : myMatrix.getNeighboursForV(0))
		// System.out.print(e.toString());
		// System.out.println();
		//
		// for (Vertex e : myMatrix.getNeighboursForV(1))
		// System.out.print(e.toString());
		// System.out.println();

	}

	List<Vertex> getVertexListForArray(List<Integer> input, boolean isU) {
		List<Vertex> result = new ArrayList<Vertex>();
		Iterator<Integer> it = input.iterator();
		Map<Integer, Vertex> temp;
		if (isU) {
			temp = this.UMap;
		} else {
			temp = this.VMap;
		}
		while (it.hasNext()) {
			result.add(temp.get(it.next()));
		}
		return result;
	}

	/**
	 * get sub graph by a given list of U
	 */
	public biadjacencyMatrix getSubGraphForU(List<Vertex> u) {
		Set<Vertex> myVset = new TreeSet<Vertex>(Vertex.Comparators.ID);
		// get all neighbours
		for (Vertex i : u) {
			List<Vertex> neighboursForI = getNeighboursForU(i.getIntID());
			for (Vertex neighbour : neighboursForI)
				myVset.add(neighbour);
		}
		biadjacencyMatrix result = new biadjacencyMatrix(u.size(), myVset.size());

		Map<Integer, Integer> tempUOldNew = new HashMap<Integer, Integer>();
		int newIndexU = 0;
		for (Vertex vertexOriU : u) {
			result.UMap.put(newIndexU, new Vertex(newIndexU, true, 0, Vertex.getOriginalVertex(vertexOriU)));
			tempUOldNew.put(vertexOriU.getIntID(), newIndexU);
			newIndexU++;
		}

		Map<Integer, Integer> tempVOldNew = new HashMap<Integer, Integer>();
		int newIndexV = 0;
		for (Vertex vertexOriV : myVset) {
			result.VMap.put(newIndexV, new Vertex(newIndexV, false, 0, Vertex.getOriginalVertex(vertexOriV)));
			tempVOldNew.put(vertexOriV.getIntID(), newIndexV);
			newIndexV++;
		}

		for (Vertex i : u) {
			List<Vertex> neighboursForI = getNeighboursForU(i.getIntID());
			int tempNewIdexU = tempUOldNew.get(i.getIntID());
			for (Vertex neighbours : neighboursForI) {
				int tempNewIdexV = tempVOldNew.get(neighbours.getIntID());
				result.addEdgeUV(tempNewIdexU, tempNewIdexV);
			}
		}

		return result;
	}

	/**
	 * get sub graph by a given list of V
	 */
	public biadjacencyMatrix getSubGraphForV(List<Vertex> v) {
		Set<Vertex> myUset = new TreeSet<Vertex>(Vertex.Comparators.ID);
		// get all neighbours
		for (Vertex i : v) {
			List<Vertex> neighboursForI = getNeighboursForV(i.getIntID());
			for (Vertex neighbour : neighboursForI)
				myUset.add(neighbour);
		}
		biadjacencyMatrix result = new biadjacencyMatrix(myUset.size(), v.size());

		Map<Integer, Integer> tempUOldNew = new HashMap<Integer, Integer>();
		int newIndexU = 0;
		for (Vertex vertexOriU : myUset) {
			result.UMap.put(newIndexU, new Vertex(newIndexU, true, 0, Vertex.getOriginalVertex(vertexOriU)));
			tempUOldNew.put(vertexOriU.getIntID(), newIndexU);
			newIndexU++;
		}

		Map<Integer, Integer> tempVOldNew = new HashMap<Integer, Integer>();
		int newIndexV = 0;
		for (Vertex vertexOriV : v) {
			result.VMap.put(newIndexV, new Vertex(newIndexV, false, 0, Vertex.getOriginalVertex(vertexOriV)));
			tempVOldNew.put(vertexOriV.getIntID(), newIndexV);
			newIndexV++;
		}
		// need to start with V
		// start U would include Vs that are not in the input
		for (Vertex i : v) {
			List<Vertex> neighboursForI = getNeighboursForV(i.getIntID());
			int tempNewIdexV = tempVOldNew.get(i.getIntID());
			for (Vertex neighbours : neighboursForI) {
				int tempNewIdexU = tempUOldNew.get(neighbours.getIntID());
				result.addEdgeUV(tempNewIdexU, tempNewIdexV);
			}
		}

		return result;
	}

	public List<Vertex> getNeighboursForU(int u) {
		itFromU = new AdjIteratorU(u);
		List<Vertex> results = new ArrayList<Vertex>();
		while (itFromU.hasNext()) {
			results.add(this.VMap.get(itFromU.next()));
		}
		return results;
	}

	public List<Vertex> getNeighboursForV(int v) {
		itFromV = new AdjIteratorV(v);
		List<Vertex> results = new ArrayList<Vertex>();
		while (itFromV.hasNext()) {
			results.add(this.UMap.get(itFromV.next()));
		}
		return results;
	}

	// read graph from a file
	public biadjacencyMatrix(String inputFileName) {
		itFromU = new AdjIteratorU(0);
		itFromV = new AdjIteratorV(0);

		File file = new File(inputFileName);
		BufferedReader intput;
		String thisLine = null;

		List<String> inputGraph = new ArrayList<String>();
		try {
			intput = new BufferedReader(new FileReader(file));
			while ((thisLine = intput.readLine()) != null) {
				inputGraph.add(thisLine);
			}
			intput.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		Set<String> myU = new HashSet<String>();
		Set<String> myV = new HashSet<String>();
		for (String s : inputGraph) {
			String[] parts = s.split("\t");
			myU.add(parts[0]);
			myV.add(parts[1]);
		}
		this.U = myU.size();
		this.V = myV.size();

		this.E = inputGraph.size();
		this.adjUV = new boolean[U][V]; // VU for easy of generation trace
		this.initVertex(U, V);

		for (String s : inputGraph) {
			String[] parts = s.split("\t");
			// Integer.parseInt(parts[1]) - this.U
			// the V in the file is indexed from U
			addEdgeUV(Integer.parseInt(parts[0]), Integer.parseInt(parts[1]) - this.U);
		}

	}

	public void printBooleanMatrix() {
		for (int i = 0; i < this.U; i++) {
			for (int j = 0; j < this.V; j++) {
				if (this.adjUV[i][j])
					System.out.print("1");
				else
					System.out.print("0");
				System.out.print(" ");
			}
			System.out.print("\n");
		}
	}

	public void initVertex(int U, int V) {
		for (int i = 0; i < U; i++)
			this.UMap.put(i, new Vertex(i, true, 0));
		for (int i = 0; i < V; i++)
			this.VMap.put(i, new Vertex(i, false, 0));
	}

	// empty graph with U and V
	public biadjacencyMatrix(int U, int V) {
		if (V < 0 || U < 0)
			throw new RuntimeException("U and V must be nonnegative");
		this.U = U;
		this.V = V;
		this.E = 0;
		this.adjUV = new boolean[U][V]; // VU for easy of generation trace
		this.initVertex(U, V);
	}

	// random graph with V vertices and E edges
	public biadjacencyMatrix(int U, int V, int selectivityV, double selectivityDistTypeV, double selectivityDistTypeU) {
		this(U, V);
		if (selectivityDistTypeV > 1)
			throw new RuntimeException("selectivityDistTypeV must < 1");
		if (selectivityDistTypeU > 1)
			throw new RuntimeException("selectivityDistTypeU must < 1");
		// put U into a list for sampling
		List<Integer> tempU = new ArrayList<Integer>(U);
		int temp = 0;
		while (temp < U) {
			tempU.add(temp);
			temp++;
		}

		if (selectivityDistTypeV < 0) { // uniform
			if (selectivityDistTypeU < 0) { // uniform
				int tempV = 0;
				while (tempV < V) {
					Set<Integer> edges = FloydRandomSampling.randomSample(tempU, selectivityV);
					Iterator<Integer> itr = edges.iterator();
					while (itr.hasNext()) {
						addEdgeUV(itr.next(), tempV);
					}
					tempV++;
				}
			} else if (selectivityDistTypeU <= 1) { // skewed

			}

		} else if (selectivityDistTypeV <= 1) {
			if (selectivityDistTypeU < 0) {

			} else if (selectivityDistTypeU <= 1) {

			}
		}
	}

	// number of vertices and edges
	public int U() {
		return U;
	}

	public int V() {
		return V;
	}

	public int E() {
		return E;
	}

	// add undirected edge
	public void addEdgeUV(int u, int v) {
		if (!adjUV[u][v])
			E++;
		adjUV[u][v] = true;
	}

	// does the graph contain the edge u v?
	public boolean containsUV(int u, int v) {
		return adjUV[u][v];
	}

	// return list of neighbors of v
	public Iterable<Integer> adjV(int v) {
		return new AdjIteratorV(v);
	}

	public Iterable<Integer> adjU(int u) {
		return new AdjIteratorU(u);
	}

	// support iteration over graph vertices
	public class AdjIteratorV implements Iterator<Integer>, Iterable<Integer> {
		int v;
		int u = 0;

		AdjIteratorV(int v) {
			this.v = v;
		}

		public Iterator<Integer> iterator() {
			return this;
		}

		public boolean hasNext() {
			while (u < U) {
				if (adjUV[u][v])
					return true;
				u++;
			}
			return false;
		}

		public Integer next() {
			if (hasNext()) {
				return u++;
			} else {
				throw new NoSuchElementException();
			}
		}

		public void remove() {
			throw new UnsupportedOperationException();
		}
	}

	public class AdjIteratorU implements Iterator<Integer>, Iterable<Integer> {
		int v = 0;
		int u;

		AdjIteratorU(int u) {
			this.u = u;
		}

		void setU(int u) {
			this.u = u;
		}

		public Iterator<Integer> iterator() {
			return this;
		}

		public boolean hasNext() {
			while (v < V) {
				if (adjUV[u][v])
					return true;
				v++;
			}
			return false;
		}

		public Integer next() {
			if (hasNext()) {
				return v++;
			} else {
				throw new NoSuchElementException();
			}
		}

		public void remove() {
			throw new UnsupportedOperationException();
		}
	}
}
