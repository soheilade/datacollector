package acqua.data.generator.matrix;

import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Random;

import org.jgrapht.graph.DefaultWeightedEdge;

import acqua.config.Config;

public class ZipfGenerator {
	private Random rnd = new Random(System.currentTimeMillis());
	private int size;
	private double skew;
	private double bottom = 0;

	public ZipfGenerator(int size, double skew) {
		this.size = size;
		this.skew = skew;

		for (int i = 1; i < size; i++) {
			this.bottom += (1 / Math.pow(i, this.skew));
		}
	}

	// the next() method returns an random rank id.
	// The frequency of returned rank ids are follows Zipf distribution.
	public int next() {
		int rank;
		double friquency = 0;
		double dice;
		rnd.setSeed(System.currentTimeMillis());
		rank = rnd.nextInt(size);
		friquency = (1.0d / Math.pow(rank, this.skew)) / this.bottom;
		dice = rnd.nextDouble();

		while (!(dice < friquency)) {
			rank = rnd.nextInt(size);
			friquency = (1.0d / Math.pow(rank, this.skew)) / this.bottom;
			dice = rnd.nextDouble();
		}

		return rank;
	}

	// This method returns a probability that the given rank occurs.
	public double getProbability(int rank) {
		return (1.0d / Math.pow(rank, this.skew)) / this.bottom;
	}

	public HashMap<Integer, Double> getZifProbability(int m, double skewness) {
		HashMap<Integer, Double> results = new HashMap<Integer, Double>();
		ZipfGenerator zipf = new ZipfGenerator(m, skewness);
		for (int i = 0; i < m; i++) {
			results.put(i, zipf.getProbability(i + 1));
		}
		return results;
	}

	// public HashMap<Integer, Double> getZipfFrequency(int m, double skewness) {
	// HashMap<Integer, Double> results = new HashMap<Integer, Double>();
	// ZipfGenerator zipf = new ZipfGenerator(m, skewness);
	// for (int i = 0; i < m; i++) {
	// results.put(i, zipf.getProbability(i+1));
	// }
	// HashMap<Integer, Double> results =
	// return results;
	// }

	public static void main(String[] args) throws InterruptedException {
		// if (args.length != 2) {
		// System.out.println("usage: ./zipf size skew");
		// System.exit(-1);
		// }
		// Config.INSTANCE.getMatrixZipfSkewness();
		ZipfGenerator zipf = new ZipfGenerator(200, 0.2);

		File file = new File("./data/changeRate/Zipf(0.2).txt");
		BufferedWriter output;
		try {
			output = new BufferedWriter(new FileWriter(file));
			for (int i = 0; i < 100; i++) {
				// System.out.println(i + " " + (205 - zipf.next()));
				output.write(i + "\t" + (205 - zipf.next()) + "\n");
				Thread.sleep(1);
				output.flush();
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}