package acqua.data.generator.matrix;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import acqua.data.generator.vertex.Vertex;

public class biadjacencyMatrixIndegreeV extends biadjacencyMatrixWeightedV {
	
	public static void main(String[] args) {
		biadjacencyMatrix myMatrix = new biadjacencyMatrix("./src/main/resources/test.txt");
		biadjacencyMatrixIndegreeV testMatrix = new biadjacencyMatrixIndegreeV(myMatrix);
		// testMatrix.printBooleanMatrix();
		// testMatrix.printWieghtedMatrix();

		Map<Long, String> tempResult = testMatrix.getVsWithInBudget(3);
		for (Entry<Long, String> e : tempResult.entrySet()) {
			System.out.println(e.getKey() + " " + e.getValue());
		}
		// System.out.println("");
		for (Vertex e : testMatrix.getWeightForVs()) {
			System.out.println(e.toFullString());
		}

	}
	public biadjacencyMatrixIndegreeV(biadjacencyMatrix booleanMatrix) {
		super(booleanMatrix);
	}
	// the weight is just 1 now
	protected void updateWeightForAU(int tempU, List<Vertex> tempNeighbours) {
		for (Vertex tempNeighbour : tempNeighbours) {
			this.adjUVWeighted[tempU][tempNeighbour.getIntID()] += 1;
			this.adjUV[tempU][tempNeighbour.getIntID()] = true;
		}
	}


}
