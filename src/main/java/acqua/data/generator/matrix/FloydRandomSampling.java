package acqua.data.generator.matrix;

import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

public class FloydRandomSampling {
	// Floyd???s Algorithm
	public static <T> Set<T> randomSample(List<T> items, int m) {
		HashSet<T> res = new HashSet<T>(m);
		int n = items.size();
		Random rnd = new Random(System.currentTimeMillis());
		for (int i = n - m; i < n; i++) {
			int pos = rnd.nextInt(i + 1);
			T item = items.get(pos);
			if (res.contains(item))
				res.add(items.get(i));
			else
				res.add(item);
		}
		return res;
	}
}
